package fr.irisa.cairn.graphviz.model.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.irisa.cairn.graphviz.model.services.DottyGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDottyParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'digraph'", "'{'", "'->'", "';'", "'}'", "'['", "','", "']'", "'subgraph'", "'shape'", "'='", "'compound'", "'true'", "'\\u00A0false'", "'color'", "'fillcolor'", "'fontcolor'", "'style'", "'label'", "'rankdir'", "'TB'", "'LR'", "'BT'", "'RL'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalDottyParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDottyParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDottyParser.tokenNames; }
    public String getGrammarFileName() { return "InternalDotty.g"; }



     	private DottyGrammarAccess grammarAccess;

        public InternalDottyParser(TokenStream input, DottyGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Graph";
       	}

       	@Override
       	protected DottyGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleGraph"
    // InternalDotty.g:64:1: entryRuleGraph returns [EObject current=null] : iv_ruleGraph= ruleGraph EOF ;
    public final EObject entryRuleGraph() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGraph = null;


        try {
            // InternalDotty.g:64:46: (iv_ruleGraph= ruleGraph EOF )
            // InternalDotty.g:65:2: iv_ruleGraph= ruleGraph EOF
            {
             newCompositeNode(grammarAccess.getGraphRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGraph=ruleGraph();

            state._fsp--;

             current =iv_ruleGraph; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGraph"


    // $ANTLR start "ruleGraph"
    // InternalDotty.g:71:1: ruleGraph returns [EObject current=null] : (otherlv_0= 'digraph' ( (lv_name_1_0= RULE_ID ) ) ( (lv_body_2_0= ruleCompound ) ) ) ;
    public final EObject ruleGraph() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        EObject lv_body_2_0 = null;



        	enterRule();

        try {
            // InternalDotty.g:77:2: ( (otherlv_0= 'digraph' ( (lv_name_1_0= RULE_ID ) ) ( (lv_body_2_0= ruleCompound ) ) ) )
            // InternalDotty.g:78:2: (otherlv_0= 'digraph' ( (lv_name_1_0= RULE_ID ) ) ( (lv_body_2_0= ruleCompound ) ) )
            {
            // InternalDotty.g:78:2: (otherlv_0= 'digraph' ( (lv_name_1_0= RULE_ID ) ) ( (lv_body_2_0= ruleCompound ) ) )
            // InternalDotty.g:79:3: otherlv_0= 'digraph' ( (lv_name_1_0= RULE_ID ) ) ( (lv_body_2_0= ruleCompound ) )
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getGraphAccess().getDigraphKeyword_0());
            		
            // InternalDotty.g:83:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalDotty.g:84:4: (lv_name_1_0= RULE_ID )
            {
            // InternalDotty.g:84:4: (lv_name_1_0= RULE_ID )
            // InternalDotty.g:85:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_1_0, grammarAccess.getGraphAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getGraphRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalDotty.g:101:3: ( (lv_body_2_0= ruleCompound ) )
            // InternalDotty.g:102:4: (lv_body_2_0= ruleCompound )
            {
            // InternalDotty.g:102:4: (lv_body_2_0= ruleCompound )
            // InternalDotty.g:103:5: lv_body_2_0= ruleCompound
            {

            					newCompositeNode(grammarAccess.getGraphAccess().getBodyCompoundParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_body_2_0=ruleCompound();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getGraphRule());
            					}
            					set(
            						current,
            						"body",
            						lv_body_2_0,
            						"fr.irisa.cairn.graphviz.model.Dotty.Compound");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGraph"


    // $ANTLR start "entryRuleCompound"
    // InternalDotty.g:124:1: entryRuleCompound returns [EObject current=null] : iv_ruleCompound= ruleCompound EOF ;
    public final EObject entryRuleCompound() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCompound = null;


        try {
            // InternalDotty.g:124:49: (iv_ruleCompound= ruleCompound EOF )
            // InternalDotty.g:125:2: iv_ruleCompound= ruleCompound EOF
            {
             newCompositeNode(grammarAccess.getCompoundRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCompound=ruleCompound();

            state._fsp--;

             current =iv_ruleCompound; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCompound"


    // $ANTLR start "ruleCompound"
    // InternalDotty.g:131:1: ruleCompound returns [EObject current=null] : (otherlv_0= '{' ( ( ( ( (lv_nodes_1_0= ruleNode ) ) (otherlv_2= '->' ( (lv_nodes_3_0= ruleNode ) ) )* ) | ( (lv_attributes_4_0= ruleAttribute ) ) | ( (lv_sub_5_0= ruleCompound ) ) ) (otherlv_6= ';' )? )* otherlv_7= '}' ) ;
    public final EObject ruleCompound() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject lv_nodes_1_0 = null;

        EObject lv_nodes_3_0 = null;

        EObject lv_attributes_4_0 = null;

        EObject lv_sub_5_0 = null;



        	enterRule();

        try {
            // InternalDotty.g:137:2: ( (otherlv_0= '{' ( ( ( ( (lv_nodes_1_0= ruleNode ) ) (otherlv_2= '->' ( (lv_nodes_3_0= ruleNode ) ) )* ) | ( (lv_attributes_4_0= ruleAttribute ) ) | ( (lv_sub_5_0= ruleCompound ) ) ) (otherlv_6= ';' )? )* otherlv_7= '}' ) )
            // InternalDotty.g:138:2: (otherlv_0= '{' ( ( ( ( (lv_nodes_1_0= ruleNode ) ) (otherlv_2= '->' ( (lv_nodes_3_0= ruleNode ) ) )* ) | ( (lv_attributes_4_0= ruleAttribute ) ) | ( (lv_sub_5_0= ruleCompound ) ) ) (otherlv_6= ';' )? )* otherlv_7= '}' )
            {
            // InternalDotty.g:138:2: (otherlv_0= '{' ( ( ( ( (lv_nodes_1_0= ruleNode ) ) (otherlv_2= '->' ( (lv_nodes_3_0= ruleNode ) ) )* ) | ( (lv_attributes_4_0= ruleAttribute ) ) | ( (lv_sub_5_0= ruleCompound ) ) ) (otherlv_6= ';' )? )* otherlv_7= '}' )
            // InternalDotty.g:139:3: otherlv_0= '{' ( ( ( ( (lv_nodes_1_0= ruleNode ) ) (otherlv_2= '->' ( (lv_nodes_3_0= ruleNode ) ) )* ) | ( (lv_attributes_4_0= ruleAttribute ) ) | ( (lv_sub_5_0= ruleCompound ) ) ) (otherlv_6= ';' )? )* otherlv_7= '}'
            {
            otherlv_0=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getCompoundAccess().getLeftCurlyBracketKeyword_0());
            		
            // InternalDotty.g:143:3: ( ( ( ( (lv_nodes_1_0= ruleNode ) ) (otherlv_2= '->' ( (lv_nodes_3_0= ruleNode ) ) )* ) | ( (lv_attributes_4_0= ruleAttribute ) ) | ( (lv_sub_5_0= ruleCompound ) ) ) (otherlv_6= ';' )? )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_ID||LA4_0==12||(LA4_0>=19 && LA4_0<=20)||LA4_0==22||(LA4_0>=25 && LA4_0<=30)) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalDotty.g:144:4: ( ( ( (lv_nodes_1_0= ruleNode ) ) (otherlv_2= '->' ( (lv_nodes_3_0= ruleNode ) ) )* ) | ( (lv_attributes_4_0= ruleAttribute ) ) | ( (lv_sub_5_0= ruleCompound ) ) ) (otherlv_6= ';' )?
            	    {
            	    // InternalDotty.g:144:4: ( ( ( (lv_nodes_1_0= ruleNode ) ) (otherlv_2= '->' ( (lv_nodes_3_0= ruleNode ) ) )* ) | ( (lv_attributes_4_0= ruleAttribute ) ) | ( (lv_sub_5_0= ruleCompound ) ) )
            	    int alt2=3;
            	    switch ( input.LA(1) ) {
            	    case RULE_ID:
            	    case 19:
            	        {
            	        alt2=1;
            	        }
            	        break;
            	    case 20:
            	    case 22:
            	    case 25:
            	    case 26:
            	    case 27:
            	    case 28:
            	    case 29:
            	    case 30:
            	        {
            	        alt2=2;
            	        }
            	        break;
            	    case 12:
            	        {
            	        alt2=3;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 2, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt2) {
            	        case 1 :
            	            // InternalDotty.g:145:5: ( ( (lv_nodes_1_0= ruleNode ) ) (otherlv_2= '->' ( (lv_nodes_3_0= ruleNode ) ) )* )
            	            {
            	            // InternalDotty.g:145:5: ( ( (lv_nodes_1_0= ruleNode ) ) (otherlv_2= '->' ( (lv_nodes_3_0= ruleNode ) ) )* )
            	            // InternalDotty.g:146:6: ( (lv_nodes_1_0= ruleNode ) ) (otherlv_2= '->' ( (lv_nodes_3_0= ruleNode ) ) )*
            	            {
            	            // InternalDotty.g:146:6: ( (lv_nodes_1_0= ruleNode ) )
            	            // InternalDotty.g:147:7: (lv_nodes_1_0= ruleNode )
            	            {
            	            // InternalDotty.g:147:7: (lv_nodes_1_0= ruleNode )
            	            // InternalDotty.g:148:8: lv_nodes_1_0= ruleNode
            	            {

            	            								newCompositeNode(grammarAccess.getCompoundAccess().getNodesNodeParserRuleCall_1_0_0_0_0());
            	            							
            	            pushFollow(FOLLOW_6);
            	            lv_nodes_1_0=ruleNode();

            	            state._fsp--;


            	            								if (current==null) {
            	            									current = createModelElementForParent(grammarAccess.getCompoundRule());
            	            								}
            	            								add(
            	            									current,
            	            									"nodes",
            	            									lv_nodes_1_0,
            	            									"fr.irisa.cairn.graphviz.model.Dotty.Node");
            	            								afterParserOrEnumRuleCall();
            	            							

            	            }


            	            }

            	            // InternalDotty.g:165:6: (otherlv_2= '->' ( (lv_nodes_3_0= ruleNode ) ) )*
            	            loop1:
            	            do {
            	                int alt1=2;
            	                int LA1_0 = input.LA(1);

            	                if ( (LA1_0==13) ) {
            	                    alt1=1;
            	                }


            	                switch (alt1) {
            	            	case 1 :
            	            	    // InternalDotty.g:166:7: otherlv_2= '->' ( (lv_nodes_3_0= ruleNode ) )
            	            	    {
            	            	    otherlv_2=(Token)match(input,13,FOLLOW_7); 

            	            	    							newLeafNode(otherlv_2, grammarAccess.getCompoundAccess().getHyphenMinusGreaterThanSignKeyword_1_0_0_1_0());
            	            	    						
            	            	    // InternalDotty.g:170:7: ( (lv_nodes_3_0= ruleNode ) )
            	            	    // InternalDotty.g:171:8: (lv_nodes_3_0= ruleNode )
            	            	    {
            	            	    // InternalDotty.g:171:8: (lv_nodes_3_0= ruleNode )
            	            	    // InternalDotty.g:172:9: lv_nodes_3_0= ruleNode
            	            	    {

            	            	    									newCompositeNode(grammarAccess.getCompoundAccess().getNodesNodeParserRuleCall_1_0_0_1_1_0());
            	            	    								
            	            	    pushFollow(FOLLOW_6);
            	            	    lv_nodes_3_0=ruleNode();

            	            	    state._fsp--;


            	            	    									if (current==null) {
            	            	    										current = createModelElementForParent(grammarAccess.getCompoundRule());
            	            	    									}
            	            	    									add(
            	            	    										current,
            	            	    										"nodes",
            	            	    										lv_nodes_3_0,
            	            	    										"fr.irisa.cairn.graphviz.model.Dotty.Node");
            	            	    									afterParserOrEnumRuleCall();
            	            	    								

            	            	    }


            	            	    }


            	            	    }
            	            	    break;

            	            	default :
            	            	    break loop1;
            	                }
            	            } while (true);


            	            }


            	            }
            	            break;
            	        case 2 :
            	            // InternalDotty.g:192:5: ( (lv_attributes_4_0= ruleAttribute ) )
            	            {
            	            // InternalDotty.g:192:5: ( (lv_attributes_4_0= ruleAttribute ) )
            	            // InternalDotty.g:193:6: (lv_attributes_4_0= ruleAttribute )
            	            {
            	            // InternalDotty.g:193:6: (lv_attributes_4_0= ruleAttribute )
            	            // InternalDotty.g:194:7: lv_attributes_4_0= ruleAttribute
            	            {

            	            							newCompositeNode(grammarAccess.getCompoundAccess().getAttributesAttributeParserRuleCall_1_0_1_0());
            	            						
            	            pushFollow(FOLLOW_8);
            	            lv_attributes_4_0=ruleAttribute();

            	            state._fsp--;


            	            							if (current==null) {
            	            								current = createModelElementForParent(grammarAccess.getCompoundRule());
            	            							}
            	            							add(
            	            								current,
            	            								"attributes",
            	            								lv_attributes_4_0,
            	            								"fr.irisa.cairn.graphviz.model.Dotty.Attribute");
            	            							afterParserOrEnumRuleCall();
            	            						

            	            }


            	            }


            	            }
            	            break;
            	        case 3 :
            	            // InternalDotty.g:212:5: ( (lv_sub_5_0= ruleCompound ) )
            	            {
            	            // InternalDotty.g:212:5: ( (lv_sub_5_0= ruleCompound ) )
            	            // InternalDotty.g:213:6: (lv_sub_5_0= ruleCompound )
            	            {
            	            // InternalDotty.g:213:6: (lv_sub_5_0= ruleCompound )
            	            // InternalDotty.g:214:7: lv_sub_5_0= ruleCompound
            	            {

            	            							newCompositeNode(grammarAccess.getCompoundAccess().getSubCompoundParserRuleCall_1_0_2_0());
            	            						
            	            pushFollow(FOLLOW_8);
            	            lv_sub_5_0=ruleCompound();

            	            state._fsp--;


            	            							if (current==null) {
            	            								current = createModelElementForParent(grammarAccess.getCompoundRule());
            	            							}
            	            							add(
            	            								current,
            	            								"sub",
            	            								lv_sub_5_0,
            	            								"fr.irisa.cairn.graphviz.model.Dotty.Compound");
            	            							afterParserOrEnumRuleCall();
            	            						

            	            }


            	            }


            	            }
            	            break;

            	    }

            	    // InternalDotty.g:232:4: (otherlv_6= ';' )?
            	    int alt3=2;
            	    int LA3_0 = input.LA(1);

            	    if ( (LA3_0==14) ) {
            	        alt3=1;
            	    }
            	    switch (alt3) {
            	        case 1 :
            	            // InternalDotty.g:233:5: otherlv_6= ';'
            	            {
            	            otherlv_6=(Token)match(input,14,FOLLOW_5); 

            	            					newLeafNode(otherlv_6, grammarAccess.getCompoundAccess().getSemicolonKeyword_1_1());
            	            				

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_7=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getCompoundAccess().getRightCurlyBracketKeyword_2());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCompound"


    // $ANTLR start "entryRuleNode"
    // InternalDotty.g:247:1: entryRuleNode returns [EObject current=null] : iv_ruleNode= ruleNode EOF ;
    public final EObject entryRuleNode() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNode = null;


        try {
            // InternalDotty.g:247:45: (iv_ruleNode= ruleNode EOF )
            // InternalDotty.g:248:2: iv_ruleNode= ruleNode EOF
            {
             newCompositeNode(grammarAccess.getNodeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNode=ruleNode();

            state._fsp--;

             current =iv_ruleNode; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNode"


    // $ANTLR start "ruleNode"
    // InternalDotty.g:254:1: ruleNode returns [EObject current=null] : (this_LeafNode_0= ruleLeafNode | this_SubNode_1= ruleSubNode ) ;
    public final EObject ruleNode() throws RecognitionException {
        EObject current = null;

        EObject this_LeafNode_0 = null;

        EObject this_SubNode_1 = null;



        	enterRule();

        try {
            // InternalDotty.g:260:2: ( (this_LeafNode_0= ruleLeafNode | this_SubNode_1= ruleSubNode ) )
            // InternalDotty.g:261:2: (this_LeafNode_0= ruleLeafNode | this_SubNode_1= ruleSubNode )
            {
            // InternalDotty.g:261:2: (this_LeafNode_0= ruleLeafNode | this_SubNode_1= ruleSubNode )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_ID) ) {
                alt5=1;
            }
            else if ( (LA5_0==19) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalDotty.g:262:3: this_LeafNode_0= ruleLeafNode
                    {

                    			newCompositeNode(grammarAccess.getNodeAccess().getLeafNodeParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_LeafNode_0=ruleLeafNode();

                    state._fsp--;


                    			current = this_LeafNode_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalDotty.g:271:3: this_SubNode_1= ruleSubNode
                    {

                    			newCompositeNode(grammarAccess.getNodeAccess().getSubNodeParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_SubNode_1=ruleSubNode();

                    state._fsp--;


                    			current = this_SubNode_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNode"


    // $ANTLR start "entryRuleLeafNode"
    // InternalDotty.g:283:1: entryRuleLeafNode returns [EObject current=null] : iv_ruleLeafNode= ruleLeafNode EOF ;
    public final EObject entryRuleLeafNode() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLeafNode = null;


        try {
            // InternalDotty.g:283:49: (iv_ruleLeafNode= ruleLeafNode EOF )
            // InternalDotty.g:284:2: iv_ruleLeafNode= ruleLeafNode EOF
            {
             newCompositeNode(grammarAccess.getLeafNodeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLeafNode=ruleLeafNode();

            state._fsp--;

             current =iv_ruleLeafNode; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLeafNode"


    // $ANTLR start "ruleLeafNode"
    // InternalDotty.g:290:1: ruleLeafNode returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '[' ( (lv_attributes_2_0= ruleAttribute ) ) (otherlv_3= ',' ( (lv_attributes_4_0= ruleAttribute ) ) )* otherlv_5= ']' )? ) ;
    public final EObject ruleLeafNode() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_attributes_2_0 = null;

        EObject lv_attributes_4_0 = null;



        	enterRule();

        try {
            // InternalDotty.g:296:2: ( ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '[' ( (lv_attributes_2_0= ruleAttribute ) ) (otherlv_3= ',' ( (lv_attributes_4_0= ruleAttribute ) ) )* otherlv_5= ']' )? ) )
            // InternalDotty.g:297:2: ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '[' ( (lv_attributes_2_0= ruleAttribute ) ) (otherlv_3= ',' ( (lv_attributes_4_0= ruleAttribute ) ) )* otherlv_5= ']' )? )
            {
            // InternalDotty.g:297:2: ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '[' ( (lv_attributes_2_0= ruleAttribute ) ) (otherlv_3= ',' ( (lv_attributes_4_0= ruleAttribute ) ) )* otherlv_5= ']' )? )
            // InternalDotty.g:298:3: ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '[' ( (lv_attributes_2_0= ruleAttribute ) ) (otherlv_3= ',' ( (lv_attributes_4_0= ruleAttribute ) ) )* otherlv_5= ']' )?
            {
            // InternalDotty.g:298:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalDotty.g:299:4: (lv_name_0_0= RULE_ID )
            {
            // InternalDotty.g:299:4: (lv_name_0_0= RULE_ID )
            // InternalDotty.g:300:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_9); 

            					newLeafNode(lv_name_0_0, grammarAccess.getLeafNodeAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getLeafNodeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalDotty.g:316:3: (otherlv_1= '[' ( (lv_attributes_2_0= ruleAttribute ) ) (otherlv_3= ',' ( (lv_attributes_4_0= ruleAttribute ) ) )* otherlv_5= ']' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==16) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalDotty.g:317:4: otherlv_1= '[' ( (lv_attributes_2_0= ruleAttribute ) ) (otherlv_3= ',' ( (lv_attributes_4_0= ruleAttribute ) ) )* otherlv_5= ']'
                    {
                    otherlv_1=(Token)match(input,16,FOLLOW_10); 

                    				newLeafNode(otherlv_1, grammarAccess.getLeafNodeAccess().getLeftSquareBracketKeyword_1_0());
                    			
                    // InternalDotty.g:321:4: ( (lv_attributes_2_0= ruleAttribute ) )
                    // InternalDotty.g:322:5: (lv_attributes_2_0= ruleAttribute )
                    {
                    // InternalDotty.g:322:5: (lv_attributes_2_0= ruleAttribute )
                    // InternalDotty.g:323:6: lv_attributes_2_0= ruleAttribute
                    {

                    						newCompositeNode(grammarAccess.getLeafNodeAccess().getAttributesAttributeParserRuleCall_1_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_attributes_2_0=ruleAttribute();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLeafNodeRule());
                    						}
                    						add(
                    							current,
                    							"attributes",
                    							lv_attributes_2_0,
                    							"fr.irisa.cairn.graphviz.model.Dotty.Attribute");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDotty.g:340:4: (otherlv_3= ',' ( (lv_attributes_4_0= ruleAttribute ) ) )*
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0==17) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // InternalDotty.g:341:5: otherlv_3= ',' ( (lv_attributes_4_0= ruleAttribute ) )
                    	    {
                    	    otherlv_3=(Token)match(input,17,FOLLOW_10); 

                    	    					newLeafNode(otherlv_3, grammarAccess.getLeafNodeAccess().getCommaKeyword_1_2_0());
                    	    				
                    	    // InternalDotty.g:345:5: ( (lv_attributes_4_0= ruleAttribute ) )
                    	    // InternalDotty.g:346:6: (lv_attributes_4_0= ruleAttribute )
                    	    {
                    	    // InternalDotty.g:346:6: (lv_attributes_4_0= ruleAttribute )
                    	    // InternalDotty.g:347:7: lv_attributes_4_0= ruleAttribute
                    	    {

                    	    							newCompositeNode(grammarAccess.getLeafNodeAccess().getAttributesAttributeParserRuleCall_1_2_1_0());
                    	    						
                    	    pushFollow(FOLLOW_11);
                    	    lv_attributes_4_0=ruleAttribute();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getLeafNodeRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"attributes",
                    	    								lv_attributes_4_0,
                    	    								"fr.irisa.cairn.graphviz.model.Dotty.Attribute");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);

                    otherlv_5=(Token)match(input,18,FOLLOW_2); 

                    				newLeafNode(otherlv_5, grammarAccess.getLeafNodeAccess().getRightSquareBracketKeyword_1_3());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLeafNode"


    // $ANTLR start "entryRuleSubNode"
    // InternalDotty.g:374:1: entryRuleSubNode returns [EObject current=null] : iv_ruleSubNode= ruleSubNode EOF ;
    public final EObject entryRuleSubNode() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSubNode = null;


        try {
            // InternalDotty.g:374:48: (iv_ruleSubNode= ruleSubNode EOF )
            // InternalDotty.g:375:2: iv_ruleSubNode= ruleSubNode EOF
            {
             newCompositeNode(grammarAccess.getSubNodeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSubNode=ruleSubNode();

            state._fsp--;

             current =iv_ruleSubNode; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSubNode"


    // $ANTLR start "ruleSubNode"
    // InternalDotty.g:381:1: ruleSubNode returns [EObject current=null] : (otherlv_0= 'subgraph' ( (lv_name_1_0= RULE_ID ) ) ( (lv_body_2_0= ruleCompound ) ) ) ;
    public final EObject ruleSubNode() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        EObject lv_body_2_0 = null;



        	enterRule();

        try {
            // InternalDotty.g:387:2: ( (otherlv_0= 'subgraph' ( (lv_name_1_0= RULE_ID ) ) ( (lv_body_2_0= ruleCompound ) ) ) )
            // InternalDotty.g:388:2: (otherlv_0= 'subgraph' ( (lv_name_1_0= RULE_ID ) ) ( (lv_body_2_0= ruleCompound ) ) )
            {
            // InternalDotty.g:388:2: (otherlv_0= 'subgraph' ( (lv_name_1_0= RULE_ID ) ) ( (lv_body_2_0= ruleCompound ) ) )
            // InternalDotty.g:389:3: otherlv_0= 'subgraph' ( (lv_name_1_0= RULE_ID ) ) ( (lv_body_2_0= ruleCompound ) )
            {
            otherlv_0=(Token)match(input,19,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getSubNodeAccess().getSubgraphKeyword_0());
            		
            // InternalDotty.g:393:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalDotty.g:394:4: (lv_name_1_0= RULE_ID )
            {
            // InternalDotty.g:394:4: (lv_name_1_0= RULE_ID )
            // InternalDotty.g:395:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_1_0, grammarAccess.getSubNodeAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSubNodeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalDotty.g:411:3: ( (lv_body_2_0= ruleCompound ) )
            // InternalDotty.g:412:4: (lv_body_2_0= ruleCompound )
            {
            // InternalDotty.g:412:4: (lv_body_2_0= ruleCompound )
            // InternalDotty.g:413:5: lv_body_2_0= ruleCompound
            {

            					newCompositeNode(grammarAccess.getSubNodeAccess().getBodyCompoundParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_body_2_0=ruleCompound();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSubNodeRule());
            					}
            					set(
            						current,
            						"body",
            						lv_body_2_0,
            						"fr.irisa.cairn.graphviz.model.Dotty.Compound");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSubNode"


    // $ANTLR start "entryRuleAttribute"
    // InternalDotty.g:434:1: entryRuleAttribute returns [EObject current=null] : iv_ruleAttribute= ruleAttribute EOF ;
    public final EObject entryRuleAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttribute = null;


        try {
            // InternalDotty.g:434:50: (iv_ruleAttribute= ruleAttribute EOF )
            // InternalDotty.g:435:2: iv_ruleAttribute= ruleAttribute EOF
            {
             newCompositeNode(grammarAccess.getAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttribute=ruleAttribute();

            state._fsp--;

             current =iv_ruleAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // InternalDotty.g:441:1: ruleAttribute returns [EObject current=null] : (this_RankAttribute_0= ruleRankAttribute | this_ShapeAttribute_1= ruleShapeAttribute | this_ColorAttribute_2= ruleColorAttribute | this_LabelAttribute_3= ruleLabelAttribute | this_StyleAttribute_4= ruleStyleAttribute | this_CompoundAttribute_5= ruleCompoundAttribute ) ;
    public final EObject ruleAttribute() throws RecognitionException {
        EObject current = null;

        EObject this_RankAttribute_0 = null;

        EObject this_ShapeAttribute_1 = null;

        EObject this_ColorAttribute_2 = null;

        EObject this_LabelAttribute_3 = null;

        EObject this_StyleAttribute_4 = null;

        EObject this_CompoundAttribute_5 = null;



        	enterRule();

        try {
            // InternalDotty.g:447:2: ( (this_RankAttribute_0= ruleRankAttribute | this_ShapeAttribute_1= ruleShapeAttribute | this_ColorAttribute_2= ruleColorAttribute | this_LabelAttribute_3= ruleLabelAttribute | this_StyleAttribute_4= ruleStyleAttribute | this_CompoundAttribute_5= ruleCompoundAttribute ) )
            // InternalDotty.g:448:2: (this_RankAttribute_0= ruleRankAttribute | this_ShapeAttribute_1= ruleShapeAttribute | this_ColorAttribute_2= ruleColorAttribute | this_LabelAttribute_3= ruleLabelAttribute | this_StyleAttribute_4= ruleStyleAttribute | this_CompoundAttribute_5= ruleCompoundAttribute )
            {
            // InternalDotty.g:448:2: (this_RankAttribute_0= ruleRankAttribute | this_ShapeAttribute_1= ruleShapeAttribute | this_ColorAttribute_2= ruleColorAttribute | this_LabelAttribute_3= ruleLabelAttribute | this_StyleAttribute_4= ruleStyleAttribute | this_CompoundAttribute_5= ruleCompoundAttribute )
            int alt8=6;
            switch ( input.LA(1) ) {
            case 30:
                {
                alt8=1;
                }
                break;
            case 20:
                {
                alt8=2;
                }
                break;
            case 25:
            case 26:
            case 27:
                {
                alt8=3;
                }
                break;
            case 29:
                {
                alt8=4;
                }
                break;
            case 28:
                {
                alt8=5;
                }
                break;
            case 22:
                {
                alt8=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalDotty.g:449:3: this_RankAttribute_0= ruleRankAttribute
                    {

                    			newCompositeNode(grammarAccess.getAttributeAccess().getRankAttributeParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_RankAttribute_0=ruleRankAttribute();

                    state._fsp--;


                    			current = this_RankAttribute_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalDotty.g:458:3: this_ShapeAttribute_1= ruleShapeAttribute
                    {

                    			newCompositeNode(grammarAccess.getAttributeAccess().getShapeAttributeParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ShapeAttribute_1=ruleShapeAttribute();

                    state._fsp--;


                    			current = this_ShapeAttribute_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalDotty.g:467:3: this_ColorAttribute_2= ruleColorAttribute
                    {

                    			newCompositeNode(grammarAccess.getAttributeAccess().getColorAttributeParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_ColorAttribute_2=ruleColorAttribute();

                    state._fsp--;


                    			current = this_ColorAttribute_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalDotty.g:476:3: this_LabelAttribute_3= ruleLabelAttribute
                    {

                    			newCompositeNode(grammarAccess.getAttributeAccess().getLabelAttributeParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_LabelAttribute_3=ruleLabelAttribute();

                    state._fsp--;


                    			current = this_LabelAttribute_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalDotty.g:485:3: this_StyleAttribute_4= ruleStyleAttribute
                    {

                    			newCompositeNode(grammarAccess.getAttributeAccess().getStyleAttributeParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_StyleAttribute_4=ruleStyleAttribute();

                    state._fsp--;


                    			current = this_StyleAttribute_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 6 :
                    // InternalDotty.g:494:3: this_CompoundAttribute_5= ruleCompoundAttribute
                    {

                    			newCompositeNode(grammarAccess.getAttributeAccess().getCompoundAttributeParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_CompoundAttribute_5=ruleCompoundAttribute();

                    state._fsp--;


                    			current = this_CompoundAttribute_5;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRuleShapeAttribute"
    // InternalDotty.g:506:1: entryRuleShapeAttribute returns [EObject current=null] : iv_ruleShapeAttribute= ruleShapeAttribute EOF ;
    public final EObject entryRuleShapeAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleShapeAttribute = null;


        try {
            // InternalDotty.g:506:55: (iv_ruleShapeAttribute= ruleShapeAttribute EOF )
            // InternalDotty.g:507:2: iv_ruleShapeAttribute= ruleShapeAttribute EOF
            {
             newCompositeNode(grammarAccess.getShapeAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleShapeAttribute=ruleShapeAttribute();

            state._fsp--;

             current =iv_ruleShapeAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleShapeAttribute"


    // $ANTLR start "ruleShapeAttribute"
    // InternalDotty.g:513:1: ruleShapeAttribute returns [EObject current=null] : (otherlv_0= 'shape' otherlv_1= '=' ( ( (lv_shape_2_1= RULE_ID | lv_shape_2_2= RULE_STRING ) ) ) ) ;
    public final EObject ruleShapeAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_shape_2_1=null;
        Token lv_shape_2_2=null;


        	enterRule();

        try {
            // InternalDotty.g:519:2: ( (otherlv_0= 'shape' otherlv_1= '=' ( ( (lv_shape_2_1= RULE_ID | lv_shape_2_2= RULE_STRING ) ) ) ) )
            // InternalDotty.g:520:2: (otherlv_0= 'shape' otherlv_1= '=' ( ( (lv_shape_2_1= RULE_ID | lv_shape_2_2= RULE_STRING ) ) ) )
            {
            // InternalDotty.g:520:2: (otherlv_0= 'shape' otherlv_1= '=' ( ( (lv_shape_2_1= RULE_ID | lv_shape_2_2= RULE_STRING ) ) ) )
            // InternalDotty.g:521:3: otherlv_0= 'shape' otherlv_1= '=' ( ( (lv_shape_2_1= RULE_ID | lv_shape_2_2= RULE_STRING ) ) )
            {
            otherlv_0=(Token)match(input,20,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getShapeAttributeAccess().getShapeKeyword_0());
            		
            otherlv_1=(Token)match(input,21,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getShapeAttributeAccess().getEqualsSignKeyword_1());
            		
            // InternalDotty.g:529:3: ( ( (lv_shape_2_1= RULE_ID | lv_shape_2_2= RULE_STRING ) ) )
            // InternalDotty.g:530:4: ( (lv_shape_2_1= RULE_ID | lv_shape_2_2= RULE_STRING ) )
            {
            // InternalDotty.g:530:4: ( (lv_shape_2_1= RULE_ID | lv_shape_2_2= RULE_STRING ) )
            // InternalDotty.g:531:5: (lv_shape_2_1= RULE_ID | lv_shape_2_2= RULE_STRING )
            {
            // InternalDotty.g:531:5: (lv_shape_2_1= RULE_ID | lv_shape_2_2= RULE_STRING )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_ID) ) {
                alt9=1;
            }
            else if ( (LA9_0==RULE_STRING) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalDotty.g:532:6: lv_shape_2_1= RULE_ID
                    {
                    lv_shape_2_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    						newLeafNode(lv_shape_2_1, grammarAccess.getShapeAttributeAccess().getShapeIDTerminalRuleCall_2_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getShapeAttributeRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"shape",
                    							lv_shape_2_1,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }
                    break;
                case 2 :
                    // InternalDotty.g:547:6: lv_shape_2_2= RULE_STRING
                    {
                    lv_shape_2_2=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_shape_2_2, grammarAccess.getShapeAttributeAccess().getShapeSTRINGTerminalRuleCall_2_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getShapeAttributeRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"shape",
                    							lv_shape_2_2,
                    							"fr.irisa.cairn.graphviz.model.Dotty.STRING");
                    					

                    }
                    break;

            }


            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleShapeAttribute"


    // $ANTLR start "entryRuleCompoundAttribute"
    // InternalDotty.g:568:1: entryRuleCompoundAttribute returns [EObject current=null] : iv_ruleCompoundAttribute= ruleCompoundAttribute EOF ;
    public final EObject entryRuleCompoundAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCompoundAttribute = null;


        try {
            // InternalDotty.g:568:58: (iv_ruleCompoundAttribute= ruleCompoundAttribute EOF )
            // InternalDotty.g:569:2: iv_ruleCompoundAttribute= ruleCompoundAttribute EOF
            {
             newCompositeNode(grammarAccess.getCompoundAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCompoundAttribute=ruleCompoundAttribute();

            state._fsp--;

             current =iv_ruleCompoundAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCompoundAttribute"


    // $ANTLR start "ruleCompoundAttribute"
    // InternalDotty.g:575:1: ruleCompoundAttribute returns [EObject current=null] : (otherlv_0= 'compound' otherlv_1= '=' ( ( (lv_compound_2_1= 'true' | lv_compound_2_2= '\\u00A0false' ) ) ) ) ;
    public final EObject ruleCompoundAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_compound_2_1=null;
        Token lv_compound_2_2=null;


        	enterRule();

        try {
            // InternalDotty.g:581:2: ( (otherlv_0= 'compound' otherlv_1= '=' ( ( (lv_compound_2_1= 'true' | lv_compound_2_2= '\\u00A0false' ) ) ) ) )
            // InternalDotty.g:582:2: (otherlv_0= 'compound' otherlv_1= '=' ( ( (lv_compound_2_1= 'true' | lv_compound_2_2= '\\u00A0false' ) ) ) )
            {
            // InternalDotty.g:582:2: (otherlv_0= 'compound' otherlv_1= '=' ( ( (lv_compound_2_1= 'true' | lv_compound_2_2= '\\u00A0false' ) ) ) )
            // InternalDotty.g:583:3: otherlv_0= 'compound' otherlv_1= '=' ( ( (lv_compound_2_1= 'true' | lv_compound_2_2= '\\u00A0false' ) ) )
            {
            otherlv_0=(Token)match(input,22,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getCompoundAttributeAccess().getCompoundKeyword_0());
            		
            otherlv_1=(Token)match(input,21,FOLLOW_14); 

            			newLeafNode(otherlv_1, grammarAccess.getCompoundAttributeAccess().getEqualsSignKeyword_1());
            		
            // InternalDotty.g:591:3: ( ( (lv_compound_2_1= 'true' | lv_compound_2_2= '\\u00A0false' ) ) )
            // InternalDotty.g:592:4: ( (lv_compound_2_1= 'true' | lv_compound_2_2= '\\u00A0false' ) )
            {
            // InternalDotty.g:592:4: ( (lv_compound_2_1= 'true' | lv_compound_2_2= '\\u00A0false' ) )
            // InternalDotty.g:593:5: (lv_compound_2_1= 'true' | lv_compound_2_2= '\\u00A0false' )
            {
            // InternalDotty.g:593:5: (lv_compound_2_1= 'true' | lv_compound_2_2= '\\u00A0false' )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==23) ) {
                alt10=1;
            }
            else if ( (LA10_0==24) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalDotty.g:594:6: lv_compound_2_1= 'true'
                    {
                    lv_compound_2_1=(Token)match(input,23,FOLLOW_2); 

                    						newLeafNode(lv_compound_2_1, grammarAccess.getCompoundAttributeAccess().getCompoundTrueKeyword_2_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getCompoundAttributeRule());
                    						}
                    						setWithLastConsumed(current, "compound", lv_compound_2_1, null);
                    					

                    }
                    break;
                case 2 :
                    // InternalDotty.g:605:6: lv_compound_2_2= '\\u00A0false'
                    {
                    lv_compound_2_2=(Token)match(input,24,FOLLOW_2); 

                    						newLeafNode(lv_compound_2_2, grammarAccess.getCompoundAttributeAccess().getCompoundFalseKeyword_2_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getCompoundAttributeRule());
                    						}
                    						setWithLastConsumed(current, "compound", lv_compound_2_2, null);
                    					

                    }
                    break;

            }


            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCompoundAttribute"


    // $ANTLR start "entryRuleColorAttribute"
    // InternalDotty.g:622:1: entryRuleColorAttribute returns [EObject current=null] : iv_ruleColorAttribute= ruleColorAttribute EOF ;
    public final EObject entryRuleColorAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleColorAttribute = null;


        try {
            // InternalDotty.g:622:55: (iv_ruleColorAttribute= ruleColorAttribute EOF )
            // InternalDotty.g:623:2: iv_ruleColorAttribute= ruleColorAttribute EOF
            {
             newCompositeNode(grammarAccess.getColorAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleColorAttribute=ruleColorAttribute();

            state._fsp--;

             current =iv_ruleColorAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleColorAttribute"


    // $ANTLR start "ruleColorAttribute"
    // InternalDotty.g:629:1: ruleColorAttribute returns [EObject current=null] : ( (otherlv_0= 'color' | otherlv_1= 'fillcolor' | otherlv_2= 'fontcolor' ) otherlv_3= '=' ( ( (lv_color_4_1= RULE_ID | lv_color_4_2= RULE_STRING ) ) ) ) ;
    public final EObject ruleColorAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_color_4_1=null;
        Token lv_color_4_2=null;


        	enterRule();

        try {
            // InternalDotty.g:635:2: ( ( (otherlv_0= 'color' | otherlv_1= 'fillcolor' | otherlv_2= 'fontcolor' ) otherlv_3= '=' ( ( (lv_color_4_1= RULE_ID | lv_color_4_2= RULE_STRING ) ) ) ) )
            // InternalDotty.g:636:2: ( (otherlv_0= 'color' | otherlv_1= 'fillcolor' | otherlv_2= 'fontcolor' ) otherlv_3= '=' ( ( (lv_color_4_1= RULE_ID | lv_color_4_2= RULE_STRING ) ) ) )
            {
            // InternalDotty.g:636:2: ( (otherlv_0= 'color' | otherlv_1= 'fillcolor' | otherlv_2= 'fontcolor' ) otherlv_3= '=' ( ( (lv_color_4_1= RULE_ID | lv_color_4_2= RULE_STRING ) ) ) )
            // InternalDotty.g:637:3: (otherlv_0= 'color' | otherlv_1= 'fillcolor' | otherlv_2= 'fontcolor' ) otherlv_3= '=' ( ( (lv_color_4_1= RULE_ID | lv_color_4_2= RULE_STRING ) ) )
            {
            // InternalDotty.g:637:3: (otherlv_0= 'color' | otherlv_1= 'fillcolor' | otherlv_2= 'fontcolor' )
            int alt11=3;
            switch ( input.LA(1) ) {
            case 25:
                {
                alt11=1;
                }
                break;
            case 26:
                {
                alt11=2;
                }
                break;
            case 27:
                {
                alt11=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }

            switch (alt11) {
                case 1 :
                    // InternalDotty.g:638:4: otherlv_0= 'color'
                    {
                    otherlv_0=(Token)match(input,25,FOLLOW_12); 

                    				newLeafNode(otherlv_0, grammarAccess.getColorAttributeAccess().getColorKeyword_0_0());
                    			

                    }
                    break;
                case 2 :
                    // InternalDotty.g:643:4: otherlv_1= 'fillcolor'
                    {
                    otherlv_1=(Token)match(input,26,FOLLOW_12); 

                    				newLeafNode(otherlv_1, grammarAccess.getColorAttributeAccess().getFillcolorKeyword_0_1());
                    			

                    }
                    break;
                case 3 :
                    // InternalDotty.g:648:4: otherlv_2= 'fontcolor'
                    {
                    otherlv_2=(Token)match(input,27,FOLLOW_12); 

                    				newLeafNode(otherlv_2, grammarAccess.getColorAttributeAccess().getFontcolorKeyword_0_2());
                    			

                    }
                    break;

            }

            otherlv_3=(Token)match(input,21,FOLLOW_13); 

            			newLeafNode(otherlv_3, grammarAccess.getColorAttributeAccess().getEqualsSignKeyword_1());
            		
            // InternalDotty.g:657:3: ( ( (lv_color_4_1= RULE_ID | lv_color_4_2= RULE_STRING ) ) )
            // InternalDotty.g:658:4: ( (lv_color_4_1= RULE_ID | lv_color_4_2= RULE_STRING ) )
            {
            // InternalDotty.g:658:4: ( (lv_color_4_1= RULE_ID | lv_color_4_2= RULE_STRING ) )
            // InternalDotty.g:659:5: (lv_color_4_1= RULE_ID | lv_color_4_2= RULE_STRING )
            {
            // InternalDotty.g:659:5: (lv_color_4_1= RULE_ID | lv_color_4_2= RULE_STRING )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==RULE_ID) ) {
                alt12=1;
            }
            else if ( (LA12_0==RULE_STRING) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalDotty.g:660:6: lv_color_4_1= RULE_ID
                    {
                    lv_color_4_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    						newLeafNode(lv_color_4_1, grammarAccess.getColorAttributeAccess().getColorIDTerminalRuleCall_2_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getColorAttributeRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"color",
                    							lv_color_4_1,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }
                    break;
                case 2 :
                    // InternalDotty.g:675:6: lv_color_4_2= RULE_STRING
                    {
                    lv_color_4_2=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_color_4_2, grammarAccess.getColorAttributeAccess().getColorSTRINGTerminalRuleCall_2_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getColorAttributeRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"color",
                    							lv_color_4_2,
                    							"fr.irisa.cairn.graphviz.model.Dotty.STRING");
                    					

                    }
                    break;

            }


            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleColorAttribute"


    // $ANTLR start "entryRuleStyleAttribute"
    // InternalDotty.g:696:1: entryRuleStyleAttribute returns [EObject current=null] : iv_ruleStyleAttribute= ruleStyleAttribute EOF ;
    public final EObject entryRuleStyleAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStyleAttribute = null;


        try {
            // InternalDotty.g:696:55: (iv_ruleStyleAttribute= ruleStyleAttribute EOF )
            // InternalDotty.g:697:2: iv_ruleStyleAttribute= ruleStyleAttribute EOF
            {
             newCompositeNode(grammarAccess.getStyleAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStyleAttribute=ruleStyleAttribute();

            state._fsp--;

             current =iv_ruleStyleAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStyleAttribute"


    // $ANTLR start "ruleStyleAttribute"
    // InternalDotty.g:703:1: ruleStyleAttribute returns [EObject current=null] : (otherlv_0= 'style' otherlv_1= '=' ( ( (lv_style_2_1= RULE_ID | lv_style_2_2= RULE_STRING ) ) ) ) ;
    public final EObject ruleStyleAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_style_2_1=null;
        Token lv_style_2_2=null;


        	enterRule();

        try {
            // InternalDotty.g:709:2: ( (otherlv_0= 'style' otherlv_1= '=' ( ( (lv_style_2_1= RULE_ID | lv_style_2_2= RULE_STRING ) ) ) ) )
            // InternalDotty.g:710:2: (otherlv_0= 'style' otherlv_1= '=' ( ( (lv_style_2_1= RULE_ID | lv_style_2_2= RULE_STRING ) ) ) )
            {
            // InternalDotty.g:710:2: (otherlv_0= 'style' otherlv_1= '=' ( ( (lv_style_2_1= RULE_ID | lv_style_2_2= RULE_STRING ) ) ) )
            // InternalDotty.g:711:3: otherlv_0= 'style' otherlv_1= '=' ( ( (lv_style_2_1= RULE_ID | lv_style_2_2= RULE_STRING ) ) )
            {
            otherlv_0=(Token)match(input,28,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getStyleAttributeAccess().getStyleKeyword_0());
            		
            otherlv_1=(Token)match(input,21,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getStyleAttributeAccess().getEqualsSignKeyword_1());
            		
            // InternalDotty.g:719:3: ( ( (lv_style_2_1= RULE_ID | lv_style_2_2= RULE_STRING ) ) )
            // InternalDotty.g:720:4: ( (lv_style_2_1= RULE_ID | lv_style_2_2= RULE_STRING ) )
            {
            // InternalDotty.g:720:4: ( (lv_style_2_1= RULE_ID | lv_style_2_2= RULE_STRING ) )
            // InternalDotty.g:721:5: (lv_style_2_1= RULE_ID | lv_style_2_2= RULE_STRING )
            {
            // InternalDotty.g:721:5: (lv_style_2_1= RULE_ID | lv_style_2_2= RULE_STRING )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==RULE_ID) ) {
                alt13=1;
            }
            else if ( (LA13_0==RULE_STRING) ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // InternalDotty.g:722:6: lv_style_2_1= RULE_ID
                    {
                    lv_style_2_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    						newLeafNode(lv_style_2_1, grammarAccess.getStyleAttributeAccess().getStyleIDTerminalRuleCall_2_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getStyleAttributeRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"style",
                    							lv_style_2_1,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }
                    break;
                case 2 :
                    // InternalDotty.g:737:6: lv_style_2_2= RULE_STRING
                    {
                    lv_style_2_2=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_style_2_2, grammarAccess.getStyleAttributeAccess().getStyleSTRINGTerminalRuleCall_2_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getStyleAttributeRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"style",
                    							lv_style_2_2,
                    							"fr.irisa.cairn.graphviz.model.Dotty.STRING");
                    					

                    }
                    break;

            }


            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStyleAttribute"


    // $ANTLR start "entryRuleLabelAttribute"
    // InternalDotty.g:758:1: entryRuleLabelAttribute returns [EObject current=null] : iv_ruleLabelAttribute= ruleLabelAttribute EOF ;
    public final EObject entryRuleLabelAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLabelAttribute = null;


        try {
            // InternalDotty.g:758:55: (iv_ruleLabelAttribute= ruleLabelAttribute EOF )
            // InternalDotty.g:759:2: iv_ruleLabelAttribute= ruleLabelAttribute EOF
            {
             newCompositeNode(grammarAccess.getLabelAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLabelAttribute=ruleLabelAttribute();

            state._fsp--;

             current =iv_ruleLabelAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLabelAttribute"


    // $ANTLR start "ruleLabelAttribute"
    // InternalDotty.g:765:1: ruleLabelAttribute returns [EObject current=null] : (otherlv_0= 'label' otherlv_1= '=' ( (lv_label_2_0= RULE_STRING ) ) ) ;
    public final EObject ruleLabelAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_label_2_0=null;


        	enterRule();

        try {
            // InternalDotty.g:771:2: ( (otherlv_0= 'label' otherlv_1= '=' ( (lv_label_2_0= RULE_STRING ) ) ) )
            // InternalDotty.g:772:2: (otherlv_0= 'label' otherlv_1= '=' ( (lv_label_2_0= RULE_STRING ) ) )
            {
            // InternalDotty.g:772:2: (otherlv_0= 'label' otherlv_1= '=' ( (lv_label_2_0= RULE_STRING ) ) )
            // InternalDotty.g:773:3: otherlv_0= 'label' otherlv_1= '=' ( (lv_label_2_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,29,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getLabelAttributeAccess().getLabelKeyword_0());
            		
            otherlv_1=(Token)match(input,21,FOLLOW_15); 

            			newLeafNode(otherlv_1, grammarAccess.getLabelAttributeAccess().getEqualsSignKeyword_1());
            		
            // InternalDotty.g:781:3: ( (lv_label_2_0= RULE_STRING ) )
            // InternalDotty.g:782:4: (lv_label_2_0= RULE_STRING )
            {
            // InternalDotty.g:782:4: (lv_label_2_0= RULE_STRING )
            // InternalDotty.g:783:5: lv_label_2_0= RULE_STRING
            {
            lv_label_2_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_label_2_0, grammarAccess.getLabelAttributeAccess().getLabelSTRINGTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getLabelAttributeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"label",
            						lv_label_2_0,
            						"fr.irisa.cairn.graphviz.model.Dotty.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLabelAttribute"


    // $ANTLR start "entryRuleRankAttribute"
    // InternalDotty.g:803:1: entryRuleRankAttribute returns [EObject current=null] : iv_ruleRankAttribute= ruleRankAttribute EOF ;
    public final EObject entryRuleRankAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRankAttribute = null;


        try {
            // InternalDotty.g:803:54: (iv_ruleRankAttribute= ruleRankAttribute EOF )
            // InternalDotty.g:804:2: iv_ruleRankAttribute= ruleRankAttribute EOF
            {
             newCompositeNode(grammarAccess.getRankAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRankAttribute=ruleRankAttribute();

            state._fsp--;

             current =iv_ruleRankAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRankAttribute"


    // $ANTLR start "ruleRankAttribute"
    // InternalDotty.g:810:1: ruleRankAttribute returns [EObject current=null] : (otherlv_0= 'rankdir' otherlv_1= '=' ( ( (lv_direction_2_1= 'TB' | lv_direction_2_2= 'LR' | lv_direction_2_3= 'BT' | lv_direction_2_4= 'RL' ) ) ) ) ;
    public final EObject ruleRankAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_direction_2_1=null;
        Token lv_direction_2_2=null;
        Token lv_direction_2_3=null;
        Token lv_direction_2_4=null;


        	enterRule();

        try {
            // InternalDotty.g:816:2: ( (otherlv_0= 'rankdir' otherlv_1= '=' ( ( (lv_direction_2_1= 'TB' | lv_direction_2_2= 'LR' | lv_direction_2_3= 'BT' | lv_direction_2_4= 'RL' ) ) ) ) )
            // InternalDotty.g:817:2: (otherlv_0= 'rankdir' otherlv_1= '=' ( ( (lv_direction_2_1= 'TB' | lv_direction_2_2= 'LR' | lv_direction_2_3= 'BT' | lv_direction_2_4= 'RL' ) ) ) )
            {
            // InternalDotty.g:817:2: (otherlv_0= 'rankdir' otherlv_1= '=' ( ( (lv_direction_2_1= 'TB' | lv_direction_2_2= 'LR' | lv_direction_2_3= 'BT' | lv_direction_2_4= 'RL' ) ) ) )
            // InternalDotty.g:818:3: otherlv_0= 'rankdir' otherlv_1= '=' ( ( (lv_direction_2_1= 'TB' | lv_direction_2_2= 'LR' | lv_direction_2_3= 'BT' | lv_direction_2_4= 'RL' ) ) )
            {
            otherlv_0=(Token)match(input,30,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getRankAttributeAccess().getRankdirKeyword_0());
            		
            otherlv_1=(Token)match(input,21,FOLLOW_16); 

            			newLeafNode(otherlv_1, grammarAccess.getRankAttributeAccess().getEqualsSignKeyword_1());
            		
            // InternalDotty.g:826:3: ( ( (lv_direction_2_1= 'TB' | lv_direction_2_2= 'LR' | lv_direction_2_3= 'BT' | lv_direction_2_4= 'RL' ) ) )
            // InternalDotty.g:827:4: ( (lv_direction_2_1= 'TB' | lv_direction_2_2= 'LR' | lv_direction_2_3= 'BT' | lv_direction_2_4= 'RL' ) )
            {
            // InternalDotty.g:827:4: ( (lv_direction_2_1= 'TB' | lv_direction_2_2= 'LR' | lv_direction_2_3= 'BT' | lv_direction_2_4= 'RL' ) )
            // InternalDotty.g:828:5: (lv_direction_2_1= 'TB' | lv_direction_2_2= 'LR' | lv_direction_2_3= 'BT' | lv_direction_2_4= 'RL' )
            {
            // InternalDotty.g:828:5: (lv_direction_2_1= 'TB' | lv_direction_2_2= 'LR' | lv_direction_2_3= 'BT' | lv_direction_2_4= 'RL' )
            int alt14=4;
            switch ( input.LA(1) ) {
            case 31:
                {
                alt14=1;
                }
                break;
            case 32:
                {
                alt14=2;
                }
                break;
            case 33:
                {
                alt14=3;
                }
                break;
            case 34:
                {
                alt14=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }

            switch (alt14) {
                case 1 :
                    // InternalDotty.g:829:6: lv_direction_2_1= 'TB'
                    {
                    lv_direction_2_1=(Token)match(input,31,FOLLOW_2); 

                    						newLeafNode(lv_direction_2_1, grammarAccess.getRankAttributeAccess().getDirectionTBKeyword_2_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRankAttributeRule());
                    						}
                    						setWithLastConsumed(current, "direction", lv_direction_2_1, null);
                    					

                    }
                    break;
                case 2 :
                    // InternalDotty.g:840:6: lv_direction_2_2= 'LR'
                    {
                    lv_direction_2_2=(Token)match(input,32,FOLLOW_2); 

                    						newLeafNode(lv_direction_2_2, grammarAccess.getRankAttributeAccess().getDirectionLRKeyword_2_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRankAttributeRule());
                    						}
                    						setWithLastConsumed(current, "direction", lv_direction_2_2, null);
                    					

                    }
                    break;
                case 3 :
                    // InternalDotty.g:851:6: lv_direction_2_3= 'BT'
                    {
                    lv_direction_2_3=(Token)match(input,33,FOLLOW_2); 

                    						newLeafNode(lv_direction_2_3, grammarAccess.getRankAttributeAccess().getDirectionBTKeyword_2_0_2());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRankAttributeRule());
                    						}
                    						setWithLastConsumed(current, "direction", lv_direction_2_3, null);
                    					

                    }
                    break;
                case 4 :
                    // InternalDotty.g:862:6: lv_direction_2_4= 'RL'
                    {
                    lv_direction_2_4=(Token)match(input,34,FOLLOW_2); 

                    						newLeafNode(lv_direction_2_4, grammarAccess.getRankAttributeAccess().getDirectionRLKeyword_2_0_3());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRankAttributeRule());
                    						}
                    						setWithLastConsumed(current, "direction", lv_direction_2_4, null);
                    					

                    }
                    break;

            }


            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRankAttribute"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000000007E589010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x000000007E58F010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000080010L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x000000007E58D010L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x000000007E500000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000001800000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000780000000L});

}