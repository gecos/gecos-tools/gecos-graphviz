/**
 * generated by Xtext 2.19.0.M3
 */
package fr.irisa.cairn.graphviz.model.dotty.impl;

import fr.irisa.cairn.graphviz.model.dotty.Attribute;
import fr.irisa.cairn.graphviz.model.dotty.DottyPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AttributeImpl extends MinimalEObjectImpl.Container implements Attribute
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AttributeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return DottyPackage.Literals.ATTRIBUTE;
  }

} //AttributeImpl
