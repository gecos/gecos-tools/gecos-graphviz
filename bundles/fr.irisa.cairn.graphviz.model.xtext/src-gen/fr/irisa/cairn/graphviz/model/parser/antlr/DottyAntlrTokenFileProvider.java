/*
 * generated by Xtext 2.19.0.M3
 */
package fr.irisa.cairn.graphviz.model.parser.antlr;

import java.io.InputStream;
import org.eclipse.xtext.parser.antlr.IAntlrTokenFileProvider;

public class DottyAntlrTokenFileProvider implements IAntlrTokenFileProvider {

	@Override
	public InputStream getAntlrTokenFile() {
		ClassLoader classLoader = getClass().getClassLoader();
		return classLoader.getResourceAsStream("fr/irisa/cairn/graphviz/model/parser/antlr/internal/InternalDotty.tokens");
	}
}
