package fr.irisa.cairn.graphviz.model.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDottyLexer extends Lexer {
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators

    public InternalDottyLexer() {;} 
    public InternalDottyLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalDottyLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "InternalDotty.g"; }

    // $ANTLR start "T__11"
    public final void mT__11() throws RecognitionException {
        try {
            int _type = T__11;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:11:7: ( 'digraph' )
            // InternalDotty.g:11:9: 'digraph'
            {
            match("digraph"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__11"

    // $ANTLR start "T__12"
    public final void mT__12() throws RecognitionException {
        try {
            int _type = T__12;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:12:7: ( '{' )
            // InternalDotty.g:12:9: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__12"

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:13:7: ( '->' )
            // InternalDotty.g:13:9: '->'
            {
            match("->"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:14:7: ( ';' )
            // InternalDotty.g:14:9: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:15:7: ( '}' )
            // InternalDotty.g:15:9: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:16:7: ( '[' )
            // InternalDotty.g:16:9: '['
            {
            match('['); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:17:7: ( ',' )
            // InternalDotty.g:17:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:18:7: ( ']' )
            // InternalDotty.g:18:9: ']'
            {
            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:19:7: ( 'subgraph' )
            // InternalDotty.g:19:9: 'subgraph'
            {
            match("subgraph"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:20:7: ( 'shape' )
            // InternalDotty.g:20:9: 'shape'
            {
            match("shape"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:21:7: ( '=' )
            // InternalDotty.g:21:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:22:7: ( 'compound' )
            // InternalDotty.g:22:9: 'compound'
            {
            match("compound"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:23:7: ( 'true' )
            // InternalDotty.g:23:9: 'true'
            {
            match("true"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:24:7: ( '\\u00A0false' )
            // InternalDotty.g:24:9: '\\u00A0false'
            {
            match("\u00A0false"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:25:7: ( 'color' )
            // InternalDotty.g:25:9: 'color'
            {
            match("color"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:26:7: ( 'fillcolor' )
            // InternalDotty.g:26:9: 'fillcolor'
            {
            match("fillcolor"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:27:7: ( 'fontcolor' )
            // InternalDotty.g:27:9: 'fontcolor'
            {
            match("fontcolor"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:28:7: ( 'style' )
            // InternalDotty.g:28:9: 'style'
            {
            match("style"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:29:7: ( 'label' )
            // InternalDotty.g:29:9: 'label'
            {
            match("label"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:30:7: ( 'rankdir' )
            // InternalDotty.g:30:9: 'rankdir'
            {
            match("rankdir"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:31:7: ( 'TB' )
            // InternalDotty.g:31:9: 'TB'
            {
            match("TB"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:32:7: ( 'LR' )
            // InternalDotty.g:32:9: 'LR'
            {
            match("LR"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:33:7: ( 'BT' )
            // InternalDotty.g:33:9: 'BT'
            {
            match("BT"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:34:7: ( 'RL' )
            // InternalDotty.g:34:9: 'RL'
            {
            match("RL"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:878:13: ( '\"' ( options {greedy=false; } : . )* '\"' )
            // InternalDotty.g:878:15: '\"' ( options {greedy=false; } : . )* '\"'
            {
            match('\"'); 
            // InternalDotty.g:878:19: ( options {greedy=false; } : . )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0=='\"') ) {
                    alt1=2;
                }
                else if ( ((LA1_0>='\u0000' && LA1_0<='!')||(LA1_0>='#' && LA1_0<='\uFFFF')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalDotty.g:878:47: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            match('\"'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:880:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // InternalDotty.g:880:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            // InternalDotty.g:880:11: ( '^' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0=='^') ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalDotty.g:880:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalDotty.g:880:40: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>='0' && LA3_0<='9')||(LA3_0>='A' && LA3_0<='Z')||LA3_0=='_'||(LA3_0>='a' && LA3_0<='z')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalDotty.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:882:10: ( ( '0' .. '9' )+ )
            // InternalDotty.g:882:12: ( '0' .. '9' )+
            {
            // InternalDotty.g:882:12: ( '0' .. '9' )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>='0' && LA4_0<='9')) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalDotty.g:882:13: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:884:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // InternalDotty.g:884:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // InternalDotty.g:884:24: ( options {greedy=false; } : . )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0=='*') ) {
                    int LA5_1 = input.LA(2);

                    if ( (LA5_1=='/') ) {
                        alt5=2;
                    }
                    else if ( ((LA5_1>='\u0000' && LA5_1<='.')||(LA5_1>='0' && LA5_1<='\uFFFF')) ) {
                        alt5=1;
                    }


                }
                else if ( ((LA5_0>='\u0000' && LA5_0<=')')||(LA5_0>='+' && LA5_0<='\uFFFF')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalDotty.g:884:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:886:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // InternalDotty.g:886:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // InternalDotty.g:886:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>='\u0000' && LA6_0<='\t')||(LA6_0>='\u000B' && LA6_0<='\f')||(LA6_0>='\u000E' && LA6_0<='\uFFFF')) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalDotty.g:886:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            // InternalDotty.g:886:40: ( ( '\\r' )? '\\n' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0=='\n'||LA8_0=='\r') ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalDotty.g:886:41: ( '\\r' )? '\\n'
                    {
                    // InternalDotty.g:886:41: ( '\\r' )?
                    int alt7=2;
                    int LA7_0 = input.LA(1);

                    if ( (LA7_0=='\r') ) {
                        alt7=1;
                    }
                    switch (alt7) {
                        case 1 :
                            // InternalDotty.g:886:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:888:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // InternalDotty.g:888:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // InternalDotty.g:888:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt9=0;
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0>='\t' && LA9_0<='\n')||LA9_0=='\r'||LA9_0==' ') ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalDotty.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt9 >= 1 ) break loop9;
                        EarlyExitException eee =
                            new EarlyExitException(9, input);
                        throw eee;
                }
                cnt9++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalDotty.g:890:16: ( . )
            // InternalDotty.g:890:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // InternalDotty.g:1:8: ( T__11 | T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | RULE_STRING | RULE_ID | RULE_INT | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt10=31;
        alt10 = dfa10.predict(input);
        switch (alt10) {
            case 1 :
                // InternalDotty.g:1:10: T__11
                {
                mT__11(); 

                }
                break;
            case 2 :
                // InternalDotty.g:1:16: T__12
                {
                mT__12(); 

                }
                break;
            case 3 :
                // InternalDotty.g:1:22: T__13
                {
                mT__13(); 

                }
                break;
            case 4 :
                // InternalDotty.g:1:28: T__14
                {
                mT__14(); 

                }
                break;
            case 5 :
                // InternalDotty.g:1:34: T__15
                {
                mT__15(); 

                }
                break;
            case 6 :
                // InternalDotty.g:1:40: T__16
                {
                mT__16(); 

                }
                break;
            case 7 :
                // InternalDotty.g:1:46: T__17
                {
                mT__17(); 

                }
                break;
            case 8 :
                // InternalDotty.g:1:52: T__18
                {
                mT__18(); 

                }
                break;
            case 9 :
                // InternalDotty.g:1:58: T__19
                {
                mT__19(); 

                }
                break;
            case 10 :
                // InternalDotty.g:1:64: T__20
                {
                mT__20(); 

                }
                break;
            case 11 :
                // InternalDotty.g:1:70: T__21
                {
                mT__21(); 

                }
                break;
            case 12 :
                // InternalDotty.g:1:76: T__22
                {
                mT__22(); 

                }
                break;
            case 13 :
                // InternalDotty.g:1:82: T__23
                {
                mT__23(); 

                }
                break;
            case 14 :
                // InternalDotty.g:1:88: T__24
                {
                mT__24(); 

                }
                break;
            case 15 :
                // InternalDotty.g:1:94: T__25
                {
                mT__25(); 

                }
                break;
            case 16 :
                // InternalDotty.g:1:100: T__26
                {
                mT__26(); 

                }
                break;
            case 17 :
                // InternalDotty.g:1:106: T__27
                {
                mT__27(); 

                }
                break;
            case 18 :
                // InternalDotty.g:1:112: T__28
                {
                mT__28(); 

                }
                break;
            case 19 :
                // InternalDotty.g:1:118: T__29
                {
                mT__29(); 

                }
                break;
            case 20 :
                // InternalDotty.g:1:124: T__30
                {
                mT__30(); 

                }
                break;
            case 21 :
                // InternalDotty.g:1:130: T__31
                {
                mT__31(); 

                }
                break;
            case 22 :
                // InternalDotty.g:1:136: T__32
                {
                mT__32(); 

                }
                break;
            case 23 :
                // InternalDotty.g:1:142: T__33
                {
                mT__33(); 

                }
                break;
            case 24 :
                // InternalDotty.g:1:148: T__34
                {
                mT__34(); 

                }
                break;
            case 25 :
                // InternalDotty.g:1:154: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 26 :
                // InternalDotty.g:1:166: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 27 :
                // InternalDotty.g:1:174: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 28 :
                // InternalDotty.g:1:183: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 29 :
                // InternalDotty.g:1:199: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 30 :
                // InternalDotty.g:1:215: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 31 :
                // InternalDotty.g:1:223: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA10 dfa10 = new DFA10(this);
    static final String DFA10_eotS =
        "\1\uffff\1\35\1\uffff\1\33\5\uffff\1\35\1\uffff\2\35\1\33\7\35\2\33\2\uffff\1\33\2\uffff\1\35\10\uffff\3\35\1\uffff\2\35\1\uffff\4\35\1\104\1\105\1\106\1\107\5\uffff\13\35\4\uffff\6\35\1\131\6\35\1\140\1\141\1\35\1\143\1\uffff\2\35\1\146\3\35\2\uffff\1\35\1\uffff\2\35\1\uffff\1\35\1\156\4\35\1\163\1\uffff\1\164\1\165\2\35\3\uffff\1\170\1\171\2\uffff";
    static final String DFA10_eofS =
        "\172\uffff";
    static final String DFA10_minS =
        "\1\0\1\151\1\uffff\1\76\5\uffff\1\150\1\uffff\1\157\1\162\1\146\1\151\2\141\1\102\1\122\1\124\1\114\1\0\1\101\2\uffff\1\52\2\uffff\1\147\10\uffff\1\142\1\141\1\171\1\uffff\1\154\1\165\1\uffff\1\154\1\156\1\142\1\156\4\60\5\uffff\1\162\1\147\1\160\1\154\1\160\1\157\1\145\1\154\1\164\1\145\1\153\4\uffff\1\141\1\162\2\145\1\157\1\162\1\60\2\143\1\154\1\144\1\160\1\141\2\60\1\165\1\60\1\uffff\2\157\1\60\1\151\1\150\1\160\2\uffff\1\156\1\uffff\2\154\1\uffff\1\162\1\60\1\150\1\144\2\157\1\60\1\uffff\2\60\2\162\3\uffff\2\60\2\uffff";
    static final String DFA10_maxS =
        "\1\uffff\1\151\1\uffff\1\76\5\uffff\1\165\1\uffff\1\157\1\162\1\146\1\157\2\141\1\102\1\122\1\124\1\114\1\uffff\1\172\2\uffff\1\57\2\uffff\1\147\10\uffff\1\142\1\141\1\171\1\uffff\1\155\1\165\1\uffff\1\154\1\156\1\142\1\156\4\172\5\uffff\1\162\1\147\1\160\1\154\1\160\1\157\1\145\1\154\1\164\1\145\1\153\4\uffff\1\141\1\162\2\145\1\157\1\162\1\172\2\143\1\154\1\144\1\160\1\141\2\172\1\165\1\172\1\uffff\2\157\1\172\1\151\1\150\1\160\2\uffff\1\156\1\uffff\2\154\1\uffff\1\162\1\172\1\150\1\144\2\157\1\172\1\uffff\2\172\2\162\3\uffff\2\172\2\uffff";
    static final String DFA10_acceptS =
        "\2\uffff\1\2\1\uffff\1\4\1\5\1\6\1\7\1\10\1\uffff\1\13\14\uffff\1\32\1\33\1\uffff\1\36\1\37\1\uffff\1\32\1\2\1\3\1\4\1\5\1\6\1\7\1\10\3\uffff\1\13\2\uffff\1\16\10\uffff\1\31\1\33\1\34\1\35\1\36\13\uffff\1\25\1\26\1\27\1\30\21\uffff\1\15\6\uffff\1\12\1\22\1\uffff\1\17\2\uffff\1\23\7\uffff\1\1\4\uffff\1\24\1\11\1\14\2\uffff\1\20\1\21";
    static final String DFA10_specialS =
        "\1\1\24\uffff\1\0\144\uffff}>";
    static final String[] DFA10_transitionS = {
            "\11\33\2\32\2\33\1\32\22\33\1\32\1\33\1\25\11\33\1\7\1\3\1\33\1\31\12\30\1\33\1\4\1\33\1\12\3\33\1\27\1\23\11\27\1\22\5\27\1\24\1\27\1\21\6\27\1\6\1\33\1\10\1\26\1\27\1\33\2\27\1\13\1\1\1\27\1\16\5\27\1\17\5\27\1\20\1\11\1\14\6\27\1\2\1\33\1\5\42\33\1\15\uff5f\33",
            "\1\34",
            "",
            "\1\37",
            "",
            "",
            "",
            "",
            "",
            "\1\46\13\uffff\1\47\1\45",
            "",
            "\1\51",
            "\1\52",
            "\1\53",
            "\1\54\5\uffff\1\55",
            "\1\56",
            "\1\57",
            "\1\60",
            "\1\61",
            "\1\62",
            "\1\63",
            "\0\64",
            "\32\35\4\uffff\1\35\1\uffff\32\35",
            "",
            "",
            "\1\66\4\uffff\1\67",
            "",
            "",
            "\1\71",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\72",
            "\1\73",
            "\1\74",
            "",
            "\1\76\1\75",
            "\1\77",
            "",
            "\1\100",
            "\1\101",
            "\1\102",
            "\1\103",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "",
            "",
            "",
            "",
            "",
            "\1\110",
            "\1\111",
            "\1\112",
            "\1\113",
            "\1\114",
            "\1\115",
            "\1\116",
            "\1\117",
            "\1\120",
            "\1\121",
            "\1\122",
            "",
            "",
            "",
            "",
            "\1\123",
            "\1\124",
            "\1\125",
            "\1\126",
            "\1\127",
            "\1\130",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\1\132",
            "\1\133",
            "\1\134",
            "\1\135",
            "\1\136",
            "\1\137",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\1\142",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "",
            "\1\144",
            "\1\145",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\1\147",
            "\1\150",
            "\1\151",
            "",
            "",
            "\1\152",
            "",
            "\1\153",
            "\1\154",
            "",
            "\1\155",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\1\157",
            "\1\160",
            "\1\161",
            "\1\162",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\1\166",
            "\1\167",
            "",
            "",
            "",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32\35",
            "",
            ""
    };

    static final short[] DFA10_eot = DFA.unpackEncodedString(DFA10_eotS);
    static final short[] DFA10_eof = DFA.unpackEncodedString(DFA10_eofS);
    static final char[] DFA10_min = DFA.unpackEncodedStringToUnsignedChars(DFA10_minS);
    static final char[] DFA10_max = DFA.unpackEncodedStringToUnsignedChars(DFA10_maxS);
    static final short[] DFA10_accept = DFA.unpackEncodedString(DFA10_acceptS);
    static final short[] DFA10_special = DFA.unpackEncodedString(DFA10_specialS);
    static final short[][] DFA10_transition;

    static {
        int numStates = DFA10_transitionS.length;
        DFA10_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA10_transition[i] = DFA.unpackEncodedString(DFA10_transitionS[i]);
        }
    }

    class DFA10 extends DFA {

        public DFA10(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 10;
            this.eot = DFA10_eot;
            this.eof = DFA10_eof;
            this.min = DFA10_min;
            this.max = DFA10_max;
            this.accept = DFA10_accept;
            this.special = DFA10_special;
            this.transition = DFA10_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__11 | T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | RULE_STRING | RULE_ID | RULE_INT | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA10_21 = input.LA(1);

                        s = -1;
                        if ( ((LA10_21>='\u0000' && LA10_21<='\uFFFF')) ) {s = 52;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA10_0 = input.LA(1);

                        s = -1;
                        if ( (LA10_0=='d') ) {s = 1;}

                        else if ( (LA10_0=='{') ) {s = 2;}

                        else if ( (LA10_0=='-') ) {s = 3;}

                        else if ( (LA10_0==';') ) {s = 4;}

                        else if ( (LA10_0=='}') ) {s = 5;}

                        else if ( (LA10_0=='[') ) {s = 6;}

                        else if ( (LA10_0==',') ) {s = 7;}

                        else if ( (LA10_0==']') ) {s = 8;}

                        else if ( (LA10_0=='s') ) {s = 9;}

                        else if ( (LA10_0=='=') ) {s = 10;}

                        else if ( (LA10_0=='c') ) {s = 11;}

                        else if ( (LA10_0=='t') ) {s = 12;}

                        else if ( (LA10_0=='\u00A0') ) {s = 13;}

                        else if ( (LA10_0=='f') ) {s = 14;}

                        else if ( (LA10_0=='l') ) {s = 15;}

                        else if ( (LA10_0=='r') ) {s = 16;}

                        else if ( (LA10_0=='T') ) {s = 17;}

                        else if ( (LA10_0=='L') ) {s = 18;}

                        else if ( (LA10_0=='B') ) {s = 19;}

                        else if ( (LA10_0=='R') ) {s = 20;}

                        else if ( (LA10_0=='\"') ) {s = 21;}

                        else if ( (LA10_0=='^') ) {s = 22;}

                        else if ( (LA10_0=='A'||(LA10_0>='C' && LA10_0<='K')||(LA10_0>='M' && LA10_0<='Q')||LA10_0=='S'||(LA10_0>='U' && LA10_0<='Z')||LA10_0=='_'||(LA10_0>='a' && LA10_0<='b')||LA10_0=='e'||(LA10_0>='g' && LA10_0<='k')||(LA10_0>='m' && LA10_0<='q')||(LA10_0>='u' && LA10_0<='z')) ) {s = 23;}

                        else if ( ((LA10_0>='0' && LA10_0<='9')) ) {s = 24;}

                        else if ( (LA10_0=='/') ) {s = 25;}

                        else if ( ((LA10_0>='\t' && LA10_0<='\n')||LA10_0=='\r'||LA10_0==' ') ) {s = 26;}

                        else if ( ((LA10_0>='\u0000' && LA10_0<='\b')||(LA10_0>='\u000B' && LA10_0<='\f')||(LA10_0>='\u000E' && LA10_0<='\u001F')||LA10_0=='!'||(LA10_0>='#' && LA10_0<='+')||LA10_0=='.'||LA10_0==':'||LA10_0=='<'||(LA10_0>='>' && LA10_0<='@')||LA10_0=='\\'||LA10_0=='`'||LA10_0=='|'||(LA10_0>='~' && LA10_0<='\u009F')||(LA10_0>='\u00A1' && LA10_0<='\uFFFF')) ) {s = 27;}

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 10, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}