/**
 * generated by Xtext 2.19.0.M3
 */
package fr.irisa.cairn.graphviz.model.dotty.util;

import fr.irisa.cairn.graphviz.model.dotty.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage
 * @generated
 */
public class DottyAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static DottyPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DottyAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = DottyPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DottySwitch<Adapter> modelSwitch =
    new DottySwitch<Adapter>()
    {
      @Override
      public Adapter caseGraph(Graph object)
      {
        return createGraphAdapter();
      }
      @Override
      public Adapter caseCompound(Compound object)
      {
        return createCompoundAdapter();
      }
      @Override
      public Adapter caseNode(Node object)
      {
        return createNodeAdapter();
      }
      @Override
      public Adapter caseLeafNode(LeafNode object)
      {
        return createLeafNodeAdapter();
      }
      @Override
      public Adapter caseSubNode(SubNode object)
      {
        return createSubNodeAdapter();
      }
      @Override
      public Adapter caseAttribute(Attribute object)
      {
        return createAttributeAdapter();
      }
      @Override
      public Adapter caseShapeAttribute(ShapeAttribute object)
      {
        return createShapeAttributeAdapter();
      }
      @Override
      public Adapter caseCompoundAttribute(CompoundAttribute object)
      {
        return createCompoundAttributeAdapter();
      }
      @Override
      public Adapter caseColorAttribute(ColorAttribute object)
      {
        return createColorAttributeAdapter();
      }
      @Override
      public Adapter caseStyleAttribute(StyleAttribute object)
      {
        return createStyleAttributeAdapter();
      }
      @Override
      public Adapter caseLabelAttribute(LabelAttribute object)
      {
        return createLabelAttributeAdapter();
      }
      @Override
      public Adapter caseRankAttribute(RankAttribute object)
      {
        return createRankAttributeAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.graphviz.model.dotty.Graph <em>Graph</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.graphviz.model.dotty.Graph
   * @generated
   */
  public Adapter createGraphAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.graphviz.model.dotty.Compound <em>Compound</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.graphviz.model.dotty.Compound
   * @generated
   */
  public Adapter createCompoundAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.graphviz.model.dotty.Node <em>Node</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.graphviz.model.dotty.Node
   * @generated
   */
  public Adapter createNodeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.graphviz.model.dotty.LeafNode <em>Leaf Node</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.graphviz.model.dotty.LeafNode
   * @generated
   */
  public Adapter createLeafNodeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.graphviz.model.dotty.SubNode <em>Sub Node</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.graphviz.model.dotty.SubNode
   * @generated
   */
  public Adapter createSubNodeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.graphviz.model.dotty.Attribute <em>Attribute</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.graphviz.model.dotty.Attribute
   * @generated
   */
  public Adapter createAttributeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.graphviz.model.dotty.ShapeAttribute <em>Shape Attribute</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.graphviz.model.dotty.ShapeAttribute
   * @generated
   */
  public Adapter createShapeAttributeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.graphviz.model.dotty.CompoundAttribute <em>Compound Attribute</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.graphviz.model.dotty.CompoundAttribute
   * @generated
   */
  public Adapter createCompoundAttributeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.graphviz.model.dotty.ColorAttribute <em>Color Attribute</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.graphviz.model.dotty.ColorAttribute
   * @generated
   */
  public Adapter createColorAttributeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.graphviz.model.dotty.StyleAttribute <em>Style Attribute</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.graphviz.model.dotty.StyleAttribute
   * @generated
   */
  public Adapter createStyleAttributeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.graphviz.model.dotty.LabelAttribute <em>Label Attribute</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.graphviz.model.dotty.LabelAttribute
   * @generated
   */
  public Adapter createLabelAttributeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.graphviz.model.dotty.RankAttribute <em>Rank Attribute</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.graphviz.model.dotty.RankAttribute
   * @generated
   */
  public Adapter createRankAttributeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //DottyAdapterFactory
