/**
 * generated by Xtext 2.19.0.M3
 */
package fr.irisa.cairn.graphviz.model.dotty;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage#getAttribute()
 * @model
 * @generated
 */
public interface Attribute extends EObject
{
} // Attribute
