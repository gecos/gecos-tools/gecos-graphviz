package fr.irisa.cairn.graphviz.model.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import fr.irisa.cairn.graphviz.model.services.DottyGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDottyParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'true'", "'\\u00A0false'", "'color'", "'fillcolor'", "'fontcolor'", "'TB'", "'LR'", "'BT'", "'RL'", "'digraph'", "'{'", "'}'", "';'", "'->'", "'['", "']'", "','", "'subgraph'", "'shape'", "'='", "'compound'", "'style'", "'label'", "'rankdir'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalDottyParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDottyParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDottyParser.tokenNames; }
    public String getGrammarFileName() { return "InternalDotty.g"; }


    	private DottyGrammarAccess grammarAccess;

    	public void setGrammarAccess(DottyGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleGraph"
    // InternalDotty.g:53:1: entryRuleGraph : ruleGraph EOF ;
    public final void entryRuleGraph() throws RecognitionException {
        try {
            // InternalDotty.g:54:1: ( ruleGraph EOF )
            // InternalDotty.g:55:1: ruleGraph EOF
            {
             before(grammarAccess.getGraphRule()); 
            pushFollow(FOLLOW_1);
            ruleGraph();

            state._fsp--;

             after(grammarAccess.getGraphRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGraph"


    // $ANTLR start "ruleGraph"
    // InternalDotty.g:62:1: ruleGraph : ( ( rule__Graph__Group__0 ) ) ;
    public final void ruleGraph() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:66:2: ( ( ( rule__Graph__Group__0 ) ) )
            // InternalDotty.g:67:2: ( ( rule__Graph__Group__0 ) )
            {
            // InternalDotty.g:67:2: ( ( rule__Graph__Group__0 ) )
            // InternalDotty.g:68:3: ( rule__Graph__Group__0 )
            {
             before(grammarAccess.getGraphAccess().getGroup()); 
            // InternalDotty.g:69:3: ( rule__Graph__Group__0 )
            // InternalDotty.g:69:4: rule__Graph__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Graph__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getGraphAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGraph"


    // $ANTLR start "entryRuleCompound"
    // InternalDotty.g:78:1: entryRuleCompound : ruleCompound EOF ;
    public final void entryRuleCompound() throws RecognitionException {
        try {
            // InternalDotty.g:79:1: ( ruleCompound EOF )
            // InternalDotty.g:80:1: ruleCompound EOF
            {
             before(grammarAccess.getCompoundRule()); 
            pushFollow(FOLLOW_1);
            ruleCompound();

            state._fsp--;

             after(grammarAccess.getCompoundRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCompound"


    // $ANTLR start "ruleCompound"
    // InternalDotty.g:87:1: ruleCompound : ( ( rule__Compound__Group__0 ) ) ;
    public final void ruleCompound() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:91:2: ( ( ( rule__Compound__Group__0 ) ) )
            // InternalDotty.g:92:2: ( ( rule__Compound__Group__0 ) )
            {
            // InternalDotty.g:92:2: ( ( rule__Compound__Group__0 ) )
            // InternalDotty.g:93:3: ( rule__Compound__Group__0 )
            {
             before(grammarAccess.getCompoundAccess().getGroup()); 
            // InternalDotty.g:94:3: ( rule__Compound__Group__0 )
            // InternalDotty.g:94:4: rule__Compound__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Compound__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCompoundAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCompound"


    // $ANTLR start "entryRuleNode"
    // InternalDotty.g:103:1: entryRuleNode : ruleNode EOF ;
    public final void entryRuleNode() throws RecognitionException {
        try {
            // InternalDotty.g:104:1: ( ruleNode EOF )
            // InternalDotty.g:105:1: ruleNode EOF
            {
             before(grammarAccess.getNodeRule()); 
            pushFollow(FOLLOW_1);
            ruleNode();

            state._fsp--;

             after(grammarAccess.getNodeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNode"


    // $ANTLR start "ruleNode"
    // InternalDotty.g:112:1: ruleNode : ( ( rule__Node__Alternatives ) ) ;
    public final void ruleNode() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:116:2: ( ( ( rule__Node__Alternatives ) ) )
            // InternalDotty.g:117:2: ( ( rule__Node__Alternatives ) )
            {
            // InternalDotty.g:117:2: ( ( rule__Node__Alternatives ) )
            // InternalDotty.g:118:3: ( rule__Node__Alternatives )
            {
             before(grammarAccess.getNodeAccess().getAlternatives()); 
            // InternalDotty.g:119:3: ( rule__Node__Alternatives )
            // InternalDotty.g:119:4: rule__Node__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Node__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getNodeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNode"


    // $ANTLR start "entryRuleLeafNode"
    // InternalDotty.g:128:1: entryRuleLeafNode : ruleLeafNode EOF ;
    public final void entryRuleLeafNode() throws RecognitionException {
        try {
            // InternalDotty.g:129:1: ( ruleLeafNode EOF )
            // InternalDotty.g:130:1: ruleLeafNode EOF
            {
             before(grammarAccess.getLeafNodeRule()); 
            pushFollow(FOLLOW_1);
            ruleLeafNode();

            state._fsp--;

             after(grammarAccess.getLeafNodeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLeafNode"


    // $ANTLR start "ruleLeafNode"
    // InternalDotty.g:137:1: ruleLeafNode : ( ( rule__LeafNode__Group__0 ) ) ;
    public final void ruleLeafNode() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:141:2: ( ( ( rule__LeafNode__Group__0 ) ) )
            // InternalDotty.g:142:2: ( ( rule__LeafNode__Group__0 ) )
            {
            // InternalDotty.g:142:2: ( ( rule__LeafNode__Group__0 ) )
            // InternalDotty.g:143:3: ( rule__LeafNode__Group__0 )
            {
             before(grammarAccess.getLeafNodeAccess().getGroup()); 
            // InternalDotty.g:144:3: ( rule__LeafNode__Group__0 )
            // InternalDotty.g:144:4: rule__LeafNode__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LeafNode__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLeafNodeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLeafNode"


    // $ANTLR start "entryRuleSubNode"
    // InternalDotty.g:153:1: entryRuleSubNode : ruleSubNode EOF ;
    public final void entryRuleSubNode() throws RecognitionException {
        try {
            // InternalDotty.g:154:1: ( ruleSubNode EOF )
            // InternalDotty.g:155:1: ruleSubNode EOF
            {
             before(grammarAccess.getSubNodeRule()); 
            pushFollow(FOLLOW_1);
            ruleSubNode();

            state._fsp--;

             after(grammarAccess.getSubNodeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSubNode"


    // $ANTLR start "ruleSubNode"
    // InternalDotty.g:162:1: ruleSubNode : ( ( rule__SubNode__Group__0 ) ) ;
    public final void ruleSubNode() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:166:2: ( ( ( rule__SubNode__Group__0 ) ) )
            // InternalDotty.g:167:2: ( ( rule__SubNode__Group__0 ) )
            {
            // InternalDotty.g:167:2: ( ( rule__SubNode__Group__0 ) )
            // InternalDotty.g:168:3: ( rule__SubNode__Group__0 )
            {
             before(grammarAccess.getSubNodeAccess().getGroup()); 
            // InternalDotty.g:169:3: ( rule__SubNode__Group__0 )
            // InternalDotty.g:169:4: rule__SubNode__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SubNode__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSubNodeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSubNode"


    // $ANTLR start "entryRuleAttribute"
    // InternalDotty.g:178:1: entryRuleAttribute : ruleAttribute EOF ;
    public final void entryRuleAttribute() throws RecognitionException {
        try {
            // InternalDotty.g:179:1: ( ruleAttribute EOF )
            // InternalDotty.g:180:1: ruleAttribute EOF
            {
             before(grammarAccess.getAttributeRule()); 
            pushFollow(FOLLOW_1);
            ruleAttribute();

            state._fsp--;

             after(grammarAccess.getAttributeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // InternalDotty.g:187:1: ruleAttribute : ( ( rule__Attribute__Alternatives ) ) ;
    public final void ruleAttribute() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:191:2: ( ( ( rule__Attribute__Alternatives ) ) )
            // InternalDotty.g:192:2: ( ( rule__Attribute__Alternatives ) )
            {
            // InternalDotty.g:192:2: ( ( rule__Attribute__Alternatives ) )
            // InternalDotty.g:193:3: ( rule__Attribute__Alternatives )
            {
             before(grammarAccess.getAttributeAccess().getAlternatives()); 
            // InternalDotty.g:194:3: ( rule__Attribute__Alternatives )
            // InternalDotty.g:194:4: rule__Attribute__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Attribute__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRuleShapeAttribute"
    // InternalDotty.g:203:1: entryRuleShapeAttribute : ruleShapeAttribute EOF ;
    public final void entryRuleShapeAttribute() throws RecognitionException {
        try {
            // InternalDotty.g:204:1: ( ruleShapeAttribute EOF )
            // InternalDotty.g:205:1: ruleShapeAttribute EOF
            {
             before(grammarAccess.getShapeAttributeRule()); 
            pushFollow(FOLLOW_1);
            ruleShapeAttribute();

            state._fsp--;

             after(grammarAccess.getShapeAttributeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleShapeAttribute"


    // $ANTLR start "ruleShapeAttribute"
    // InternalDotty.g:212:1: ruleShapeAttribute : ( ( rule__ShapeAttribute__Group__0 ) ) ;
    public final void ruleShapeAttribute() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:216:2: ( ( ( rule__ShapeAttribute__Group__0 ) ) )
            // InternalDotty.g:217:2: ( ( rule__ShapeAttribute__Group__0 ) )
            {
            // InternalDotty.g:217:2: ( ( rule__ShapeAttribute__Group__0 ) )
            // InternalDotty.g:218:3: ( rule__ShapeAttribute__Group__0 )
            {
             before(grammarAccess.getShapeAttributeAccess().getGroup()); 
            // InternalDotty.g:219:3: ( rule__ShapeAttribute__Group__0 )
            // InternalDotty.g:219:4: rule__ShapeAttribute__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ShapeAttribute__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getShapeAttributeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleShapeAttribute"


    // $ANTLR start "entryRuleCompoundAttribute"
    // InternalDotty.g:228:1: entryRuleCompoundAttribute : ruleCompoundAttribute EOF ;
    public final void entryRuleCompoundAttribute() throws RecognitionException {
        try {
            // InternalDotty.g:229:1: ( ruleCompoundAttribute EOF )
            // InternalDotty.g:230:1: ruleCompoundAttribute EOF
            {
             before(grammarAccess.getCompoundAttributeRule()); 
            pushFollow(FOLLOW_1);
            ruleCompoundAttribute();

            state._fsp--;

             after(grammarAccess.getCompoundAttributeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCompoundAttribute"


    // $ANTLR start "ruleCompoundAttribute"
    // InternalDotty.g:237:1: ruleCompoundAttribute : ( ( rule__CompoundAttribute__Group__0 ) ) ;
    public final void ruleCompoundAttribute() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:241:2: ( ( ( rule__CompoundAttribute__Group__0 ) ) )
            // InternalDotty.g:242:2: ( ( rule__CompoundAttribute__Group__0 ) )
            {
            // InternalDotty.g:242:2: ( ( rule__CompoundAttribute__Group__0 ) )
            // InternalDotty.g:243:3: ( rule__CompoundAttribute__Group__0 )
            {
             before(grammarAccess.getCompoundAttributeAccess().getGroup()); 
            // InternalDotty.g:244:3: ( rule__CompoundAttribute__Group__0 )
            // InternalDotty.g:244:4: rule__CompoundAttribute__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CompoundAttribute__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCompoundAttributeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCompoundAttribute"


    // $ANTLR start "entryRuleColorAttribute"
    // InternalDotty.g:253:1: entryRuleColorAttribute : ruleColorAttribute EOF ;
    public final void entryRuleColorAttribute() throws RecognitionException {
        try {
            // InternalDotty.g:254:1: ( ruleColorAttribute EOF )
            // InternalDotty.g:255:1: ruleColorAttribute EOF
            {
             before(grammarAccess.getColorAttributeRule()); 
            pushFollow(FOLLOW_1);
            ruleColorAttribute();

            state._fsp--;

             after(grammarAccess.getColorAttributeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleColorAttribute"


    // $ANTLR start "ruleColorAttribute"
    // InternalDotty.g:262:1: ruleColorAttribute : ( ( rule__ColorAttribute__Group__0 ) ) ;
    public final void ruleColorAttribute() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:266:2: ( ( ( rule__ColorAttribute__Group__0 ) ) )
            // InternalDotty.g:267:2: ( ( rule__ColorAttribute__Group__0 ) )
            {
            // InternalDotty.g:267:2: ( ( rule__ColorAttribute__Group__0 ) )
            // InternalDotty.g:268:3: ( rule__ColorAttribute__Group__0 )
            {
             before(grammarAccess.getColorAttributeAccess().getGroup()); 
            // InternalDotty.g:269:3: ( rule__ColorAttribute__Group__0 )
            // InternalDotty.g:269:4: rule__ColorAttribute__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ColorAttribute__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getColorAttributeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleColorAttribute"


    // $ANTLR start "entryRuleStyleAttribute"
    // InternalDotty.g:278:1: entryRuleStyleAttribute : ruleStyleAttribute EOF ;
    public final void entryRuleStyleAttribute() throws RecognitionException {
        try {
            // InternalDotty.g:279:1: ( ruleStyleAttribute EOF )
            // InternalDotty.g:280:1: ruleStyleAttribute EOF
            {
             before(grammarAccess.getStyleAttributeRule()); 
            pushFollow(FOLLOW_1);
            ruleStyleAttribute();

            state._fsp--;

             after(grammarAccess.getStyleAttributeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStyleAttribute"


    // $ANTLR start "ruleStyleAttribute"
    // InternalDotty.g:287:1: ruleStyleAttribute : ( ( rule__StyleAttribute__Group__0 ) ) ;
    public final void ruleStyleAttribute() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:291:2: ( ( ( rule__StyleAttribute__Group__0 ) ) )
            // InternalDotty.g:292:2: ( ( rule__StyleAttribute__Group__0 ) )
            {
            // InternalDotty.g:292:2: ( ( rule__StyleAttribute__Group__0 ) )
            // InternalDotty.g:293:3: ( rule__StyleAttribute__Group__0 )
            {
             before(grammarAccess.getStyleAttributeAccess().getGroup()); 
            // InternalDotty.g:294:3: ( rule__StyleAttribute__Group__0 )
            // InternalDotty.g:294:4: rule__StyleAttribute__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__StyleAttribute__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStyleAttributeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStyleAttribute"


    // $ANTLR start "entryRuleLabelAttribute"
    // InternalDotty.g:303:1: entryRuleLabelAttribute : ruleLabelAttribute EOF ;
    public final void entryRuleLabelAttribute() throws RecognitionException {
        try {
            // InternalDotty.g:304:1: ( ruleLabelAttribute EOF )
            // InternalDotty.g:305:1: ruleLabelAttribute EOF
            {
             before(grammarAccess.getLabelAttributeRule()); 
            pushFollow(FOLLOW_1);
            ruleLabelAttribute();

            state._fsp--;

             after(grammarAccess.getLabelAttributeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLabelAttribute"


    // $ANTLR start "ruleLabelAttribute"
    // InternalDotty.g:312:1: ruleLabelAttribute : ( ( rule__LabelAttribute__Group__0 ) ) ;
    public final void ruleLabelAttribute() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:316:2: ( ( ( rule__LabelAttribute__Group__0 ) ) )
            // InternalDotty.g:317:2: ( ( rule__LabelAttribute__Group__0 ) )
            {
            // InternalDotty.g:317:2: ( ( rule__LabelAttribute__Group__0 ) )
            // InternalDotty.g:318:3: ( rule__LabelAttribute__Group__0 )
            {
             before(grammarAccess.getLabelAttributeAccess().getGroup()); 
            // InternalDotty.g:319:3: ( rule__LabelAttribute__Group__0 )
            // InternalDotty.g:319:4: rule__LabelAttribute__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LabelAttribute__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLabelAttributeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLabelAttribute"


    // $ANTLR start "entryRuleRankAttribute"
    // InternalDotty.g:328:1: entryRuleRankAttribute : ruleRankAttribute EOF ;
    public final void entryRuleRankAttribute() throws RecognitionException {
        try {
            // InternalDotty.g:329:1: ( ruleRankAttribute EOF )
            // InternalDotty.g:330:1: ruleRankAttribute EOF
            {
             before(grammarAccess.getRankAttributeRule()); 
            pushFollow(FOLLOW_1);
            ruleRankAttribute();

            state._fsp--;

             after(grammarAccess.getRankAttributeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRankAttribute"


    // $ANTLR start "ruleRankAttribute"
    // InternalDotty.g:337:1: ruleRankAttribute : ( ( rule__RankAttribute__Group__0 ) ) ;
    public final void ruleRankAttribute() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:341:2: ( ( ( rule__RankAttribute__Group__0 ) ) )
            // InternalDotty.g:342:2: ( ( rule__RankAttribute__Group__0 ) )
            {
            // InternalDotty.g:342:2: ( ( rule__RankAttribute__Group__0 ) )
            // InternalDotty.g:343:3: ( rule__RankAttribute__Group__0 )
            {
             before(grammarAccess.getRankAttributeAccess().getGroup()); 
            // InternalDotty.g:344:3: ( rule__RankAttribute__Group__0 )
            // InternalDotty.g:344:4: rule__RankAttribute__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__RankAttribute__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRankAttributeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRankAttribute"


    // $ANTLR start "rule__Compound__Alternatives_1_0"
    // InternalDotty.g:352:1: rule__Compound__Alternatives_1_0 : ( ( ( rule__Compound__Group_1_0_0__0 ) ) | ( ( rule__Compound__AttributesAssignment_1_0_1 ) ) | ( ( rule__Compound__SubAssignment_1_0_2 ) ) );
    public final void rule__Compound__Alternatives_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:356:1: ( ( ( rule__Compound__Group_1_0_0__0 ) ) | ( ( rule__Compound__AttributesAssignment_1_0_1 ) ) | ( ( rule__Compound__SubAssignment_1_0_2 ) ) )
            int alt1=3;
            switch ( input.LA(1) ) {
            case RULE_ID:
            case 28:
                {
                alt1=1;
                }
                break;
            case 13:
            case 14:
            case 15:
            case 29:
            case 31:
            case 32:
            case 33:
            case 34:
                {
                alt1=2;
                }
                break;
            case 21:
                {
                alt1=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalDotty.g:357:2: ( ( rule__Compound__Group_1_0_0__0 ) )
                    {
                    // InternalDotty.g:357:2: ( ( rule__Compound__Group_1_0_0__0 ) )
                    // InternalDotty.g:358:3: ( rule__Compound__Group_1_0_0__0 )
                    {
                     before(grammarAccess.getCompoundAccess().getGroup_1_0_0()); 
                    // InternalDotty.g:359:3: ( rule__Compound__Group_1_0_0__0 )
                    // InternalDotty.g:359:4: rule__Compound__Group_1_0_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Compound__Group_1_0_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getCompoundAccess().getGroup_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDotty.g:363:2: ( ( rule__Compound__AttributesAssignment_1_0_1 ) )
                    {
                    // InternalDotty.g:363:2: ( ( rule__Compound__AttributesAssignment_1_0_1 ) )
                    // InternalDotty.g:364:3: ( rule__Compound__AttributesAssignment_1_0_1 )
                    {
                     before(grammarAccess.getCompoundAccess().getAttributesAssignment_1_0_1()); 
                    // InternalDotty.g:365:3: ( rule__Compound__AttributesAssignment_1_0_1 )
                    // InternalDotty.g:365:4: rule__Compound__AttributesAssignment_1_0_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Compound__AttributesAssignment_1_0_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getCompoundAccess().getAttributesAssignment_1_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDotty.g:369:2: ( ( rule__Compound__SubAssignment_1_0_2 ) )
                    {
                    // InternalDotty.g:369:2: ( ( rule__Compound__SubAssignment_1_0_2 ) )
                    // InternalDotty.g:370:3: ( rule__Compound__SubAssignment_1_0_2 )
                    {
                     before(grammarAccess.getCompoundAccess().getSubAssignment_1_0_2()); 
                    // InternalDotty.g:371:3: ( rule__Compound__SubAssignment_1_0_2 )
                    // InternalDotty.g:371:4: rule__Compound__SubAssignment_1_0_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Compound__SubAssignment_1_0_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getCompoundAccess().getSubAssignment_1_0_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__Alternatives_1_0"


    // $ANTLR start "rule__Node__Alternatives"
    // InternalDotty.g:379:1: rule__Node__Alternatives : ( ( ruleLeafNode ) | ( ruleSubNode ) );
    public final void rule__Node__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:383:1: ( ( ruleLeafNode ) | ( ruleSubNode ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_ID) ) {
                alt2=1;
            }
            else if ( (LA2_0==28) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalDotty.g:384:2: ( ruleLeafNode )
                    {
                    // InternalDotty.g:384:2: ( ruleLeafNode )
                    // InternalDotty.g:385:3: ruleLeafNode
                    {
                     before(grammarAccess.getNodeAccess().getLeafNodeParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleLeafNode();

                    state._fsp--;

                     after(grammarAccess.getNodeAccess().getLeafNodeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDotty.g:390:2: ( ruleSubNode )
                    {
                    // InternalDotty.g:390:2: ( ruleSubNode )
                    // InternalDotty.g:391:3: ruleSubNode
                    {
                     before(grammarAccess.getNodeAccess().getSubNodeParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleSubNode();

                    state._fsp--;

                     after(grammarAccess.getNodeAccess().getSubNodeParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Alternatives"


    // $ANTLR start "rule__Attribute__Alternatives"
    // InternalDotty.g:400:1: rule__Attribute__Alternatives : ( ( ruleRankAttribute ) | ( ruleShapeAttribute ) | ( ruleColorAttribute ) | ( ruleLabelAttribute ) | ( ruleStyleAttribute ) | ( ruleCompoundAttribute ) );
    public final void rule__Attribute__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:404:1: ( ( ruleRankAttribute ) | ( ruleShapeAttribute ) | ( ruleColorAttribute ) | ( ruleLabelAttribute ) | ( ruleStyleAttribute ) | ( ruleCompoundAttribute ) )
            int alt3=6;
            switch ( input.LA(1) ) {
            case 34:
                {
                alt3=1;
                }
                break;
            case 29:
                {
                alt3=2;
                }
                break;
            case 13:
            case 14:
            case 15:
                {
                alt3=3;
                }
                break;
            case 33:
                {
                alt3=4;
                }
                break;
            case 32:
                {
                alt3=5;
                }
                break;
            case 31:
                {
                alt3=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalDotty.g:405:2: ( ruleRankAttribute )
                    {
                    // InternalDotty.g:405:2: ( ruleRankAttribute )
                    // InternalDotty.g:406:3: ruleRankAttribute
                    {
                     before(grammarAccess.getAttributeAccess().getRankAttributeParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleRankAttribute();

                    state._fsp--;

                     after(grammarAccess.getAttributeAccess().getRankAttributeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDotty.g:411:2: ( ruleShapeAttribute )
                    {
                    // InternalDotty.g:411:2: ( ruleShapeAttribute )
                    // InternalDotty.g:412:3: ruleShapeAttribute
                    {
                     before(grammarAccess.getAttributeAccess().getShapeAttributeParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleShapeAttribute();

                    state._fsp--;

                     after(grammarAccess.getAttributeAccess().getShapeAttributeParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDotty.g:417:2: ( ruleColorAttribute )
                    {
                    // InternalDotty.g:417:2: ( ruleColorAttribute )
                    // InternalDotty.g:418:3: ruleColorAttribute
                    {
                     before(grammarAccess.getAttributeAccess().getColorAttributeParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleColorAttribute();

                    state._fsp--;

                     after(grammarAccess.getAttributeAccess().getColorAttributeParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalDotty.g:423:2: ( ruleLabelAttribute )
                    {
                    // InternalDotty.g:423:2: ( ruleLabelAttribute )
                    // InternalDotty.g:424:3: ruleLabelAttribute
                    {
                     before(grammarAccess.getAttributeAccess().getLabelAttributeParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleLabelAttribute();

                    state._fsp--;

                     after(grammarAccess.getAttributeAccess().getLabelAttributeParserRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalDotty.g:429:2: ( ruleStyleAttribute )
                    {
                    // InternalDotty.g:429:2: ( ruleStyleAttribute )
                    // InternalDotty.g:430:3: ruleStyleAttribute
                    {
                     before(grammarAccess.getAttributeAccess().getStyleAttributeParserRuleCall_4()); 
                    pushFollow(FOLLOW_2);
                    ruleStyleAttribute();

                    state._fsp--;

                     after(grammarAccess.getAttributeAccess().getStyleAttributeParserRuleCall_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalDotty.g:435:2: ( ruleCompoundAttribute )
                    {
                    // InternalDotty.g:435:2: ( ruleCompoundAttribute )
                    // InternalDotty.g:436:3: ruleCompoundAttribute
                    {
                     before(grammarAccess.getAttributeAccess().getCompoundAttributeParserRuleCall_5()); 
                    pushFollow(FOLLOW_2);
                    ruleCompoundAttribute();

                    state._fsp--;

                     after(grammarAccess.getAttributeAccess().getCompoundAttributeParserRuleCall_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Alternatives"


    // $ANTLR start "rule__ShapeAttribute__ShapeAlternatives_2_0"
    // InternalDotty.g:445:1: rule__ShapeAttribute__ShapeAlternatives_2_0 : ( ( RULE_ID ) | ( RULE_STRING ) );
    public final void rule__ShapeAttribute__ShapeAlternatives_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:449:1: ( ( RULE_ID ) | ( RULE_STRING ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_ID) ) {
                alt4=1;
            }
            else if ( (LA4_0==RULE_STRING) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalDotty.g:450:2: ( RULE_ID )
                    {
                    // InternalDotty.g:450:2: ( RULE_ID )
                    // InternalDotty.g:451:3: RULE_ID
                    {
                     before(grammarAccess.getShapeAttributeAccess().getShapeIDTerminalRuleCall_2_0_0()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getShapeAttributeAccess().getShapeIDTerminalRuleCall_2_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDotty.g:456:2: ( RULE_STRING )
                    {
                    // InternalDotty.g:456:2: ( RULE_STRING )
                    // InternalDotty.g:457:3: RULE_STRING
                    {
                     before(grammarAccess.getShapeAttributeAccess().getShapeSTRINGTerminalRuleCall_2_0_1()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getShapeAttributeAccess().getShapeSTRINGTerminalRuleCall_2_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ShapeAttribute__ShapeAlternatives_2_0"


    // $ANTLR start "rule__CompoundAttribute__CompoundAlternatives_2_0"
    // InternalDotty.g:466:1: rule__CompoundAttribute__CompoundAlternatives_2_0 : ( ( 'true' ) | ( '\\u00A0false' ) );
    public final void rule__CompoundAttribute__CompoundAlternatives_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:470:1: ( ( 'true' ) | ( '\\u00A0false' ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==11) ) {
                alt5=1;
            }
            else if ( (LA5_0==12) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalDotty.g:471:2: ( 'true' )
                    {
                    // InternalDotty.g:471:2: ( 'true' )
                    // InternalDotty.g:472:3: 'true'
                    {
                     before(grammarAccess.getCompoundAttributeAccess().getCompoundTrueKeyword_2_0_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getCompoundAttributeAccess().getCompoundTrueKeyword_2_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDotty.g:477:2: ( '\\u00A0false' )
                    {
                    // InternalDotty.g:477:2: ( '\\u00A0false' )
                    // InternalDotty.g:478:3: '\\u00A0false'
                    {
                     before(grammarAccess.getCompoundAttributeAccess().getCompoundFalseKeyword_2_0_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getCompoundAttributeAccess().getCompoundFalseKeyword_2_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundAttribute__CompoundAlternatives_2_0"


    // $ANTLR start "rule__ColorAttribute__Alternatives_0"
    // InternalDotty.g:487:1: rule__ColorAttribute__Alternatives_0 : ( ( 'color' ) | ( 'fillcolor' ) | ( 'fontcolor' ) );
    public final void rule__ColorAttribute__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:491:1: ( ( 'color' ) | ( 'fillcolor' ) | ( 'fontcolor' ) )
            int alt6=3;
            switch ( input.LA(1) ) {
            case 13:
                {
                alt6=1;
                }
                break;
            case 14:
                {
                alt6=2;
                }
                break;
            case 15:
                {
                alt6=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalDotty.g:492:2: ( 'color' )
                    {
                    // InternalDotty.g:492:2: ( 'color' )
                    // InternalDotty.g:493:3: 'color'
                    {
                     before(grammarAccess.getColorAttributeAccess().getColorKeyword_0_0()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getColorAttributeAccess().getColorKeyword_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDotty.g:498:2: ( 'fillcolor' )
                    {
                    // InternalDotty.g:498:2: ( 'fillcolor' )
                    // InternalDotty.g:499:3: 'fillcolor'
                    {
                     before(grammarAccess.getColorAttributeAccess().getFillcolorKeyword_0_1()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getColorAttributeAccess().getFillcolorKeyword_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDotty.g:504:2: ( 'fontcolor' )
                    {
                    // InternalDotty.g:504:2: ( 'fontcolor' )
                    // InternalDotty.g:505:3: 'fontcolor'
                    {
                     before(grammarAccess.getColorAttributeAccess().getFontcolorKeyword_0_2()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getColorAttributeAccess().getFontcolorKeyword_0_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ColorAttribute__Alternatives_0"


    // $ANTLR start "rule__ColorAttribute__ColorAlternatives_2_0"
    // InternalDotty.g:514:1: rule__ColorAttribute__ColorAlternatives_2_0 : ( ( RULE_ID ) | ( RULE_STRING ) );
    public final void rule__ColorAttribute__ColorAlternatives_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:518:1: ( ( RULE_ID ) | ( RULE_STRING ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_ID) ) {
                alt7=1;
            }
            else if ( (LA7_0==RULE_STRING) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalDotty.g:519:2: ( RULE_ID )
                    {
                    // InternalDotty.g:519:2: ( RULE_ID )
                    // InternalDotty.g:520:3: RULE_ID
                    {
                     before(grammarAccess.getColorAttributeAccess().getColorIDTerminalRuleCall_2_0_0()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getColorAttributeAccess().getColorIDTerminalRuleCall_2_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDotty.g:525:2: ( RULE_STRING )
                    {
                    // InternalDotty.g:525:2: ( RULE_STRING )
                    // InternalDotty.g:526:3: RULE_STRING
                    {
                     before(grammarAccess.getColorAttributeAccess().getColorSTRINGTerminalRuleCall_2_0_1()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getColorAttributeAccess().getColorSTRINGTerminalRuleCall_2_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ColorAttribute__ColorAlternatives_2_0"


    // $ANTLR start "rule__StyleAttribute__StyleAlternatives_2_0"
    // InternalDotty.g:535:1: rule__StyleAttribute__StyleAlternatives_2_0 : ( ( RULE_ID ) | ( RULE_STRING ) );
    public final void rule__StyleAttribute__StyleAlternatives_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:539:1: ( ( RULE_ID ) | ( RULE_STRING ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_ID) ) {
                alt8=1;
            }
            else if ( (LA8_0==RULE_STRING) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalDotty.g:540:2: ( RULE_ID )
                    {
                    // InternalDotty.g:540:2: ( RULE_ID )
                    // InternalDotty.g:541:3: RULE_ID
                    {
                     before(grammarAccess.getStyleAttributeAccess().getStyleIDTerminalRuleCall_2_0_0()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getStyleAttributeAccess().getStyleIDTerminalRuleCall_2_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDotty.g:546:2: ( RULE_STRING )
                    {
                    // InternalDotty.g:546:2: ( RULE_STRING )
                    // InternalDotty.g:547:3: RULE_STRING
                    {
                     before(grammarAccess.getStyleAttributeAccess().getStyleSTRINGTerminalRuleCall_2_0_1()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getStyleAttributeAccess().getStyleSTRINGTerminalRuleCall_2_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StyleAttribute__StyleAlternatives_2_0"


    // $ANTLR start "rule__RankAttribute__DirectionAlternatives_2_0"
    // InternalDotty.g:556:1: rule__RankAttribute__DirectionAlternatives_2_0 : ( ( 'TB' ) | ( 'LR' ) | ( 'BT' ) | ( 'RL' ) );
    public final void rule__RankAttribute__DirectionAlternatives_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:560:1: ( ( 'TB' ) | ( 'LR' ) | ( 'BT' ) | ( 'RL' ) )
            int alt9=4;
            switch ( input.LA(1) ) {
            case 16:
                {
                alt9=1;
                }
                break;
            case 17:
                {
                alt9=2;
                }
                break;
            case 18:
                {
                alt9=3;
                }
                break;
            case 19:
                {
                alt9=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalDotty.g:561:2: ( 'TB' )
                    {
                    // InternalDotty.g:561:2: ( 'TB' )
                    // InternalDotty.g:562:3: 'TB'
                    {
                     before(grammarAccess.getRankAttributeAccess().getDirectionTBKeyword_2_0_0()); 
                    match(input,16,FOLLOW_2); 
                     after(grammarAccess.getRankAttributeAccess().getDirectionTBKeyword_2_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDotty.g:567:2: ( 'LR' )
                    {
                    // InternalDotty.g:567:2: ( 'LR' )
                    // InternalDotty.g:568:3: 'LR'
                    {
                     before(grammarAccess.getRankAttributeAccess().getDirectionLRKeyword_2_0_1()); 
                    match(input,17,FOLLOW_2); 
                     after(grammarAccess.getRankAttributeAccess().getDirectionLRKeyword_2_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDotty.g:573:2: ( 'BT' )
                    {
                    // InternalDotty.g:573:2: ( 'BT' )
                    // InternalDotty.g:574:3: 'BT'
                    {
                     before(grammarAccess.getRankAttributeAccess().getDirectionBTKeyword_2_0_2()); 
                    match(input,18,FOLLOW_2); 
                     after(grammarAccess.getRankAttributeAccess().getDirectionBTKeyword_2_0_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalDotty.g:579:2: ( 'RL' )
                    {
                    // InternalDotty.g:579:2: ( 'RL' )
                    // InternalDotty.g:580:3: 'RL'
                    {
                     before(grammarAccess.getRankAttributeAccess().getDirectionRLKeyword_2_0_3()); 
                    match(input,19,FOLLOW_2); 
                     after(grammarAccess.getRankAttributeAccess().getDirectionRLKeyword_2_0_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RankAttribute__DirectionAlternatives_2_0"


    // $ANTLR start "rule__Graph__Group__0"
    // InternalDotty.g:589:1: rule__Graph__Group__0 : rule__Graph__Group__0__Impl rule__Graph__Group__1 ;
    public final void rule__Graph__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:593:1: ( rule__Graph__Group__0__Impl rule__Graph__Group__1 )
            // InternalDotty.g:594:2: rule__Graph__Group__0__Impl rule__Graph__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Graph__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Graph__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Graph__Group__0"


    // $ANTLR start "rule__Graph__Group__0__Impl"
    // InternalDotty.g:601:1: rule__Graph__Group__0__Impl : ( 'digraph' ) ;
    public final void rule__Graph__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:605:1: ( ( 'digraph' ) )
            // InternalDotty.g:606:1: ( 'digraph' )
            {
            // InternalDotty.g:606:1: ( 'digraph' )
            // InternalDotty.g:607:2: 'digraph'
            {
             before(grammarAccess.getGraphAccess().getDigraphKeyword_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getGraphAccess().getDigraphKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Graph__Group__0__Impl"


    // $ANTLR start "rule__Graph__Group__1"
    // InternalDotty.g:616:1: rule__Graph__Group__1 : rule__Graph__Group__1__Impl rule__Graph__Group__2 ;
    public final void rule__Graph__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:620:1: ( rule__Graph__Group__1__Impl rule__Graph__Group__2 )
            // InternalDotty.g:621:2: rule__Graph__Group__1__Impl rule__Graph__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Graph__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Graph__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Graph__Group__1"


    // $ANTLR start "rule__Graph__Group__1__Impl"
    // InternalDotty.g:628:1: rule__Graph__Group__1__Impl : ( ( rule__Graph__NameAssignment_1 ) ) ;
    public final void rule__Graph__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:632:1: ( ( ( rule__Graph__NameAssignment_1 ) ) )
            // InternalDotty.g:633:1: ( ( rule__Graph__NameAssignment_1 ) )
            {
            // InternalDotty.g:633:1: ( ( rule__Graph__NameAssignment_1 ) )
            // InternalDotty.g:634:2: ( rule__Graph__NameAssignment_1 )
            {
             before(grammarAccess.getGraphAccess().getNameAssignment_1()); 
            // InternalDotty.g:635:2: ( rule__Graph__NameAssignment_1 )
            // InternalDotty.g:635:3: rule__Graph__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Graph__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getGraphAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Graph__Group__1__Impl"


    // $ANTLR start "rule__Graph__Group__2"
    // InternalDotty.g:643:1: rule__Graph__Group__2 : rule__Graph__Group__2__Impl ;
    public final void rule__Graph__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:647:1: ( rule__Graph__Group__2__Impl )
            // InternalDotty.g:648:2: rule__Graph__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Graph__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Graph__Group__2"


    // $ANTLR start "rule__Graph__Group__2__Impl"
    // InternalDotty.g:654:1: rule__Graph__Group__2__Impl : ( ( rule__Graph__BodyAssignment_2 ) ) ;
    public final void rule__Graph__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:658:1: ( ( ( rule__Graph__BodyAssignment_2 ) ) )
            // InternalDotty.g:659:1: ( ( rule__Graph__BodyAssignment_2 ) )
            {
            // InternalDotty.g:659:1: ( ( rule__Graph__BodyAssignment_2 ) )
            // InternalDotty.g:660:2: ( rule__Graph__BodyAssignment_2 )
            {
             before(grammarAccess.getGraphAccess().getBodyAssignment_2()); 
            // InternalDotty.g:661:2: ( rule__Graph__BodyAssignment_2 )
            // InternalDotty.g:661:3: rule__Graph__BodyAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Graph__BodyAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getGraphAccess().getBodyAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Graph__Group__2__Impl"


    // $ANTLR start "rule__Compound__Group__0"
    // InternalDotty.g:670:1: rule__Compound__Group__0 : rule__Compound__Group__0__Impl rule__Compound__Group__1 ;
    public final void rule__Compound__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:674:1: ( rule__Compound__Group__0__Impl rule__Compound__Group__1 )
            // InternalDotty.g:675:2: rule__Compound__Group__0__Impl rule__Compound__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Compound__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Compound__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__Group__0"


    // $ANTLR start "rule__Compound__Group__0__Impl"
    // InternalDotty.g:682:1: rule__Compound__Group__0__Impl : ( '{' ) ;
    public final void rule__Compound__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:686:1: ( ( '{' ) )
            // InternalDotty.g:687:1: ( '{' )
            {
            // InternalDotty.g:687:1: ( '{' )
            // InternalDotty.g:688:2: '{'
            {
             before(grammarAccess.getCompoundAccess().getLeftCurlyBracketKeyword_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getCompoundAccess().getLeftCurlyBracketKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__Group__0__Impl"


    // $ANTLR start "rule__Compound__Group__1"
    // InternalDotty.g:697:1: rule__Compound__Group__1 : rule__Compound__Group__1__Impl rule__Compound__Group__2 ;
    public final void rule__Compound__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:701:1: ( rule__Compound__Group__1__Impl rule__Compound__Group__2 )
            // InternalDotty.g:702:2: rule__Compound__Group__1__Impl rule__Compound__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Compound__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Compound__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__Group__1"


    // $ANTLR start "rule__Compound__Group__1__Impl"
    // InternalDotty.g:709:1: rule__Compound__Group__1__Impl : ( ( rule__Compound__Group_1__0 )* ) ;
    public final void rule__Compound__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:713:1: ( ( ( rule__Compound__Group_1__0 )* ) )
            // InternalDotty.g:714:1: ( ( rule__Compound__Group_1__0 )* )
            {
            // InternalDotty.g:714:1: ( ( rule__Compound__Group_1__0 )* )
            // InternalDotty.g:715:2: ( rule__Compound__Group_1__0 )*
            {
             before(grammarAccess.getCompoundAccess().getGroup_1()); 
            // InternalDotty.g:716:2: ( rule__Compound__Group_1__0 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==RULE_ID||(LA10_0>=13 && LA10_0<=15)||LA10_0==21||(LA10_0>=28 && LA10_0<=29)||(LA10_0>=31 && LA10_0<=34)) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalDotty.g:716:3: rule__Compound__Group_1__0
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__Compound__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getCompoundAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__Group__1__Impl"


    // $ANTLR start "rule__Compound__Group__2"
    // InternalDotty.g:724:1: rule__Compound__Group__2 : rule__Compound__Group__2__Impl ;
    public final void rule__Compound__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:728:1: ( rule__Compound__Group__2__Impl )
            // InternalDotty.g:729:2: rule__Compound__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Compound__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__Group__2"


    // $ANTLR start "rule__Compound__Group__2__Impl"
    // InternalDotty.g:735:1: rule__Compound__Group__2__Impl : ( '}' ) ;
    public final void rule__Compound__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:739:1: ( ( '}' ) )
            // InternalDotty.g:740:1: ( '}' )
            {
            // InternalDotty.g:740:1: ( '}' )
            // InternalDotty.g:741:2: '}'
            {
             before(grammarAccess.getCompoundAccess().getRightCurlyBracketKeyword_2()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getCompoundAccess().getRightCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__Group__2__Impl"


    // $ANTLR start "rule__Compound__Group_1__0"
    // InternalDotty.g:751:1: rule__Compound__Group_1__0 : rule__Compound__Group_1__0__Impl rule__Compound__Group_1__1 ;
    public final void rule__Compound__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:755:1: ( rule__Compound__Group_1__0__Impl rule__Compound__Group_1__1 )
            // InternalDotty.g:756:2: rule__Compound__Group_1__0__Impl rule__Compound__Group_1__1
            {
            pushFollow(FOLLOW_7);
            rule__Compound__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Compound__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__Group_1__0"


    // $ANTLR start "rule__Compound__Group_1__0__Impl"
    // InternalDotty.g:763:1: rule__Compound__Group_1__0__Impl : ( ( rule__Compound__Alternatives_1_0 ) ) ;
    public final void rule__Compound__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:767:1: ( ( ( rule__Compound__Alternatives_1_0 ) ) )
            // InternalDotty.g:768:1: ( ( rule__Compound__Alternatives_1_0 ) )
            {
            // InternalDotty.g:768:1: ( ( rule__Compound__Alternatives_1_0 ) )
            // InternalDotty.g:769:2: ( rule__Compound__Alternatives_1_0 )
            {
             before(grammarAccess.getCompoundAccess().getAlternatives_1_0()); 
            // InternalDotty.g:770:2: ( rule__Compound__Alternatives_1_0 )
            // InternalDotty.g:770:3: rule__Compound__Alternatives_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Compound__Alternatives_1_0();

            state._fsp--;


            }

             after(grammarAccess.getCompoundAccess().getAlternatives_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__Group_1__0__Impl"


    // $ANTLR start "rule__Compound__Group_1__1"
    // InternalDotty.g:778:1: rule__Compound__Group_1__1 : rule__Compound__Group_1__1__Impl ;
    public final void rule__Compound__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:782:1: ( rule__Compound__Group_1__1__Impl )
            // InternalDotty.g:783:2: rule__Compound__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Compound__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__Group_1__1"


    // $ANTLR start "rule__Compound__Group_1__1__Impl"
    // InternalDotty.g:789:1: rule__Compound__Group_1__1__Impl : ( ( ';' )? ) ;
    public final void rule__Compound__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:793:1: ( ( ( ';' )? ) )
            // InternalDotty.g:794:1: ( ( ';' )? )
            {
            // InternalDotty.g:794:1: ( ( ';' )? )
            // InternalDotty.g:795:2: ( ';' )?
            {
             before(grammarAccess.getCompoundAccess().getSemicolonKeyword_1_1()); 
            // InternalDotty.g:796:2: ( ';' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==23) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalDotty.g:796:3: ';'
                    {
                    match(input,23,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getCompoundAccess().getSemicolonKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__Group_1__1__Impl"


    // $ANTLR start "rule__Compound__Group_1_0_0__0"
    // InternalDotty.g:805:1: rule__Compound__Group_1_0_0__0 : rule__Compound__Group_1_0_0__0__Impl rule__Compound__Group_1_0_0__1 ;
    public final void rule__Compound__Group_1_0_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:809:1: ( rule__Compound__Group_1_0_0__0__Impl rule__Compound__Group_1_0_0__1 )
            // InternalDotty.g:810:2: rule__Compound__Group_1_0_0__0__Impl rule__Compound__Group_1_0_0__1
            {
            pushFollow(FOLLOW_8);
            rule__Compound__Group_1_0_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Compound__Group_1_0_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__Group_1_0_0__0"


    // $ANTLR start "rule__Compound__Group_1_0_0__0__Impl"
    // InternalDotty.g:817:1: rule__Compound__Group_1_0_0__0__Impl : ( ( rule__Compound__NodesAssignment_1_0_0_0 ) ) ;
    public final void rule__Compound__Group_1_0_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:821:1: ( ( ( rule__Compound__NodesAssignment_1_0_0_0 ) ) )
            // InternalDotty.g:822:1: ( ( rule__Compound__NodesAssignment_1_0_0_0 ) )
            {
            // InternalDotty.g:822:1: ( ( rule__Compound__NodesAssignment_1_0_0_0 ) )
            // InternalDotty.g:823:2: ( rule__Compound__NodesAssignment_1_0_0_0 )
            {
             before(grammarAccess.getCompoundAccess().getNodesAssignment_1_0_0_0()); 
            // InternalDotty.g:824:2: ( rule__Compound__NodesAssignment_1_0_0_0 )
            // InternalDotty.g:824:3: rule__Compound__NodesAssignment_1_0_0_0
            {
            pushFollow(FOLLOW_2);
            rule__Compound__NodesAssignment_1_0_0_0();

            state._fsp--;


            }

             after(grammarAccess.getCompoundAccess().getNodesAssignment_1_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__Group_1_0_0__0__Impl"


    // $ANTLR start "rule__Compound__Group_1_0_0__1"
    // InternalDotty.g:832:1: rule__Compound__Group_1_0_0__1 : rule__Compound__Group_1_0_0__1__Impl ;
    public final void rule__Compound__Group_1_0_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:836:1: ( rule__Compound__Group_1_0_0__1__Impl )
            // InternalDotty.g:837:2: rule__Compound__Group_1_0_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Compound__Group_1_0_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__Group_1_0_0__1"


    // $ANTLR start "rule__Compound__Group_1_0_0__1__Impl"
    // InternalDotty.g:843:1: rule__Compound__Group_1_0_0__1__Impl : ( ( rule__Compound__Group_1_0_0_1__0 )* ) ;
    public final void rule__Compound__Group_1_0_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:847:1: ( ( ( rule__Compound__Group_1_0_0_1__0 )* ) )
            // InternalDotty.g:848:1: ( ( rule__Compound__Group_1_0_0_1__0 )* )
            {
            // InternalDotty.g:848:1: ( ( rule__Compound__Group_1_0_0_1__0 )* )
            // InternalDotty.g:849:2: ( rule__Compound__Group_1_0_0_1__0 )*
            {
             before(grammarAccess.getCompoundAccess().getGroup_1_0_0_1()); 
            // InternalDotty.g:850:2: ( rule__Compound__Group_1_0_0_1__0 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==24) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalDotty.g:850:3: rule__Compound__Group_1_0_0_1__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Compound__Group_1_0_0_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

             after(grammarAccess.getCompoundAccess().getGroup_1_0_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__Group_1_0_0__1__Impl"


    // $ANTLR start "rule__Compound__Group_1_0_0_1__0"
    // InternalDotty.g:859:1: rule__Compound__Group_1_0_0_1__0 : rule__Compound__Group_1_0_0_1__0__Impl rule__Compound__Group_1_0_0_1__1 ;
    public final void rule__Compound__Group_1_0_0_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:863:1: ( rule__Compound__Group_1_0_0_1__0__Impl rule__Compound__Group_1_0_0_1__1 )
            // InternalDotty.g:864:2: rule__Compound__Group_1_0_0_1__0__Impl rule__Compound__Group_1_0_0_1__1
            {
            pushFollow(FOLLOW_10);
            rule__Compound__Group_1_0_0_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Compound__Group_1_0_0_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__Group_1_0_0_1__0"


    // $ANTLR start "rule__Compound__Group_1_0_0_1__0__Impl"
    // InternalDotty.g:871:1: rule__Compound__Group_1_0_0_1__0__Impl : ( '->' ) ;
    public final void rule__Compound__Group_1_0_0_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:875:1: ( ( '->' ) )
            // InternalDotty.g:876:1: ( '->' )
            {
            // InternalDotty.g:876:1: ( '->' )
            // InternalDotty.g:877:2: '->'
            {
             before(grammarAccess.getCompoundAccess().getHyphenMinusGreaterThanSignKeyword_1_0_0_1_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getCompoundAccess().getHyphenMinusGreaterThanSignKeyword_1_0_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__Group_1_0_0_1__0__Impl"


    // $ANTLR start "rule__Compound__Group_1_0_0_1__1"
    // InternalDotty.g:886:1: rule__Compound__Group_1_0_0_1__1 : rule__Compound__Group_1_0_0_1__1__Impl ;
    public final void rule__Compound__Group_1_0_0_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:890:1: ( rule__Compound__Group_1_0_0_1__1__Impl )
            // InternalDotty.g:891:2: rule__Compound__Group_1_0_0_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Compound__Group_1_0_0_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__Group_1_0_0_1__1"


    // $ANTLR start "rule__Compound__Group_1_0_0_1__1__Impl"
    // InternalDotty.g:897:1: rule__Compound__Group_1_0_0_1__1__Impl : ( ( rule__Compound__NodesAssignment_1_0_0_1_1 ) ) ;
    public final void rule__Compound__Group_1_0_0_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:901:1: ( ( ( rule__Compound__NodesAssignment_1_0_0_1_1 ) ) )
            // InternalDotty.g:902:1: ( ( rule__Compound__NodesAssignment_1_0_0_1_1 ) )
            {
            // InternalDotty.g:902:1: ( ( rule__Compound__NodesAssignment_1_0_0_1_1 ) )
            // InternalDotty.g:903:2: ( rule__Compound__NodesAssignment_1_0_0_1_1 )
            {
             before(grammarAccess.getCompoundAccess().getNodesAssignment_1_0_0_1_1()); 
            // InternalDotty.g:904:2: ( rule__Compound__NodesAssignment_1_0_0_1_1 )
            // InternalDotty.g:904:3: rule__Compound__NodesAssignment_1_0_0_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Compound__NodesAssignment_1_0_0_1_1();

            state._fsp--;


            }

             after(grammarAccess.getCompoundAccess().getNodesAssignment_1_0_0_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__Group_1_0_0_1__1__Impl"


    // $ANTLR start "rule__LeafNode__Group__0"
    // InternalDotty.g:913:1: rule__LeafNode__Group__0 : rule__LeafNode__Group__0__Impl rule__LeafNode__Group__1 ;
    public final void rule__LeafNode__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:917:1: ( rule__LeafNode__Group__0__Impl rule__LeafNode__Group__1 )
            // InternalDotty.g:918:2: rule__LeafNode__Group__0__Impl rule__LeafNode__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__LeafNode__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafNode__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafNode__Group__0"


    // $ANTLR start "rule__LeafNode__Group__0__Impl"
    // InternalDotty.g:925:1: rule__LeafNode__Group__0__Impl : ( ( rule__LeafNode__NameAssignment_0 ) ) ;
    public final void rule__LeafNode__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:929:1: ( ( ( rule__LeafNode__NameAssignment_0 ) ) )
            // InternalDotty.g:930:1: ( ( rule__LeafNode__NameAssignment_0 ) )
            {
            // InternalDotty.g:930:1: ( ( rule__LeafNode__NameAssignment_0 ) )
            // InternalDotty.g:931:2: ( rule__LeafNode__NameAssignment_0 )
            {
             before(grammarAccess.getLeafNodeAccess().getNameAssignment_0()); 
            // InternalDotty.g:932:2: ( rule__LeafNode__NameAssignment_0 )
            // InternalDotty.g:932:3: rule__LeafNode__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__LeafNode__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getLeafNodeAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafNode__Group__0__Impl"


    // $ANTLR start "rule__LeafNode__Group__1"
    // InternalDotty.g:940:1: rule__LeafNode__Group__1 : rule__LeafNode__Group__1__Impl ;
    public final void rule__LeafNode__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:944:1: ( rule__LeafNode__Group__1__Impl )
            // InternalDotty.g:945:2: rule__LeafNode__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LeafNode__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafNode__Group__1"


    // $ANTLR start "rule__LeafNode__Group__1__Impl"
    // InternalDotty.g:951:1: rule__LeafNode__Group__1__Impl : ( ( rule__LeafNode__Group_1__0 )? ) ;
    public final void rule__LeafNode__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:955:1: ( ( ( rule__LeafNode__Group_1__0 )? ) )
            // InternalDotty.g:956:1: ( ( rule__LeafNode__Group_1__0 )? )
            {
            // InternalDotty.g:956:1: ( ( rule__LeafNode__Group_1__0 )? )
            // InternalDotty.g:957:2: ( rule__LeafNode__Group_1__0 )?
            {
             before(grammarAccess.getLeafNodeAccess().getGroup_1()); 
            // InternalDotty.g:958:2: ( rule__LeafNode__Group_1__0 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==25) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalDotty.g:958:3: rule__LeafNode__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LeafNode__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getLeafNodeAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafNode__Group__1__Impl"


    // $ANTLR start "rule__LeafNode__Group_1__0"
    // InternalDotty.g:967:1: rule__LeafNode__Group_1__0 : rule__LeafNode__Group_1__0__Impl rule__LeafNode__Group_1__1 ;
    public final void rule__LeafNode__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:971:1: ( rule__LeafNode__Group_1__0__Impl rule__LeafNode__Group_1__1 )
            // InternalDotty.g:972:2: rule__LeafNode__Group_1__0__Impl rule__LeafNode__Group_1__1
            {
            pushFollow(FOLLOW_12);
            rule__LeafNode__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafNode__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafNode__Group_1__0"


    // $ANTLR start "rule__LeafNode__Group_1__0__Impl"
    // InternalDotty.g:979:1: rule__LeafNode__Group_1__0__Impl : ( '[' ) ;
    public final void rule__LeafNode__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:983:1: ( ( '[' ) )
            // InternalDotty.g:984:1: ( '[' )
            {
            // InternalDotty.g:984:1: ( '[' )
            // InternalDotty.g:985:2: '['
            {
             before(grammarAccess.getLeafNodeAccess().getLeftSquareBracketKeyword_1_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getLeafNodeAccess().getLeftSquareBracketKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafNode__Group_1__0__Impl"


    // $ANTLR start "rule__LeafNode__Group_1__1"
    // InternalDotty.g:994:1: rule__LeafNode__Group_1__1 : rule__LeafNode__Group_1__1__Impl rule__LeafNode__Group_1__2 ;
    public final void rule__LeafNode__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:998:1: ( rule__LeafNode__Group_1__1__Impl rule__LeafNode__Group_1__2 )
            // InternalDotty.g:999:2: rule__LeafNode__Group_1__1__Impl rule__LeafNode__Group_1__2
            {
            pushFollow(FOLLOW_13);
            rule__LeafNode__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafNode__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafNode__Group_1__1"


    // $ANTLR start "rule__LeafNode__Group_1__1__Impl"
    // InternalDotty.g:1006:1: rule__LeafNode__Group_1__1__Impl : ( ( rule__LeafNode__AttributesAssignment_1_1 ) ) ;
    public final void rule__LeafNode__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1010:1: ( ( ( rule__LeafNode__AttributesAssignment_1_1 ) ) )
            // InternalDotty.g:1011:1: ( ( rule__LeafNode__AttributesAssignment_1_1 ) )
            {
            // InternalDotty.g:1011:1: ( ( rule__LeafNode__AttributesAssignment_1_1 ) )
            // InternalDotty.g:1012:2: ( rule__LeafNode__AttributesAssignment_1_1 )
            {
             before(grammarAccess.getLeafNodeAccess().getAttributesAssignment_1_1()); 
            // InternalDotty.g:1013:2: ( rule__LeafNode__AttributesAssignment_1_1 )
            // InternalDotty.g:1013:3: rule__LeafNode__AttributesAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__LeafNode__AttributesAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getLeafNodeAccess().getAttributesAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafNode__Group_1__1__Impl"


    // $ANTLR start "rule__LeafNode__Group_1__2"
    // InternalDotty.g:1021:1: rule__LeafNode__Group_1__2 : rule__LeafNode__Group_1__2__Impl rule__LeafNode__Group_1__3 ;
    public final void rule__LeafNode__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1025:1: ( rule__LeafNode__Group_1__2__Impl rule__LeafNode__Group_1__3 )
            // InternalDotty.g:1026:2: rule__LeafNode__Group_1__2__Impl rule__LeafNode__Group_1__3
            {
            pushFollow(FOLLOW_13);
            rule__LeafNode__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafNode__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafNode__Group_1__2"


    // $ANTLR start "rule__LeafNode__Group_1__2__Impl"
    // InternalDotty.g:1033:1: rule__LeafNode__Group_1__2__Impl : ( ( rule__LeafNode__Group_1_2__0 )* ) ;
    public final void rule__LeafNode__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1037:1: ( ( ( rule__LeafNode__Group_1_2__0 )* ) )
            // InternalDotty.g:1038:1: ( ( rule__LeafNode__Group_1_2__0 )* )
            {
            // InternalDotty.g:1038:1: ( ( rule__LeafNode__Group_1_2__0 )* )
            // InternalDotty.g:1039:2: ( rule__LeafNode__Group_1_2__0 )*
            {
             before(grammarAccess.getLeafNodeAccess().getGroup_1_2()); 
            // InternalDotty.g:1040:2: ( rule__LeafNode__Group_1_2__0 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==27) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalDotty.g:1040:3: rule__LeafNode__Group_1_2__0
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__LeafNode__Group_1_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

             after(grammarAccess.getLeafNodeAccess().getGroup_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafNode__Group_1__2__Impl"


    // $ANTLR start "rule__LeafNode__Group_1__3"
    // InternalDotty.g:1048:1: rule__LeafNode__Group_1__3 : rule__LeafNode__Group_1__3__Impl ;
    public final void rule__LeafNode__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1052:1: ( rule__LeafNode__Group_1__3__Impl )
            // InternalDotty.g:1053:2: rule__LeafNode__Group_1__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LeafNode__Group_1__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafNode__Group_1__3"


    // $ANTLR start "rule__LeafNode__Group_1__3__Impl"
    // InternalDotty.g:1059:1: rule__LeafNode__Group_1__3__Impl : ( ']' ) ;
    public final void rule__LeafNode__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1063:1: ( ( ']' ) )
            // InternalDotty.g:1064:1: ( ']' )
            {
            // InternalDotty.g:1064:1: ( ']' )
            // InternalDotty.g:1065:2: ']'
            {
             before(grammarAccess.getLeafNodeAccess().getRightSquareBracketKeyword_1_3()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getLeafNodeAccess().getRightSquareBracketKeyword_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafNode__Group_1__3__Impl"


    // $ANTLR start "rule__LeafNode__Group_1_2__0"
    // InternalDotty.g:1075:1: rule__LeafNode__Group_1_2__0 : rule__LeafNode__Group_1_2__0__Impl rule__LeafNode__Group_1_2__1 ;
    public final void rule__LeafNode__Group_1_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1079:1: ( rule__LeafNode__Group_1_2__0__Impl rule__LeafNode__Group_1_2__1 )
            // InternalDotty.g:1080:2: rule__LeafNode__Group_1_2__0__Impl rule__LeafNode__Group_1_2__1
            {
            pushFollow(FOLLOW_12);
            rule__LeafNode__Group_1_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafNode__Group_1_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafNode__Group_1_2__0"


    // $ANTLR start "rule__LeafNode__Group_1_2__0__Impl"
    // InternalDotty.g:1087:1: rule__LeafNode__Group_1_2__0__Impl : ( ',' ) ;
    public final void rule__LeafNode__Group_1_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1091:1: ( ( ',' ) )
            // InternalDotty.g:1092:1: ( ',' )
            {
            // InternalDotty.g:1092:1: ( ',' )
            // InternalDotty.g:1093:2: ','
            {
             before(grammarAccess.getLeafNodeAccess().getCommaKeyword_1_2_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getLeafNodeAccess().getCommaKeyword_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafNode__Group_1_2__0__Impl"


    // $ANTLR start "rule__LeafNode__Group_1_2__1"
    // InternalDotty.g:1102:1: rule__LeafNode__Group_1_2__1 : rule__LeafNode__Group_1_2__1__Impl ;
    public final void rule__LeafNode__Group_1_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1106:1: ( rule__LeafNode__Group_1_2__1__Impl )
            // InternalDotty.g:1107:2: rule__LeafNode__Group_1_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LeafNode__Group_1_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafNode__Group_1_2__1"


    // $ANTLR start "rule__LeafNode__Group_1_2__1__Impl"
    // InternalDotty.g:1113:1: rule__LeafNode__Group_1_2__1__Impl : ( ( rule__LeafNode__AttributesAssignment_1_2_1 ) ) ;
    public final void rule__LeafNode__Group_1_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1117:1: ( ( ( rule__LeafNode__AttributesAssignment_1_2_1 ) ) )
            // InternalDotty.g:1118:1: ( ( rule__LeafNode__AttributesAssignment_1_2_1 ) )
            {
            // InternalDotty.g:1118:1: ( ( rule__LeafNode__AttributesAssignment_1_2_1 ) )
            // InternalDotty.g:1119:2: ( rule__LeafNode__AttributesAssignment_1_2_1 )
            {
             before(grammarAccess.getLeafNodeAccess().getAttributesAssignment_1_2_1()); 
            // InternalDotty.g:1120:2: ( rule__LeafNode__AttributesAssignment_1_2_1 )
            // InternalDotty.g:1120:3: rule__LeafNode__AttributesAssignment_1_2_1
            {
            pushFollow(FOLLOW_2);
            rule__LeafNode__AttributesAssignment_1_2_1();

            state._fsp--;


            }

             after(grammarAccess.getLeafNodeAccess().getAttributesAssignment_1_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafNode__Group_1_2__1__Impl"


    // $ANTLR start "rule__SubNode__Group__0"
    // InternalDotty.g:1129:1: rule__SubNode__Group__0 : rule__SubNode__Group__0__Impl rule__SubNode__Group__1 ;
    public final void rule__SubNode__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1133:1: ( rule__SubNode__Group__0__Impl rule__SubNode__Group__1 )
            // InternalDotty.g:1134:2: rule__SubNode__Group__0__Impl rule__SubNode__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__SubNode__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SubNode__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SubNode__Group__0"


    // $ANTLR start "rule__SubNode__Group__0__Impl"
    // InternalDotty.g:1141:1: rule__SubNode__Group__0__Impl : ( 'subgraph' ) ;
    public final void rule__SubNode__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1145:1: ( ( 'subgraph' ) )
            // InternalDotty.g:1146:1: ( 'subgraph' )
            {
            // InternalDotty.g:1146:1: ( 'subgraph' )
            // InternalDotty.g:1147:2: 'subgraph'
            {
             before(grammarAccess.getSubNodeAccess().getSubgraphKeyword_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getSubNodeAccess().getSubgraphKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SubNode__Group__0__Impl"


    // $ANTLR start "rule__SubNode__Group__1"
    // InternalDotty.g:1156:1: rule__SubNode__Group__1 : rule__SubNode__Group__1__Impl rule__SubNode__Group__2 ;
    public final void rule__SubNode__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1160:1: ( rule__SubNode__Group__1__Impl rule__SubNode__Group__2 )
            // InternalDotty.g:1161:2: rule__SubNode__Group__1__Impl rule__SubNode__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__SubNode__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SubNode__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SubNode__Group__1"


    // $ANTLR start "rule__SubNode__Group__1__Impl"
    // InternalDotty.g:1168:1: rule__SubNode__Group__1__Impl : ( ( rule__SubNode__NameAssignment_1 ) ) ;
    public final void rule__SubNode__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1172:1: ( ( ( rule__SubNode__NameAssignment_1 ) ) )
            // InternalDotty.g:1173:1: ( ( rule__SubNode__NameAssignment_1 ) )
            {
            // InternalDotty.g:1173:1: ( ( rule__SubNode__NameAssignment_1 ) )
            // InternalDotty.g:1174:2: ( rule__SubNode__NameAssignment_1 )
            {
             before(grammarAccess.getSubNodeAccess().getNameAssignment_1()); 
            // InternalDotty.g:1175:2: ( rule__SubNode__NameAssignment_1 )
            // InternalDotty.g:1175:3: rule__SubNode__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__SubNode__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getSubNodeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SubNode__Group__1__Impl"


    // $ANTLR start "rule__SubNode__Group__2"
    // InternalDotty.g:1183:1: rule__SubNode__Group__2 : rule__SubNode__Group__2__Impl ;
    public final void rule__SubNode__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1187:1: ( rule__SubNode__Group__2__Impl )
            // InternalDotty.g:1188:2: rule__SubNode__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SubNode__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SubNode__Group__2"


    // $ANTLR start "rule__SubNode__Group__2__Impl"
    // InternalDotty.g:1194:1: rule__SubNode__Group__2__Impl : ( ( rule__SubNode__BodyAssignment_2 ) ) ;
    public final void rule__SubNode__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1198:1: ( ( ( rule__SubNode__BodyAssignment_2 ) ) )
            // InternalDotty.g:1199:1: ( ( rule__SubNode__BodyAssignment_2 ) )
            {
            // InternalDotty.g:1199:1: ( ( rule__SubNode__BodyAssignment_2 ) )
            // InternalDotty.g:1200:2: ( rule__SubNode__BodyAssignment_2 )
            {
             before(grammarAccess.getSubNodeAccess().getBodyAssignment_2()); 
            // InternalDotty.g:1201:2: ( rule__SubNode__BodyAssignment_2 )
            // InternalDotty.g:1201:3: rule__SubNode__BodyAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__SubNode__BodyAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getSubNodeAccess().getBodyAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SubNode__Group__2__Impl"


    // $ANTLR start "rule__ShapeAttribute__Group__0"
    // InternalDotty.g:1210:1: rule__ShapeAttribute__Group__0 : rule__ShapeAttribute__Group__0__Impl rule__ShapeAttribute__Group__1 ;
    public final void rule__ShapeAttribute__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1214:1: ( rule__ShapeAttribute__Group__0__Impl rule__ShapeAttribute__Group__1 )
            // InternalDotty.g:1215:2: rule__ShapeAttribute__Group__0__Impl rule__ShapeAttribute__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__ShapeAttribute__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ShapeAttribute__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ShapeAttribute__Group__0"


    // $ANTLR start "rule__ShapeAttribute__Group__0__Impl"
    // InternalDotty.g:1222:1: rule__ShapeAttribute__Group__0__Impl : ( 'shape' ) ;
    public final void rule__ShapeAttribute__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1226:1: ( ( 'shape' ) )
            // InternalDotty.g:1227:1: ( 'shape' )
            {
            // InternalDotty.g:1227:1: ( 'shape' )
            // InternalDotty.g:1228:2: 'shape'
            {
             before(grammarAccess.getShapeAttributeAccess().getShapeKeyword_0()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getShapeAttributeAccess().getShapeKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ShapeAttribute__Group__0__Impl"


    // $ANTLR start "rule__ShapeAttribute__Group__1"
    // InternalDotty.g:1237:1: rule__ShapeAttribute__Group__1 : rule__ShapeAttribute__Group__1__Impl rule__ShapeAttribute__Group__2 ;
    public final void rule__ShapeAttribute__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1241:1: ( rule__ShapeAttribute__Group__1__Impl rule__ShapeAttribute__Group__2 )
            // InternalDotty.g:1242:2: rule__ShapeAttribute__Group__1__Impl rule__ShapeAttribute__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__ShapeAttribute__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ShapeAttribute__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ShapeAttribute__Group__1"


    // $ANTLR start "rule__ShapeAttribute__Group__1__Impl"
    // InternalDotty.g:1249:1: rule__ShapeAttribute__Group__1__Impl : ( '=' ) ;
    public final void rule__ShapeAttribute__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1253:1: ( ( '=' ) )
            // InternalDotty.g:1254:1: ( '=' )
            {
            // InternalDotty.g:1254:1: ( '=' )
            // InternalDotty.g:1255:2: '='
            {
             before(grammarAccess.getShapeAttributeAccess().getEqualsSignKeyword_1()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getShapeAttributeAccess().getEqualsSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ShapeAttribute__Group__1__Impl"


    // $ANTLR start "rule__ShapeAttribute__Group__2"
    // InternalDotty.g:1264:1: rule__ShapeAttribute__Group__2 : rule__ShapeAttribute__Group__2__Impl ;
    public final void rule__ShapeAttribute__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1268:1: ( rule__ShapeAttribute__Group__2__Impl )
            // InternalDotty.g:1269:2: rule__ShapeAttribute__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ShapeAttribute__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ShapeAttribute__Group__2"


    // $ANTLR start "rule__ShapeAttribute__Group__2__Impl"
    // InternalDotty.g:1275:1: rule__ShapeAttribute__Group__2__Impl : ( ( rule__ShapeAttribute__ShapeAssignment_2 ) ) ;
    public final void rule__ShapeAttribute__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1279:1: ( ( ( rule__ShapeAttribute__ShapeAssignment_2 ) ) )
            // InternalDotty.g:1280:1: ( ( rule__ShapeAttribute__ShapeAssignment_2 ) )
            {
            // InternalDotty.g:1280:1: ( ( rule__ShapeAttribute__ShapeAssignment_2 ) )
            // InternalDotty.g:1281:2: ( rule__ShapeAttribute__ShapeAssignment_2 )
            {
             before(grammarAccess.getShapeAttributeAccess().getShapeAssignment_2()); 
            // InternalDotty.g:1282:2: ( rule__ShapeAttribute__ShapeAssignment_2 )
            // InternalDotty.g:1282:3: rule__ShapeAttribute__ShapeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__ShapeAttribute__ShapeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getShapeAttributeAccess().getShapeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ShapeAttribute__Group__2__Impl"


    // $ANTLR start "rule__CompoundAttribute__Group__0"
    // InternalDotty.g:1291:1: rule__CompoundAttribute__Group__0 : rule__CompoundAttribute__Group__0__Impl rule__CompoundAttribute__Group__1 ;
    public final void rule__CompoundAttribute__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1295:1: ( rule__CompoundAttribute__Group__0__Impl rule__CompoundAttribute__Group__1 )
            // InternalDotty.g:1296:2: rule__CompoundAttribute__Group__0__Impl rule__CompoundAttribute__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__CompoundAttribute__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundAttribute__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundAttribute__Group__0"


    // $ANTLR start "rule__CompoundAttribute__Group__0__Impl"
    // InternalDotty.g:1303:1: rule__CompoundAttribute__Group__0__Impl : ( 'compound' ) ;
    public final void rule__CompoundAttribute__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1307:1: ( ( 'compound' ) )
            // InternalDotty.g:1308:1: ( 'compound' )
            {
            // InternalDotty.g:1308:1: ( 'compound' )
            // InternalDotty.g:1309:2: 'compound'
            {
             before(grammarAccess.getCompoundAttributeAccess().getCompoundKeyword_0()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getCompoundAttributeAccess().getCompoundKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundAttribute__Group__0__Impl"


    // $ANTLR start "rule__CompoundAttribute__Group__1"
    // InternalDotty.g:1318:1: rule__CompoundAttribute__Group__1 : rule__CompoundAttribute__Group__1__Impl rule__CompoundAttribute__Group__2 ;
    public final void rule__CompoundAttribute__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1322:1: ( rule__CompoundAttribute__Group__1__Impl rule__CompoundAttribute__Group__2 )
            // InternalDotty.g:1323:2: rule__CompoundAttribute__Group__1__Impl rule__CompoundAttribute__Group__2
            {
            pushFollow(FOLLOW_17);
            rule__CompoundAttribute__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundAttribute__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundAttribute__Group__1"


    // $ANTLR start "rule__CompoundAttribute__Group__1__Impl"
    // InternalDotty.g:1330:1: rule__CompoundAttribute__Group__1__Impl : ( '=' ) ;
    public final void rule__CompoundAttribute__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1334:1: ( ( '=' ) )
            // InternalDotty.g:1335:1: ( '=' )
            {
            // InternalDotty.g:1335:1: ( '=' )
            // InternalDotty.g:1336:2: '='
            {
             before(grammarAccess.getCompoundAttributeAccess().getEqualsSignKeyword_1()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getCompoundAttributeAccess().getEqualsSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundAttribute__Group__1__Impl"


    // $ANTLR start "rule__CompoundAttribute__Group__2"
    // InternalDotty.g:1345:1: rule__CompoundAttribute__Group__2 : rule__CompoundAttribute__Group__2__Impl ;
    public final void rule__CompoundAttribute__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1349:1: ( rule__CompoundAttribute__Group__2__Impl )
            // InternalDotty.g:1350:2: rule__CompoundAttribute__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CompoundAttribute__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundAttribute__Group__2"


    // $ANTLR start "rule__CompoundAttribute__Group__2__Impl"
    // InternalDotty.g:1356:1: rule__CompoundAttribute__Group__2__Impl : ( ( rule__CompoundAttribute__CompoundAssignment_2 ) ) ;
    public final void rule__CompoundAttribute__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1360:1: ( ( ( rule__CompoundAttribute__CompoundAssignment_2 ) ) )
            // InternalDotty.g:1361:1: ( ( rule__CompoundAttribute__CompoundAssignment_2 ) )
            {
            // InternalDotty.g:1361:1: ( ( rule__CompoundAttribute__CompoundAssignment_2 ) )
            // InternalDotty.g:1362:2: ( rule__CompoundAttribute__CompoundAssignment_2 )
            {
             before(grammarAccess.getCompoundAttributeAccess().getCompoundAssignment_2()); 
            // InternalDotty.g:1363:2: ( rule__CompoundAttribute__CompoundAssignment_2 )
            // InternalDotty.g:1363:3: rule__CompoundAttribute__CompoundAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__CompoundAttribute__CompoundAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCompoundAttributeAccess().getCompoundAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundAttribute__Group__2__Impl"


    // $ANTLR start "rule__ColorAttribute__Group__0"
    // InternalDotty.g:1372:1: rule__ColorAttribute__Group__0 : rule__ColorAttribute__Group__0__Impl rule__ColorAttribute__Group__1 ;
    public final void rule__ColorAttribute__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1376:1: ( rule__ColorAttribute__Group__0__Impl rule__ColorAttribute__Group__1 )
            // InternalDotty.g:1377:2: rule__ColorAttribute__Group__0__Impl rule__ColorAttribute__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__ColorAttribute__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ColorAttribute__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ColorAttribute__Group__0"


    // $ANTLR start "rule__ColorAttribute__Group__0__Impl"
    // InternalDotty.g:1384:1: rule__ColorAttribute__Group__0__Impl : ( ( rule__ColorAttribute__Alternatives_0 ) ) ;
    public final void rule__ColorAttribute__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1388:1: ( ( ( rule__ColorAttribute__Alternatives_0 ) ) )
            // InternalDotty.g:1389:1: ( ( rule__ColorAttribute__Alternatives_0 ) )
            {
            // InternalDotty.g:1389:1: ( ( rule__ColorAttribute__Alternatives_0 ) )
            // InternalDotty.g:1390:2: ( rule__ColorAttribute__Alternatives_0 )
            {
             before(grammarAccess.getColorAttributeAccess().getAlternatives_0()); 
            // InternalDotty.g:1391:2: ( rule__ColorAttribute__Alternatives_0 )
            // InternalDotty.g:1391:3: rule__ColorAttribute__Alternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__ColorAttribute__Alternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getColorAttributeAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ColorAttribute__Group__0__Impl"


    // $ANTLR start "rule__ColorAttribute__Group__1"
    // InternalDotty.g:1399:1: rule__ColorAttribute__Group__1 : rule__ColorAttribute__Group__1__Impl rule__ColorAttribute__Group__2 ;
    public final void rule__ColorAttribute__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1403:1: ( rule__ColorAttribute__Group__1__Impl rule__ColorAttribute__Group__2 )
            // InternalDotty.g:1404:2: rule__ColorAttribute__Group__1__Impl rule__ColorAttribute__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__ColorAttribute__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ColorAttribute__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ColorAttribute__Group__1"


    // $ANTLR start "rule__ColorAttribute__Group__1__Impl"
    // InternalDotty.g:1411:1: rule__ColorAttribute__Group__1__Impl : ( '=' ) ;
    public final void rule__ColorAttribute__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1415:1: ( ( '=' ) )
            // InternalDotty.g:1416:1: ( '=' )
            {
            // InternalDotty.g:1416:1: ( '=' )
            // InternalDotty.g:1417:2: '='
            {
             before(grammarAccess.getColorAttributeAccess().getEqualsSignKeyword_1()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getColorAttributeAccess().getEqualsSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ColorAttribute__Group__1__Impl"


    // $ANTLR start "rule__ColorAttribute__Group__2"
    // InternalDotty.g:1426:1: rule__ColorAttribute__Group__2 : rule__ColorAttribute__Group__2__Impl ;
    public final void rule__ColorAttribute__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1430:1: ( rule__ColorAttribute__Group__2__Impl )
            // InternalDotty.g:1431:2: rule__ColorAttribute__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ColorAttribute__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ColorAttribute__Group__2"


    // $ANTLR start "rule__ColorAttribute__Group__2__Impl"
    // InternalDotty.g:1437:1: rule__ColorAttribute__Group__2__Impl : ( ( rule__ColorAttribute__ColorAssignment_2 ) ) ;
    public final void rule__ColorAttribute__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1441:1: ( ( ( rule__ColorAttribute__ColorAssignment_2 ) ) )
            // InternalDotty.g:1442:1: ( ( rule__ColorAttribute__ColorAssignment_2 ) )
            {
            // InternalDotty.g:1442:1: ( ( rule__ColorAttribute__ColorAssignment_2 ) )
            // InternalDotty.g:1443:2: ( rule__ColorAttribute__ColorAssignment_2 )
            {
             before(grammarAccess.getColorAttributeAccess().getColorAssignment_2()); 
            // InternalDotty.g:1444:2: ( rule__ColorAttribute__ColorAssignment_2 )
            // InternalDotty.g:1444:3: rule__ColorAttribute__ColorAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__ColorAttribute__ColorAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getColorAttributeAccess().getColorAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ColorAttribute__Group__2__Impl"


    // $ANTLR start "rule__StyleAttribute__Group__0"
    // InternalDotty.g:1453:1: rule__StyleAttribute__Group__0 : rule__StyleAttribute__Group__0__Impl rule__StyleAttribute__Group__1 ;
    public final void rule__StyleAttribute__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1457:1: ( rule__StyleAttribute__Group__0__Impl rule__StyleAttribute__Group__1 )
            // InternalDotty.g:1458:2: rule__StyleAttribute__Group__0__Impl rule__StyleAttribute__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__StyleAttribute__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StyleAttribute__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StyleAttribute__Group__0"


    // $ANTLR start "rule__StyleAttribute__Group__0__Impl"
    // InternalDotty.g:1465:1: rule__StyleAttribute__Group__0__Impl : ( 'style' ) ;
    public final void rule__StyleAttribute__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1469:1: ( ( 'style' ) )
            // InternalDotty.g:1470:1: ( 'style' )
            {
            // InternalDotty.g:1470:1: ( 'style' )
            // InternalDotty.g:1471:2: 'style'
            {
             before(grammarAccess.getStyleAttributeAccess().getStyleKeyword_0()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getStyleAttributeAccess().getStyleKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StyleAttribute__Group__0__Impl"


    // $ANTLR start "rule__StyleAttribute__Group__1"
    // InternalDotty.g:1480:1: rule__StyleAttribute__Group__1 : rule__StyleAttribute__Group__1__Impl rule__StyleAttribute__Group__2 ;
    public final void rule__StyleAttribute__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1484:1: ( rule__StyleAttribute__Group__1__Impl rule__StyleAttribute__Group__2 )
            // InternalDotty.g:1485:2: rule__StyleAttribute__Group__1__Impl rule__StyleAttribute__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__StyleAttribute__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StyleAttribute__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StyleAttribute__Group__1"


    // $ANTLR start "rule__StyleAttribute__Group__1__Impl"
    // InternalDotty.g:1492:1: rule__StyleAttribute__Group__1__Impl : ( '=' ) ;
    public final void rule__StyleAttribute__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1496:1: ( ( '=' ) )
            // InternalDotty.g:1497:1: ( '=' )
            {
            // InternalDotty.g:1497:1: ( '=' )
            // InternalDotty.g:1498:2: '='
            {
             before(grammarAccess.getStyleAttributeAccess().getEqualsSignKeyword_1()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getStyleAttributeAccess().getEqualsSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StyleAttribute__Group__1__Impl"


    // $ANTLR start "rule__StyleAttribute__Group__2"
    // InternalDotty.g:1507:1: rule__StyleAttribute__Group__2 : rule__StyleAttribute__Group__2__Impl ;
    public final void rule__StyleAttribute__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1511:1: ( rule__StyleAttribute__Group__2__Impl )
            // InternalDotty.g:1512:2: rule__StyleAttribute__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StyleAttribute__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StyleAttribute__Group__2"


    // $ANTLR start "rule__StyleAttribute__Group__2__Impl"
    // InternalDotty.g:1518:1: rule__StyleAttribute__Group__2__Impl : ( ( rule__StyleAttribute__StyleAssignment_2 ) ) ;
    public final void rule__StyleAttribute__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1522:1: ( ( ( rule__StyleAttribute__StyleAssignment_2 ) ) )
            // InternalDotty.g:1523:1: ( ( rule__StyleAttribute__StyleAssignment_2 ) )
            {
            // InternalDotty.g:1523:1: ( ( rule__StyleAttribute__StyleAssignment_2 ) )
            // InternalDotty.g:1524:2: ( rule__StyleAttribute__StyleAssignment_2 )
            {
             before(grammarAccess.getStyleAttributeAccess().getStyleAssignment_2()); 
            // InternalDotty.g:1525:2: ( rule__StyleAttribute__StyleAssignment_2 )
            // InternalDotty.g:1525:3: rule__StyleAttribute__StyleAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__StyleAttribute__StyleAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getStyleAttributeAccess().getStyleAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StyleAttribute__Group__2__Impl"


    // $ANTLR start "rule__LabelAttribute__Group__0"
    // InternalDotty.g:1534:1: rule__LabelAttribute__Group__0 : rule__LabelAttribute__Group__0__Impl rule__LabelAttribute__Group__1 ;
    public final void rule__LabelAttribute__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1538:1: ( rule__LabelAttribute__Group__0__Impl rule__LabelAttribute__Group__1 )
            // InternalDotty.g:1539:2: rule__LabelAttribute__Group__0__Impl rule__LabelAttribute__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__LabelAttribute__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LabelAttribute__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelAttribute__Group__0"


    // $ANTLR start "rule__LabelAttribute__Group__0__Impl"
    // InternalDotty.g:1546:1: rule__LabelAttribute__Group__0__Impl : ( 'label' ) ;
    public final void rule__LabelAttribute__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1550:1: ( ( 'label' ) )
            // InternalDotty.g:1551:1: ( 'label' )
            {
            // InternalDotty.g:1551:1: ( 'label' )
            // InternalDotty.g:1552:2: 'label'
            {
             before(grammarAccess.getLabelAttributeAccess().getLabelKeyword_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getLabelAttributeAccess().getLabelKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelAttribute__Group__0__Impl"


    // $ANTLR start "rule__LabelAttribute__Group__1"
    // InternalDotty.g:1561:1: rule__LabelAttribute__Group__1 : rule__LabelAttribute__Group__1__Impl rule__LabelAttribute__Group__2 ;
    public final void rule__LabelAttribute__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1565:1: ( rule__LabelAttribute__Group__1__Impl rule__LabelAttribute__Group__2 )
            // InternalDotty.g:1566:2: rule__LabelAttribute__Group__1__Impl rule__LabelAttribute__Group__2
            {
            pushFollow(FOLLOW_18);
            rule__LabelAttribute__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LabelAttribute__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelAttribute__Group__1"


    // $ANTLR start "rule__LabelAttribute__Group__1__Impl"
    // InternalDotty.g:1573:1: rule__LabelAttribute__Group__1__Impl : ( '=' ) ;
    public final void rule__LabelAttribute__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1577:1: ( ( '=' ) )
            // InternalDotty.g:1578:1: ( '=' )
            {
            // InternalDotty.g:1578:1: ( '=' )
            // InternalDotty.g:1579:2: '='
            {
             before(grammarAccess.getLabelAttributeAccess().getEqualsSignKeyword_1()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getLabelAttributeAccess().getEqualsSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelAttribute__Group__1__Impl"


    // $ANTLR start "rule__LabelAttribute__Group__2"
    // InternalDotty.g:1588:1: rule__LabelAttribute__Group__2 : rule__LabelAttribute__Group__2__Impl ;
    public final void rule__LabelAttribute__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1592:1: ( rule__LabelAttribute__Group__2__Impl )
            // InternalDotty.g:1593:2: rule__LabelAttribute__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LabelAttribute__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelAttribute__Group__2"


    // $ANTLR start "rule__LabelAttribute__Group__2__Impl"
    // InternalDotty.g:1599:1: rule__LabelAttribute__Group__2__Impl : ( ( rule__LabelAttribute__LabelAssignment_2 ) ) ;
    public final void rule__LabelAttribute__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1603:1: ( ( ( rule__LabelAttribute__LabelAssignment_2 ) ) )
            // InternalDotty.g:1604:1: ( ( rule__LabelAttribute__LabelAssignment_2 ) )
            {
            // InternalDotty.g:1604:1: ( ( rule__LabelAttribute__LabelAssignment_2 ) )
            // InternalDotty.g:1605:2: ( rule__LabelAttribute__LabelAssignment_2 )
            {
             before(grammarAccess.getLabelAttributeAccess().getLabelAssignment_2()); 
            // InternalDotty.g:1606:2: ( rule__LabelAttribute__LabelAssignment_2 )
            // InternalDotty.g:1606:3: rule__LabelAttribute__LabelAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__LabelAttribute__LabelAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getLabelAttributeAccess().getLabelAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelAttribute__Group__2__Impl"


    // $ANTLR start "rule__RankAttribute__Group__0"
    // InternalDotty.g:1615:1: rule__RankAttribute__Group__0 : rule__RankAttribute__Group__0__Impl rule__RankAttribute__Group__1 ;
    public final void rule__RankAttribute__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1619:1: ( rule__RankAttribute__Group__0__Impl rule__RankAttribute__Group__1 )
            // InternalDotty.g:1620:2: rule__RankAttribute__Group__0__Impl rule__RankAttribute__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__RankAttribute__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RankAttribute__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RankAttribute__Group__0"


    // $ANTLR start "rule__RankAttribute__Group__0__Impl"
    // InternalDotty.g:1627:1: rule__RankAttribute__Group__0__Impl : ( 'rankdir' ) ;
    public final void rule__RankAttribute__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1631:1: ( ( 'rankdir' ) )
            // InternalDotty.g:1632:1: ( 'rankdir' )
            {
            // InternalDotty.g:1632:1: ( 'rankdir' )
            // InternalDotty.g:1633:2: 'rankdir'
            {
             before(grammarAccess.getRankAttributeAccess().getRankdirKeyword_0()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getRankAttributeAccess().getRankdirKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RankAttribute__Group__0__Impl"


    // $ANTLR start "rule__RankAttribute__Group__1"
    // InternalDotty.g:1642:1: rule__RankAttribute__Group__1 : rule__RankAttribute__Group__1__Impl rule__RankAttribute__Group__2 ;
    public final void rule__RankAttribute__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1646:1: ( rule__RankAttribute__Group__1__Impl rule__RankAttribute__Group__2 )
            // InternalDotty.g:1647:2: rule__RankAttribute__Group__1__Impl rule__RankAttribute__Group__2
            {
            pushFollow(FOLLOW_19);
            rule__RankAttribute__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RankAttribute__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RankAttribute__Group__1"


    // $ANTLR start "rule__RankAttribute__Group__1__Impl"
    // InternalDotty.g:1654:1: rule__RankAttribute__Group__1__Impl : ( '=' ) ;
    public final void rule__RankAttribute__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1658:1: ( ( '=' ) )
            // InternalDotty.g:1659:1: ( '=' )
            {
            // InternalDotty.g:1659:1: ( '=' )
            // InternalDotty.g:1660:2: '='
            {
             before(grammarAccess.getRankAttributeAccess().getEqualsSignKeyword_1()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getRankAttributeAccess().getEqualsSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RankAttribute__Group__1__Impl"


    // $ANTLR start "rule__RankAttribute__Group__2"
    // InternalDotty.g:1669:1: rule__RankAttribute__Group__2 : rule__RankAttribute__Group__2__Impl ;
    public final void rule__RankAttribute__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1673:1: ( rule__RankAttribute__Group__2__Impl )
            // InternalDotty.g:1674:2: rule__RankAttribute__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__RankAttribute__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RankAttribute__Group__2"


    // $ANTLR start "rule__RankAttribute__Group__2__Impl"
    // InternalDotty.g:1680:1: rule__RankAttribute__Group__2__Impl : ( ( rule__RankAttribute__DirectionAssignment_2 ) ) ;
    public final void rule__RankAttribute__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1684:1: ( ( ( rule__RankAttribute__DirectionAssignment_2 ) ) )
            // InternalDotty.g:1685:1: ( ( rule__RankAttribute__DirectionAssignment_2 ) )
            {
            // InternalDotty.g:1685:1: ( ( rule__RankAttribute__DirectionAssignment_2 ) )
            // InternalDotty.g:1686:2: ( rule__RankAttribute__DirectionAssignment_2 )
            {
             before(grammarAccess.getRankAttributeAccess().getDirectionAssignment_2()); 
            // InternalDotty.g:1687:2: ( rule__RankAttribute__DirectionAssignment_2 )
            // InternalDotty.g:1687:3: rule__RankAttribute__DirectionAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__RankAttribute__DirectionAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getRankAttributeAccess().getDirectionAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RankAttribute__Group__2__Impl"


    // $ANTLR start "rule__Graph__NameAssignment_1"
    // InternalDotty.g:1696:1: rule__Graph__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Graph__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1700:1: ( ( RULE_ID ) )
            // InternalDotty.g:1701:2: ( RULE_ID )
            {
            // InternalDotty.g:1701:2: ( RULE_ID )
            // InternalDotty.g:1702:3: RULE_ID
            {
             before(grammarAccess.getGraphAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getGraphAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Graph__NameAssignment_1"


    // $ANTLR start "rule__Graph__BodyAssignment_2"
    // InternalDotty.g:1711:1: rule__Graph__BodyAssignment_2 : ( ruleCompound ) ;
    public final void rule__Graph__BodyAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1715:1: ( ( ruleCompound ) )
            // InternalDotty.g:1716:2: ( ruleCompound )
            {
            // InternalDotty.g:1716:2: ( ruleCompound )
            // InternalDotty.g:1717:3: ruleCompound
            {
             before(grammarAccess.getGraphAccess().getBodyCompoundParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleCompound();

            state._fsp--;

             after(grammarAccess.getGraphAccess().getBodyCompoundParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Graph__BodyAssignment_2"


    // $ANTLR start "rule__Compound__NodesAssignment_1_0_0_0"
    // InternalDotty.g:1726:1: rule__Compound__NodesAssignment_1_0_0_0 : ( ruleNode ) ;
    public final void rule__Compound__NodesAssignment_1_0_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1730:1: ( ( ruleNode ) )
            // InternalDotty.g:1731:2: ( ruleNode )
            {
            // InternalDotty.g:1731:2: ( ruleNode )
            // InternalDotty.g:1732:3: ruleNode
            {
             before(grammarAccess.getCompoundAccess().getNodesNodeParserRuleCall_1_0_0_0_0()); 
            pushFollow(FOLLOW_2);
            ruleNode();

            state._fsp--;

             after(grammarAccess.getCompoundAccess().getNodesNodeParserRuleCall_1_0_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__NodesAssignment_1_0_0_0"


    // $ANTLR start "rule__Compound__NodesAssignment_1_0_0_1_1"
    // InternalDotty.g:1741:1: rule__Compound__NodesAssignment_1_0_0_1_1 : ( ruleNode ) ;
    public final void rule__Compound__NodesAssignment_1_0_0_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1745:1: ( ( ruleNode ) )
            // InternalDotty.g:1746:2: ( ruleNode )
            {
            // InternalDotty.g:1746:2: ( ruleNode )
            // InternalDotty.g:1747:3: ruleNode
            {
             before(grammarAccess.getCompoundAccess().getNodesNodeParserRuleCall_1_0_0_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleNode();

            state._fsp--;

             after(grammarAccess.getCompoundAccess().getNodesNodeParserRuleCall_1_0_0_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__NodesAssignment_1_0_0_1_1"


    // $ANTLR start "rule__Compound__AttributesAssignment_1_0_1"
    // InternalDotty.g:1756:1: rule__Compound__AttributesAssignment_1_0_1 : ( ruleAttribute ) ;
    public final void rule__Compound__AttributesAssignment_1_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1760:1: ( ( ruleAttribute ) )
            // InternalDotty.g:1761:2: ( ruleAttribute )
            {
            // InternalDotty.g:1761:2: ( ruleAttribute )
            // InternalDotty.g:1762:3: ruleAttribute
            {
             before(grammarAccess.getCompoundAccess().getAttributesAttributeParserRuleCall_1_0_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAttribute();

            state._fsp--;

             after(grammarAccess.getCompoundAccess().getAttributesAttributeParserRuleCall_1_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__AttributesAssignment_1_0_1"


    // $ANTLR start "rule__Compound__SubAssignment_1_0_2"
    // InternalDotty.g:1771:1: rule__Compound__SubAssignment_1_0_2 : ( ruleCompound ) ;
    public final void rule__Compound__SubAssignment_1_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1775:1: ( ( ruleCompound ) )
            // InternalDotty.g:1776:2: ( ruleCompound )
            {
            // InternalDotty.g:1776:2: ( ruleCompound )
            // InternalDotty.g:1777:3: ruleCompound
            {
             before(grammarAccess.getCompoundAccess().getSubCompoundParserRuleCall_1_0_2_0()); 
            pushFollow(FOLLOW_2);
            ruleCompound();

            state._fsp--;

             after(grammarAccess.getCompoundAccess().getSubCompoundParserRuleCall_1_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Compound__SubAssignment_1_0_2"


    // $ANTLR start "rule__LeafNode__NameAssignment_0"
    // InternalDotty.g:1786:1: rule__LeafNode__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__LeafNode__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1790:1: ( ( RULE_ID ) )
            // InternalDotty.g:1791:2: ( RULE_ID )
            {
            // InternalDotty.g:1791:2: ( RULE_ID )
            // InternalDotty.g:1792:3: RULE_ID
            {
             before(grammarAccess.getLeafNodeAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLeafNodeAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafNode__NameAssignment_0"


    // $ANTLR start "rule__LeafNode__AttributesAssignment_1_1"
    // InternalDotty.g:1801:1: rule__LeafNode__AttributesAssignment_1_1 : ( ruleAttribute ) ;
    public final void rule__LeafNode__AttributesAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1805:1: ( ( ruleAttribute ) )
            // InternalDotty.g:1806:2: ( ruleAttribute )
            {
            // InternalDotty.g:1806:2: ( ruleAttribute )
            // InternalDotty.g:1807:3: ruleAttribute
            {
             before(grammarAccess.getLeafNodeAccess().getAttributesAttributeParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAttribute();

            state._fsp--;

             after(grammarAccess.getLeafNodeAccess().getAttributesAttributeParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafNode__AttributesAssignment_1_1"


    // $ANTLR start "rule__LeafNode__AttributesAssignment_1_2_1"
    // InternalDotty.g:1816:1: rule__LeafNode__AttributesAssignment_1_2_1 : ( ruleAttribute ) ;
    public final void rule__LeafNode__AttributesAssignment_1_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1820:1: ( ( ruleAttribute ) )
            // InternalDotty.g:1821:2: ( ruleAttribute )
            {
            // InternalDotty.g:1821:2: ( ruleAttribute )
            // InternalDotty.g:1822:3: ruleAttribute
            {
             before(grammarAccess.getLeafNodeAccess().getAttributesAttributeParserRuleCall_1_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAttribute();

            state._fsp--;

             after(grammarAccess.getLeafNodeAccess().getAttributesAttributeParserRuleCall_1_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafNode__AttributesAssignment_1_2_1"


    // $ANTLR start "rule__SubNode__NameAssignment_1"
    // InternalDotty.g:1831:1: rule__SubNode__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__SubNode__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1835:1: ( ( RULE_ID ) )
            // InternalDotty.g:1836:2: ( RULE_ID )
            {
            // InternalDotty.g:1836:2: ( RULE_ID )
            // InternalDotty.g:1837:3: RULE_ID
            {
             before(grammarAccess.getSubNodeAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getSubNodeAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SubNode__NameAssignment_1"


    // $ANTLR start "rule__SubNode__BodyAssignment_2"
    // InternalDotty.g:1846:1: rule__SubNode__BodyAssignment_2 : ( ruleCompound ) ;
    public final void rule__SubNode__BodyAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1850:1: ( ( ruleCompound ) )
            // InternalDotty.g:1851:2: ( ruleCompound )
            {
            // InternalDotty.g:1851:2: ( ruleCompound )
            // InternalDotty.g:1852:3: ruleCompound
            {
             before(grammarAccess.getSubNodeAccess().getBodyCompoundParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleCompound();

            state._fsp--;

             after(grammarAccess.getSubNodeAccess().getBodyCompoundParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SubNode__BodyAssignment_2"


    // $ANTLR start "rule__ShapeAttribute__ShapeAssignment_2"
    // InternalDotty.g:1861:1: rule__ShapeAttribute__ShapeAssignment_2 : ( ( rule__ShapeAttribute__ShapeAlternatives_2_0 ) ) ;
    public final void rule__ShapeAttribute__ShapeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1865:1: ( ( ( rule__ShapeAttribute__ShapeAlternatives_2_0 ) ) )
            // InternalDotty.g:1866:2: ( ( rule__ShapeAttribute__ShapeAlternatives_2_0 ) )
            {
            // InternalDotty.g:1866:2: ( ( rule__ShapeAttribute__ShapeAlternatives_2_0 ) )
            // InternalDotty.g:1867:3: ( rule__ShapeAttribute__ShapeAlternatives_2_0 )
            {
             before(grammarAccess.getShapeAttributeAccess().getShapeAlternatives_2_0()); 
            // InternalDotty.g:1868:3: ( rule__ShapeAttribute__ShapeAlternatives_2_0 )
            // InternalDotty.g:1868:4: rule__ShapeAttribute__ShapeAlternatives_2_0
            {
            pushFollow(FOLLOW_2);
            rule__ShapeAttribute__ShapeAlternatives_2_0();

            state._fsp--;


            }

             after(grammarAccess.getShapeAttributeAccess().getShapeAlternatives_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ShapeAttribute__ShapeAssignment_2"


    // $ANTLR start "rule__CompoundAttribute__CompoundAssignment_2"
    // InternalDotty.g:1876:1: rule__CompoundAttribute__CompoundAssignment_2 : ( ( rule__CompoundAttribute__CompoundAlternatives_2_0 ) ) ;
    public final void rule__CompoundAttribute__CompoundAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1880:1: ( ( ( rule__CompoundAttribute__CompoundAlternatives_2_0 ) ) )
            // InternalDotty.g:1881:2: ( ( rule__CompoundAttribute__CompoundAlternatives_2_0 ) )
            {
            // InternalDotty.g:1881:2: ( ( rule__CompoundAttribute__CompoundAlternatives_2_0 ) )
            // InternalDotty.g:1882:3: ( rule__CompoundAttribute__CompoundAlternatives_2_0 )
            {
             before(grammarAccess.getCompoundAttributeAccess().getCompoundAlternatives_2_0()); 
            // InternalDotty.g:1883:3: ( rule__CompoundAttribute__CompoundAlternatives_2_0 )
            // InternalDotty.g:1883:4: rule__CompoundAttribute__CompoundAlternatives_2_0
            {
            pushFollow(FOLLOW_2);
            rule__CompoundAttribute__CompoundAlternatives_2_0();

            state._fsp--;


            }

             after(grammarAccess.getCompoundAttributeAccess().getCompoundAlternatives_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundAttribute__CompoundAssignment_2"


    // $ANTLR start "rule__ColorAttribute__ColorAssignment_2"
    // InternalDotty.g:1891:1: rule__ColorAttribute__ColorAssignment_2 : ( ( rule__ColorAttribute__ColorAlternatives_2_0 ) ) ;
    public final void rule__ColorAttribute__ColorAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1895:1: ( ( ( rule__ColorAttribute__ColorAlternatives_2_0 ) ) )
            // InternalDotty.g:1896:2: ( ( rule__ColorAttribute__ColorAlternatives_2_0 ) )
            {
            // InternalDotty.g:1896:2: ( ( rule__ColorAttribute__ColorAlternatives_2_0 ) )
            // InternalDotty.g:1897:3: ( rule__ColorAttribute__ColorAlternatives_2_0 )
            {
             before(grammarAccess.getColorAttributeAccess().getColorAlternatives_2_0()); 
            // InternalDotty.g:1898:3: ( rule__ColorAttribute__ColorAlternatives_2_0 )
            // InternalDotty.g:1898:4: rule__ColorAttribute__ColorAlternatives_2_0
            {
            pushFollow(FOLLOW_2);
            rule__ColorAttribute__ColorAlternatives_2_0();

            state._fsp--;


            }

             after(grammarAccess.getColorAttributeAccess().getColorAlternatives_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ColorAttribute__ColorAssignment_2"


    // $ANTLR start "rule__StyleAttribute__StyleAssignment_2"
    // InternalDotty.g:1906:1: rule__StyleAttribute__StyleAssignment_2 : ( ( rule__StyleAttribute__StyleAlternatives_2_0 ) ) ;
    public final void rule__StyleAttribute__StyleAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1910:1: ( ( ( rule__StyleAttribute__StyleAlternatives_2_0 ) ) )
            // InternalDotty.g:1911:2: ( ( rule__StyleAttribute__StyleAlternatives_2_0 ) )
            {
            // InternalDotty.g:1911:2: ( ( rule__StyleAttribute__StyleAlternatives_2_0 ) )
            // InternalDotty.g:1912:3: ( rule__StyleAttribute__StyleAlternatives_2_0 )
            {
             before(grammarAccess.getStyleAttributeAccess().getStyleAlternatives_2_0()); 
            // InternalDotty.g:1913:3: ( rule__StyleAttribute__StyleAlternatives_2_0 )
            // InternalDotty.g:1913:4: rule__StyleAttribute__StyleAlternatives_2_0
            {
            pushFollow(FOLLOW_2);
            rule__StyleAttribute__StyleAlternatives_2_0();

            state._fsp--;


            }

             after(grammarAccess.getStyleAttributeAccess().getStyleAlternatives_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StyleAttribute__StyleAssignment_2"


    // $ANTLR start "rule__LabelAttribute__LabelAssignment_2"
    // InternalDotty.g:1921:1: rule__LabelAttribute__LabelAssignment_2 : ( RULE_STRING ) ;
    public final void rule__LabelAttribute__LabelAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1925:1: ( ( RULE_STRING ) )
            // InternalDotty.g:1926:2: ( RULE_STRING )
            {
            // InternalDotty.g:1926:2: ( RULE_STRING )
            // InternalDotty.g:1927:3: RULE_STRING
            {
             before(grammarAccess.getLabelAttributeAccess().getLabelSTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getLabelAttributeAccess().getLabelSTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelAttribute__LabelAssignment_2"


    // $ANTLR start "rule__RankAttribute__DirectionAssignment_2"
    // InternalDotty.g:1936:1: rule__RankAttribute__DirectionAssignment_2 : ( ( rule__RankAttribute__DirectionAlternatives_2_0 ) ) ;
    public final void rule__RankAttribute__DirectionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDotty.g:1940:1: ( ( ( rule__RankAttribute__DirectionAlternatives_2_0 ) ) )
            // InternalDotty.g:1941:2: ( ( rule__RankAttribute__DirectionAlternatives_2_0 ) )
            {
            // InternalDotty.g:1941:2: ( ( rule__RankAttribute__DirectionAlternatives_2_0 ) )
            // InternalDotty.g:1942:3: ( rule__RankAttribute__DirectionAlternatives_2_0 )
            {
             before(grammarAccess.getRankAttributeAccess().getDirectionAlternatives_2_0()); 
            // InternalDotty.g:1943:3: ( rule__RankAttribute__DirectionAlternatives_2_0 )
            // InternalDotty.g:1943:4: rule__RankAttribute__DirectionAlternatives_2_0
            {
            pushFollow(FOLLOW_2);
            rule__RankAttribute__DirectionAlternatives_2_0();

            state._fsp--;


            }

             after(grammarAccess.getRankAttributeAccess().getDirectionAlternatives_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RankAttribute__DirectionAssignment_2"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x00000007B060E010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x00000007B020E012L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000001000002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000010000010L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x00000007A000E000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x000000000C000000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000008000002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x00000000000F0000L});

}