package fr.irisa.cairn.graphviz.model;

import com.google.common.collect.Iterables;
import fr.irisa.cairn.graphviz.model.dotty.CompoundGVNode;
import fr.irisa.cairn.graphviz.model.dotty.DottyFactory;
import fr.irisa.cairn.graphviz.model.dotty.GVAttribute;
import fr.irisa.cairn.graphviz.model.dotty.GVEdge;
import fr.irisa.cairn.graphviz.model.dotty.GVGraph;
import fr.irisa.cairn.graphviz.model.dotty.GVNode;
import java.util.Collections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@SuppressWarnings("all")
public class GVUserFactory {
  public GVUserFactory() {
  }
  
  public static GVGraph graph(final boolean acyclic) {
    GVGraph _xblockexpression = null;
    {
      final GVGraph e = DottyFactory.eINSTANCE.createGVGraph();
      e.setAcyclic(acyclic);
      _xblockexpression = e;
    }
    return _xblockexpression;
  }
  
  public static GVNode node(final String name, final int rank, final Iterable<GVAttribute> attributes) {
    GVNode _xblockexpression = null;
    {
      final GVNode e = DottyFactory.eINSTANCE.createGVNode();
      e.setName(name);
      e.setRank(rank);
      EList<GVAttribute> _attributes = e.getAttributes();
      Iterables.<GVAttribute>addAll(_attributes, attributes);
      _xblockexpression = e;
    }
    return _xblockexpression;
  }
  
  public static CompoundGVNode superNode(final String name, final Iterable<GVAttribute> attributes) {
    CompoundGVNode _xblockexpression = null;
    {
      final CompoundGVNode e = DottyFactory.eINSTANCE.createCompoundGVNode();
      e.setName(("cluster_" + name));
      EList<GVAttribute> _attributes = e.getAttributes();
      Iterables.<GVAttribute>addAll(_attributes, attributes);
      _xblockexpression = e;
    }
    return _xblockexpression;
  }
  
  public static GVAttribute label(final String txt) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\"");
    String _replace = txt.replace("\"", "\\\'");
    _builder.append(_replace);
    _builder.append("\"");
    return GVUserFactory.attribute("label", _builder.toString());
  }
  
  public static GVAttribute color(final String txt) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\"");
    String _replace = txt.replace("\"", "\\\'");
    _builder.append(_replace);
    _builder.append("\"");
    return GVUserFactory.attribute("color", _builder.toString());
  }
  
  public static GVAttribute shape(final String txt) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\"");
    String _replace = txt.replace("\"", "\\\'");
    _builder.append(_replace);
    _builder.append("\"");
    return GVUserFactory.attribute("shape", _builder.toString());
  }
  
  public static GVAttribute style(final String txt) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\"");
    String _replace = txt.replace("\"", "\\\'");
    _builder.append(_replace);
    _builder.append("\"");
    return GVUserFactory.attribute("style", _builder.toString());
  }
  
  public static GVAttribute dashed() {
    return GVUserFactory.style("dashed");
  }
  
  public static GVAttribute dotted() {
    return GVUserFactory.style("dotted");
  }
  
  public static GVNode node(final String name, final String label, final Iterable<GVAttribute> attributes) {
    GVAttribute _label = GVUserFactory.label(label);
    Iterable<GVAttribute> _plus = Iterables.<GVAttribute>concat(Collections.<GVAttribute>unmodifiableList(CollectionLiterals.<GVAttribute>newArrayList(_label)), attributes);
    return GVUserFactory.node(name, (-1), _plus);
  }
  
  public static GVNode node(final String name, final String label, final int rank, final Iterable<GVAttribute> attributes) {
    int _max = Math.max(rank, (-1));
    GVAttribute _label = GVUserFactory.label(label);
    Iterable<GVAttribute> _plus = Iterables.<GVAttribute>concat(Collections.<GVAttribute>unmodifiableList(CollectionLiterals.<GVAttribute>newArrayList(_label)), attributes);
    return GVUserFactory.node(name, _max, _plus);
  }
  
  public static GVNode node(final String name, final String label, final int rank) {
    GVAttribute _label = GVUserFactory.label(label);
    return GVUserFactory.node(name, Math.max(rank, (-1)), Collections.<GVAttribute>unmodifiableList(CollectionLiterals.<GVAttribute>newArrayList(_label)));
  }
  
  public static GVNode node(final String name, final String label) {
    GVAttribute _label = GVUserFactory.label(label);
    return GVUserFactory.node(name, (-1), Collections.<GVAttribute>unmodifiableList(CollectionLiterals.<GVAttribute>newArrayList(_label)));
  }
  
  public static GVNode node(final String name) {
    return GVUserFactory.node(name, (-1), Collections.<GVAttribute>unmodifiableList(CollectionLiterals.<GVAttribute>newArrayList()));
  }
  
  public static GVEdge edge(final GVNode src, final GVNode dst) {
    GVEdge _xblockexpression = null;
    {
      if ((src == null)) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Invalid edge with null source");
        throw new UnsupportedOperationException(_builder.toString());
      }
      if ((dst == null)) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("Invalid edge with null destination");
        throw new UnsupportedOperationException(_builder_1.toString());
      }
      final GVEdge e = DottyFactory.eINSTANCE.createGVEdge();
      e.setDest(dst);
      e.setSource(src);
      _xblockexpression = e;
    }
    return _xblockexpression;
  }
  
  public static GVEdge edge(final GVNode src, final GVNode dst, final String labelTxt) {
    GVEdge _xblockexpression = null;
    {
      final GVEdge e = GVUserFactory.edge(src, dst);
      EList<GVAttribute> _attributes = e.getAttributes();
      GVAttribute _label = GVUserFactory.label(labelTxt);
      _attributes.add(_label);
      _xblockexpression = e;
    }
    return _xblockexpression;
  }
  
  public static GVEdge edge(final GVNode src, final GVNode dst, final Iterable<GVAttribute> attributes) {
    GVEdge _xblockexpression = null;
    {
      final GVEdge e = GVUserFactory.edge(src, dst);
      EList<GVAttribute> _attributes = e.getAttributes();
      Iterables.<GVAttribute>addAll(_attributes, attributes);
      _xblockexpression = e;
    }
    return _xblockexpression;
  }
  
  public static GVEdge edge(final GVNode src, final GVNode dst, final String labelTxt, final Iterable<GVAttribute> attributes) {
    GVEdge _xblockexpression = null;
    {
      final GVEdge e = GVUserFactory.edge(src, dst, labelTxt);
      EList<GVAttribute> _attributes = e.getAttributes();
      Iterables.<GVAttribute>addAll(_attributes, attributes);
      _xblockexpression = e;
    }
    return _xblockexpression;
  }
  
  public static GVAttribute attribute(final String name, final String value) {
    GVAttribute _xblockexpression = null;
    {
      final GVAttribute e = DottyFactory.eINSTANCE.createGVAttribute();
      e.setName(name);
      e.setValue(value);
      _xblockexpression = e;
    }
    return _xblockexpression;
  }
  
  public static CompoundGVNode compound(final String name, final Iterable<GVAttribute> attributes, final Iterable<GVNode> children) {
    CompoundGVNode _xblockexpression = null;
    {
      final CompoundGVNode e = DottyFactory.eINSTANCE.createCompoundGVNode();
      e.setName(name);
      EList<GVNode> _nodes = e.getNodes();
      Iterables.<GVNode>addAll(_nodes, children);
      EList<GVAttribute> _attributes = e.getAttributes();
      Iterables.<GVAttribute>addAll(_attributes, attributes);
      _xblockexpression = e;
    }
    return _xblockexpression;
  }
}
