package fr.irisa.cairn.graphviz.model;

import fr.irisa.cairn.graphviz.model.dotty.CompoundGVNode;
import fr.irisa.cairn.graphviz.model.dotty.GVAttribute;
import fr.irisa.cairn.graphviz.model.dotty.GVEdge;
import fr.irisa.cairn.graphviz.model.dotty.GVGraph;
import fr.irisa.cairn.graphviz.model.dotty.GVNode;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Pattern;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

@SuppressWarnings("all")
public class GVChecker {
  private HashMap<String, GVNode> nameMap = new HashMap<String, GVNode>();
  
  private GVGraph g;
  
  public GVChecker(final GVGraph g) {
    this.g = g;
  }
  
  public GVNode check() {
    return this.check(this.g);
  }
  
  protected GVNode _check(final GVGraph n) {
    this.nameMap.clear();
    final EList<GVNode> n_nodes = EMFUtils.<GVNode>eAllContentsInstancesOf(n, GVNode.class);
    int _size = n.getNodes().size();
    boolean _equals = (_size == 0);
    if (_equals) {
    }
    for (final GVNode _n : n_nodes) {
      this.check(_n);
    }
    EList<GVEdge> _edges = n.getEdges();
    for (final GVEdge e : _edges) {
      {
        this.check(e);
        boolean _contains = n_nodes.contains(e.getSource());
        boolean _not = (!_contains);
        if (_not) {
          GVNode _source = e.getSource();
          String _plus = ("Inconsistent graphviz model : source node " + _source);
          String _plus_1 = (_plus + " does not belong to the current graph");
          throw new RuntimeException(_plus_1);
        }
        boolean _contains_1 = n_nodes.contains(e.getDest());
        boolean _not_1 = (!_contains_1);
        if (_not_1) {
          GVNode _dest = e.getDest();
          String _plus_2 = ("Inconsistent graphviz model : destination node " + _dest);
          String _plus_3 = (_plus_2 + " does not belong to the current graph");
          throw new RuntimeException(_plus_3);
        }
      }
    }
    return null;
  }
  
  protected GVNode _check(final CompoundGVNode n) {
    this.checkName(n.getName(), n);
    int _size = n.getNodes().size();
    boolean _equals = (_size == 0);
    if (_equals) {
      throw new RuntimeException("Inconsistent graphviz node");
    }
    EList<GVNode> _nodes = n.getNodes();
    for (final GVNode _n : _nodes) {
      this.check(_n);
    }
    EList<GVEdge> _edges = n.getEdges();
    for (final GVEdge e : _edges) {
      {
        this.check(e);
        boolean _contains = n.getNodes().contains(e.getSource());
        boolean _not = (!_contains);
        if (_not) {
          throw new RuntimeException("Edge in compound node has external source");
        }
        boolean _contains_1 = n.getNodes().contains(e.getDest());
        boolean _not_1 = (!_contains_1);
        if (_not_1) {
          throw new RuntimeException("Inconsistent graphviz model : inconsitent edge");
        }
      }
    }
    return null;
  }
  
  public void checkName(final String s, final Object o) {
    final Pattern p = Pattern.compile("([a-zA-Z_$][0-9a-zA-Z_$]*)");
    boolean _matches = p.matcher(s).matches();
    boolean _not = (!_matches);
    if (_not) {
      String _simpleName = o.getClass().getSimpleName();
      String _plus = ((("Invalid name " + s) + " for ") + _simpleName);
      throw new RuntimeException(_plus);
    }
  }
  
  protected GVNode _check(final GVNode n) {
    GVNode _xblockexpression = null;
    {
      this.checkName(n.getName(), n);
      GVNode _xifexpression = null;
      if ((this.nameMap.containsKey(n.getName()) && (this.nameMap.get(n.getName()) != n))) {
        GVNode _get = this.nameMap.get(n.getName());
        String _plus = ((("Inconsistent graphviz model : nodes " + n) + " and ") + _get);
        String _plus_1 = (_plus + " have the same id ");
        String _name = n.getName();
        String _plus_2 = (_plus_1 + _name);
        throw new RuntimeException(_plus_2);
      } else {
        _xifexpression = this.nameMap.put(n.getName(), n);
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  protected GVNode _check(final GVAttribute a) {
    this.checkName(a.getName(), a);
    return null;
  }
  
  protected GVNode _check(final GVEdge e) {
    GVNode _source = e.getSource();
    boolean _tripleEquals = (_source == null);
    if (_tripleEquals) {
      throw new RuntimeException("Inconsistent graphviz model");
    }
    GVNode _dest = e.getDest();
    boolean _tripleEquals_1 = (_dest == null);
    if (_tripleEquals_1) {
      throw new RuntimeException("Inconsistent graphviz model");
    }
    return null;
  }
  
  public GVNode check(final EObject n) {
    if (n instanceof CompoundGVNode) {
      return _check((CompoundGVNode)n);
    } else if (n instanceof GVEdge) {
      return _check((GVEdge)n);
    } else if (n instanceof GVGraph) {
      return _check((GVGraph)n);
    } else if (n instanceof GVNode) {
      return _check((GVNode)n);
    } else if (n instanceof GVAttribute) {
      return _check((GVAttribute)n);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(n).toString());
    }
  }
}
