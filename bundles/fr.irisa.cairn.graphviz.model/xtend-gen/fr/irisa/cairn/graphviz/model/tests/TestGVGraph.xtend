package fr.irisa.cairn.graphviz.model.tests

import fr.irisa.cairn.graphviz.model.GVChecker
import fr.irisa.cairn.graphviz.model.GVPrettyPrinter
import static fr.irisa.cairn.graphviz.model.GVUserFactory.*

class TestGVGraph {
	def static void main(String[] args) {
		val g = graph(true)
		val v0 = node("V0", "node0")
		val v1 = node("V1", "node1")
		val v2 = node("V2", "node2")
		val v3 = 	node("V3", "node3")
		val v4 = 	node("V4", "node4")
		val v5 = node("V5", "node5")
		val c0 = compound("cluster_C0", #[], #[v3,v4])

		val e0= edge(v0,v1)
		val e1= edge(v1,c0)
		val e2= edge(v2,v3)
		val e3= edge(v3,v4)
		val e4= edge(v3,v2)
		val e5= edge(v4,v5)

		g.nodes+= #[v0,v1,v2,v5,c0];
		g.edges+= #[e0,e1,e2,e4,e5];
		c0.edges+= #[e3];
		
		new GVChecker(g).check
		
		GVPrettyPrinter.save("test.dot", g)
				
		
	}
}