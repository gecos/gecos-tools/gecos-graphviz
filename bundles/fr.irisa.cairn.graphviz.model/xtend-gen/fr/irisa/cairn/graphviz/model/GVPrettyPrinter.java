package fr.irisa.cairn.graphviz.model;

import com.google.common.collect.Iterables;
import fr.irisa.cairn.graphviz.model.dotty.CompoundGVNode;
import fr.irisa.cairn.graphviz.model.dotty.GVAttribute;
import fr.irisa.cairn.graphviz.model.dotty.GVEdge;
import fr.irisa.cairn.graphviz.model.dotty.GVGraph;
import fr.irisa.cairn.graphviz.model.dotty.GVNode;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionExtensions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.Functions.Function2;
import org.eclipse.xtext.xbase.lib.IntegerRange;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.eclipse.xtext.xbase.lib.MapExtensions;

@SuppressWarnings("all")
public class GVPrettyPrinter {
  private static Function1<GVNode, Boolean> filter;
  
  public static void save(final String path, final GVGraph g) {
    try {
      final PrintStream ps = new PrintStream(path);
      final GVPrettyPrinter pp = new GVPrettyPrinter();
      int _size = IterableExtensions.size(Iterables.<CompoundGVNode>filter(g.getNodes(), CompoundGVNode.class));
      boolean _notEquals = (_size != 0);
      if (_notEquals) {
        pp.buildRankMap(g);
        pp.noRank = true;
      }
      ps.append(pp.print(g));
      final Function1<GVNode, Boolean> _function = (GVNode it) -> {
        return Boolean.valueOf(true);
      };
      GVPrettyPrinter.filter = _function;
      ps.close();
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public static void save(final String path, final GVGraph g, final Function1<GVNode, Boolean> nodeFilter) {
    try {
      final PrintStream ps = new PrintStream(path);
      final GVPrettyPrinter pp = new GVPrettyPrinter();
      GVPrettyPrinter.filter = nodeFilter;
      ps.append(pp.print(g));
      ps.close();
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public GVPrettyPrinter() {
  }
  
  private Map<Integer, List<GVNode>> rankMap = new HashMap<Integer, List<GVNode>>();
  
  private boolean noRank = false;
  
  public void buildRankMap(final GVGraph g) {
    this.rankMap.clear();
    EList<GVNode> _nodes = g.getNodes();
    for (final GVNode n : _nodes) {
      {
        boolean _containsKey = this.rankMap.containsKey(Integer.valueOf(n.getRank()));
        boolean _not = (!_containsKey);
        if (_not) {
          int _rank = n.getRank();
          ArrayList<GVNode> _arrayList = new ArrayList<GVNode>();
          this.rankMap.put(Integer.valueOf(_rank), _arrayList);
        }
        List<GVNode> _get = this.rankMap.get(Integer.valueOf(n.getRank()));
        _get.add(n);
      }
    }
  }
  
  public HashSet<GVNode> extractNonRankedNode(final GVGraph g) {
    HashSet<GVNode> _xblockexpression = null;
    {
      EList<GVNode> _nodes = g.getNodes();
      HashSet<GVNode> set = new HashSet<GVNode>(_nodes);
      final Function2<Integer, List<GVNode>, Boolean> _function = (Integer k, List<GVNode> v) -> {
        return Boolean.valueOf(((k).intValue() != 1));
      };
      CollectionExtensions.<GVNode>removeAll(set, Iterables.<GVNode>concat(MapExtensions.<Integer, List<GVNode>>filter(this.rankMap, _function).values()));
      _xblockexpression = set;
    }
    return _xblockexpression;
  }
  
  protected CharSequence _print(final GVGraph g) {
    CharSequence _xblockexpression = null;
    {
      this.buildRankMap(g);
      final int minRank = 1;
      final Function1<GVNode, Boolean> _function = (GVNode it) -> {
        int _rank = it.getRank();
        return Boolean.valueOf((_rank < minRank));
      };
      final Iterable<GVNode> nonRankedNode = IterableExtensions.<GVNode>filter(g.getNodes(), _function);
      Integer _xifexpression = null;
      int _size = g.getNodes().size();
      boolean _greaterThan = (_size > 0);
      if (_greaterThan) {
        final Function1<GVNode, Integer> _function_1 = (GVNode it) -> {
          return Integer.valueOf(it.getRank());
        };
        _xifexpression = IterableExtensions.<Integer>max(ListExtensions.<GVNode, Integer>map(g.getNodes(), _function_1));
      } else {
        _xifexpression = Integer.valueOf((-1));
      }
      final Integer maxRank = _xifexpression;
      CharSequence _xifexpression_1 = null;
      if ((((maxRank).intValue() == (-1)) || this.noRank)) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("digraph Graph_");
        String _name = g.getName();
        _builder.append(_name);
        _builder.append(" {");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("compound=true;");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("// non ranked nodes");
        _builder.newLine();
        _builder.append("\t");
        _builder.newLine();
        {
          int _size_1 = g.getDefaultNodeAttributes().size();
          boolean _notEquals = (_size_1 != 0);
          if (_notEquals) {
            _builder.append("\t");
            _builder.append("graph[");
            {
              EList<GVAttribute> _defaultNodeAttributes = g.getDefaultNodeAttributes();
              boolean _hasElements = false;
              for(final GVAttribute a : _defaultNodeAttributes) {
                if (!_hasElements) {
                  _hasElements = true;
                } else {
                  _builder.appendImmediate(",", "\t");
                }
                _builder.append(" ");
                Object _print = this.print(a);
                _builder.append(_print, "\t");
                _builder.append(" ");
              }
            }
            _builder.append(" ]");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("node [");
            {
              EList<GVAttribute> _defaultNodeAttributes_1 = g.getDefaultNodeAttributes();
              boolean _hasElements_1 = false;
              for(final GVAttribute a_1 : _defaultNodeAttributes_1) {
                if (!_hasElements_1) {
                  _hasElements_1 = true;
                } else {
                  _builder.appendImmediate(",", "\t");
                }
                _builder.append(" ");
                Object _print_1 = this.print(a_1);
                _builder.append(_print_1, "\t");
                _builder.append(" ");
              }
            }
            _builder.append(" ]");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("\t");
        _builder.newLine();
        {
          int _size_2 = g.getDefaultEdgeAttributes().size();
          boolean _notEquals_1 = (_size_2 != 0);
          if (_notEquals_1) {
            _builder.append("\t");
            _builder.append("edge [");
            {
              EList<GVAttribute> _defaultEdgeAttributes = g.getDefaultEdgeAttributes();
              boolean _hasElements_2 = false;
              for(final GVAttribute a_2 : _defaultEdgeAttributes) {
                if (!_hasElements_2) {
                  _hasElements_2 = true;
                } else {
                  _builder.appendImmediate(",", "\t");
                }
                _builder.append(" ");
                Object _print_2 = this.print(a_2);
                _builder.append(_print_2, "\t");
                _builder.append(" ");
              }
            }
            _builder.append(" ]");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("\t");
        _builder.newLine();
        {
          EList<GVNode> _nodes = g.getNodes();
          for(final GVNode n : _nodes) {
            _builder.append("\t");
            _builder.append("// node ");
            String _name_1 = n.getName();
            _builder.append(_name_1, "\t");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            Object _print_3 = this.print(n);
            _builder.append(_print_3, "\t");
            _builder.append(" ");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("\t");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("// edges ");
        _builder.newLine();
        {
          EList<GVEdge> _edges = g.getEdges();
          for(final GVEdge e : _edges) {
            _builder.append("\t");
            Object _print_4 = this.print(e);
            _builder.append(_print_4, "\t");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("}");
        _builder.newLine();
        _xifexpression_1 = _builder;
      } else {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("digraph Graph_");
        String _name_2 = g.getName();
        _builder_1.append(_name_2);
        _builder_1.append(" {");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("\t");
        _builder_1.append("compound=true;");
        _builder_1.newLine();
        _builder_1.append("\t");
        _builder_1.append("rankdir=TB;");
        _builder_1.newLine();
        _builder_1.append("\t  ");
        _builder_1.newLine();
        {
          int _size_3 = g.getDefaultNodeAttributes().size();
          boolean _notEquals_2 = (_size_3 != 0);
          if (_notEquals_2) {
            _builder_1.append("node [");
            {
              EList<GVAttribute> _defaultNodeAttributes_2 = g.getDefaultNodeAttributes();
              for(final GVAttribute a_3 : _defaultNodeAttributes_2) {
                _builder_1.append(" ");
                Object _print_5 = this.print(a_3);
                _builder_1.append(_print_5);
                _builder_1.append(" ");
              }
            }
            _builder_1.append(" ]");
            _builder_1.newLineIfNotEmpty();
          }
        }
        _builder_1.newLine();
        {
          int _size_4 = g.getDefaultEdgeAttributes().size();
          boolean _notEquals_3 = (_size_4 != 0);
          if (_notEquals_3) {
            _builder_1.append("edge [");
            {
              EList<GVAttribute> _defaultEdgeAttributes_1 = g.getDefaultEdgeAttributes();
              for(final GVAttribute a_4 : _defaultEdgeAttributes_1) {
                _builder_1.append(" ");
                Object _print_6 = this.print(a_4);
                _builder_1.append(_print_6);
                _builder_1.append(" ");
              }
            }
            _builder_1.append(" ]");
            _builder_1.newLineIfNotEmpty();
          }
        }
        _builder_1.newLine();
        _builder_1.append("\t");
        _builder_1.append("// ranked nodes");
        _builder_1.newLine();
        {
          IntegerRange _upTo = new IntegerRange(minRank, (maxRank).intValue());
          for(final Integer r : _upTo) {
            _builder_1.append("\t");
            _builder_1.append("{ ");
            _builder_1.newLine();
            _builder_1.append("\t");
            _builder_1.append("\t");
            _builder_1.append("// Rank ");
            _builder_1.append(r, "\t\t");
            _builder_1.newLineIfNotEmpty();
            _builder_1.append("\t");
            _builder_1.append("\t");
            _builder_1.append("rank = same;");
            _builder_1.newLine();
            {
              boolean _containsKey = this.rankMap.containsKey(r);
              if (_containsKey) {
                {
                  List<GVNode> _get = this.rankMap.get(r);
                  for(final GVNode n_1 : _get) {
                    _builder_1.append("\t");
                    _builder_1.append("\t");
                    Object _print_7 = this.print(n_1);
                    _builder_1.append(_print_7, "\t\t");
                    _builder_1.append(" ");
                    _builder_1.newLineIfNotEmpty();
                  }
                }
              }
            }
            _builder_1.append("\t");
            _builder_1.append("}");
            _builder_1.newLine();
          }
        }
        _builder_1.append("\t");
        _builder_1.newLine();
        _builder_1.newLine();
        _builder_1.append("\t");
        _builder_1.append("// non ranked nodes");
        _builder_1.newLine();
        {
          for(final GVNode n_2 : nonRankedNode) {
            _builder_1.append("\t");
            _builder_1.append("// node ");
            String _name_3 = n_2.getName();
            _builder_1.append(_name_3, "\t");
            _builder_1.newLineIfNotEmpty();
            _builder_1.append("\t");
            Object _print_8 = this.print(n_2);
            _builder_1.append(_print_8, "\t");
            _builder_1.append(" ");
            _builder_1.newLineIfNotEmpty();
          }
        }
        _builder_1.append("\t");
        _builder_1.newLine();
        _builder_1.append("\t");
        _builder_1.append("// edges ");
        _builder_1.newLine();
        {
          EList<GVEdge> _edges_1 = g.getEdges();
          for(final GVEdge e_1 : _edges_1) {
            _builder_1.append("\t");
            Object _print_9 = this.print(e_1);
            _builder_1.append(_print_9, "\t");
            _builder_1.newLineIfNotEmpty();
          }
        }
        _builder_1.append("}");
        _builder_1.newLine();
        _xifexpression_1 = _builder_1;
      }
      _xblockexpression = _xifexpression_1;
    }
    return _xblockexpression;
  }
  
  protected CharSequence _print(final CompoundGVNode n) {
    CharSequence _xblockexpression = null;
    {
      int _size = IterableExtensions.size(Iterables.<CompoundGVNode>filter(n.getNodes(), CompoundGVNode.class));
      final boolean compound = (_size != 0);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("subgraph ");
      String _name = n.getName();
      _builder.append(_name);
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append("compound = true;");
      _builder.newLine();
      {
        EList<GVAttribute> _attributes = n.getAttributes();
        for(final GVAttribute e : _attributes) {
          _builder.append("\t\t\t");
          Object _print = this.print(e);
          _builder.append(_print, "\t\t\t");
          _builder.newLineIfNotEmpty();
        }
      }
      {
        EList<GVNode> _nodes = n.getNodes();
        for(final GVNode c : _nodes) {
          _builder.append("\t\t\t");
          Object _print_1 = this.print(c);
          _builder.append(_print_1, "\t\t\t");
          _builder.append(" ");
          _builder.newLineIfNotEmpty();
        }
      }
      {
        EList<GVEdge> _edges = n.getEdges();
        for(final GVEdge e_1 : _edges) {
          _builder.append("\t\t\t");
          Object _print_2 = this.print(e_1);
          _builder.append(_print_2, "\t\t\t");
          _builder.append(" ");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append("\t\t");
      _builder.append("}");
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  protected CharSequence _print(final GVNode n) {
    CharSequence _xifexpression = null;
    int _size = n.getAttributes().size();
    boolean _equals = (_size == 0);
    if (_equals) {
      StringConcatenation _builder = new StringConcatenation();
      String _name = n.getName();
      _builder.append(_name);
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      String _name_1 = n.getName();
      _builder_1.append(_name_1);
      _builder_1.append(" [");
      {
        EList<GVAttribute> _attributes = n.getAttributes();
        boolean _hasElements = false;
        for(final GVAttribute e : _attributes) {
          if (!_hasElements) {
            _hasElements = true;
          } else {
            _builder_1.appendImmediate(",", "");
          }
          Object _print = this.print(e);
          _builder_1.append(_print);
        }
      }
      _builder_1.append("];");
      _builder_1.newLineIfNotEmpty();
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  protected CharSequence _print(final GVAttribute n) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append(" ");
    String _name = n.getName();
    _builder.append(_name, " ");
    _builder.append("=");
    String _value = n.getValue();
    _builder.append(_value, " ");
    return _builder;
  }
  
  protected CharSequence _print(final GVEdge e) {
    CharSequence _xifexpression = null;
    int _size = e.getAttributes().size();
    boolean _equals = (_size == 0);
    if (_equals) {
      StringConcatenation _builder = new StringConcatenation();
      String _name = e.getSource().getName();
      _builder.append(_name);
      _builder.append("->");
      String _name_1 = e.getDest().getName();
      _builder.append(_name_1);
      _builder.append("; ");
      _builder.newLineIfNotEmpty();
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      String _name_2 = e.getSource().getName();
      _builder_1.append(_name_2);
      _builder_1.append("->");
      String _name_3 = e.getDest().getName();
      _builder_1.append(_name_3);
      _builder_1.append(" [");
      {
        EList<GVAttribute> _attributes = e.getAttributes();
        boolean _hasElements = false;
        for(final GVAttribute a : _attributes) {
          if (!_hasElements) {
            _hasElements = true;
          } else {
            _builder_1.appendImmediate(",", "");
          }
          Object _print = this.print(a);
          _builder_1.append(_print);
        }
      }
      _builder_1.append("] ;");
      _builder_1.newLineIfNotEmpty();
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  public CharSequence print(final EObject n) {
    if (n instanceof CompoundGVNode) {
      return _print((CompoundGVNode)n);
    } else if (n instanceof GVEdge) {
      return _print((GVEdge)n);
    } else if (n instanceof GVGraph) {
      return _print((GVGraph)n);
    } else if (n instanceof GVNode) {
      return _print((GVNode)n);
    } else if (n instanceof GVAttribute) {
      return _print((GVAttribute)n);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(n).toString());
    }
  }
}
