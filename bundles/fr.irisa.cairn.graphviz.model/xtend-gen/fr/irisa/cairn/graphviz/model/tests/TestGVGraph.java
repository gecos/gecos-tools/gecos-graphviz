package fr.irisa.cairn.graphviz.model.tests;

import com.google.common.collect.Iterables;
import fr.irisa.cairn.graphviz.model.GVChecker;
import fr.irisa.cairn.graphviz.model.GVPrettyPrinter;
import fr.irisa.cairn.graphviz.model.GVUserFactory;
import fr.irisa.cairn.graphviz.model.dotty.CompoundGVNode;
import fr.irisa.cairn.graphviz.model.dotty.GVAttribute;
import fr.irisa.cairn.graphviz.model.dotty.GVEdge;
import fr.irisa.cairn.graphviz.model.dotty.GVGraph;
import fr.irisa.cairn.graphviz.model.dotty.GVNode;
import java.util.Collections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@SuppressWarnings("all")
public class TestGVGraph {
  public static void main(final String[] args) {
    final GVGraph g = GVUserFactory.graph(true);
    final GVNode v0 = GVUserFactory.node("V0", "node0");
    final GVNode v1 = GVUserFactory.node("V1", "node1");
    final GVNode v2 = GVUserFactory.node("V2", "node2");
    final GVNode v3 = GVUserFactory.node("V3", "node3");
    final GVNode v4 = GVUserFactory.node("V4", "node4");
    final GVNode v5 = GVUserFactory.node("V5", "node5");
    final CompoundGVNode c0 = GVUserFactory.compound("cluster_C0", Collections.<GVAttribute>unmodifiableList(CollectionLiterals.<GVAttribute>newArrayList()), Collections.<GVNode>unmodifiableList(CollectionLiterals.<GVNode>newArrayList(v3, v4)));
    final GVEdge e0 = GVUserFactory.edge(v0, v1);
    final GVEdge e1 = GVUserFactory.edge(v1, c0);
    final GVEdge e2 = GVUserFactory.edge(v2, v3);
    final GVEdge e3 = GVUserFactory.edge(v3, v4);
    final GVEdge e4 = GVUserFactory.edge(v3, v2);
    final GVEdge e5 = GVUserFactory.edge(v4, v5);
    EList<GVNode> _nodes = g.getNodes();
    Iterables.<GVNode>addAll(_nodes, Collections.<GVNode>unmodifiableList(CollectionLiterals.<GVNode>newArrayList(v0, v1, v2, v5, c0)));
    EList<GVEdge> _edges = g.getEdges();
    Iterables.<GVEdge>addAll(_edges, Collections.<GVEdge>unmodifiableList(CollectionLiterals.<GVEdge>newArrayList(e0, e1, e2, e4, e5)));
    EList<GVEdge> _edges_1 = c0.getEdges();
    Iterables.<GVEdge>addAll(_edges_1, Collections.<GVEdge>unmodifiableList(CollectionLiterals.<GVEdge>newArrayList(e3)));
    new GVChecker(g).check();
    GVPrettyPrinter.save("test.dot", g);
  }
}
