package fr.irisa.cairn.graphviz.model;

import fr.irisa.cairn.graphviz.model.GVUserFactory;
import fr.irisa.cairn.graphviz.model.dotty.GVEdge;
import fr.irisa.cairn.graphviz.model.dotty.GVGraph;
import fr.irisa.cairn.graphviz.model.dotty.GVNode;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.emf.common.util.EList;

@SuppressWarnings("all")
public abstract class GVAdapter<V extends Object, E extends Object> extends GVUserFactory {
  protected Map<V, GVNode> nodeMap = new HashMap<V, GVNode>();
  
  protected GVGraph graph;
  
  protected int id = 0;
  
  public GVAdapter(final String name, final boolean acyclic) {
    this.graph = GVUserFactory.graph(true);
    this.graph.setName(name);
  }
  
  public void buildDottyModel() {
    Iterable<V> _allNodes = this.getAllNodes();
    for (final V n : _allNodes) {
      this.addNode(n);
    }
    Iterable<E> _allEdges = this.getAllEdges();
    for (final E e : _allEdges) {
      this.addEdge(e);
    }
  }
  
  public abstract Iterable<E> getAllEdges();
  
  public abstract Iterable<V> getAllNodes();
  
  public abstract boolean isSubGraph(final V a);
  
  public abstract V getSource(final E e);
  
  public abstract V getDest(final E e);
  
  public GVGraph getDottyModel() {
    return this.graph;
  }
  
  public boolean addNode(final V obj) {
    boolean _xblockexpression = false;
    {
      int _plusPlus = this.id++;
      String _plus = ("n" + Integer.valueOf(_plusPlus));
      final GVNode node = GVUserFactory.node(_plus);
      this.nodeMap.put(obj, node);
      EList<GVNode> _nodes = this.graph.getNodes();
      _xblockexpression = _nodes.add(node);
    }
    return _xblockexpression;
  }
  
  public boolean addEdge(final E e) {
    boolean _xblockexpression = false;
    {
      final V src = this.getSource(e);
      final V dst = this.getDest(e);
      boolean _containsKey = this.nodeMap.containsKey(src);
      boolean _not = (!_containsKey);
      if (_not) {
        throw new UnsupportedOperationException((("Source node " + src) + " is not a known node"));
      }
      boolean _containsKey_1 = this.nodeMap.containsKey(dst);
      boolean _not_1 = (!_containsKey_1);
      if (_not_1) {
        throw new UnsupportedOperationException((("Dest node " + dst) + " is not a known node"));
      }
      final GVEdge edge = GVUserFactory.edge(this.nodeMap.get(src), this.nodeMap.get(dst));
      EList<GVEdge> _edges = this.graph.getEdges();
      _xblockexpression = _edges.add(edge);
    }
    return _xblockexpression;
  }
}
