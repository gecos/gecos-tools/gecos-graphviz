package fr.irisa.cairn.graphviz.model;

import fr.irisa.cairn.graphviz.model.dotty.GVGraph;
import fr.irisa.cairn.graphviz.model.dotty.GVNode;
import org.eclipse.emf.common.util.EList;

@SuppressWarnings("all")
public class GVFixer {
  public static void fix(final GVGraph g) {
    int id = 0;
    EList<GVNode> _nodes = g.getNodes();
    for (final GVNode n : _nodes) {
      int _plusPlus = id++;
      String _plus = ("n" + Integer.valueOf(_plusPlus));
      n.setName(_plus);
    }
  }
}
