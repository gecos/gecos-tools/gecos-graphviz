/**
 */
package fr.irisa.cairn.graphviz.model.dotty;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>GV Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.GVNode#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.GVNode#getRank <em>Rank</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage#getGVNode()
 * @model
 * @generated
 */
public interface GVNode extends GVObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage#getGVNode_Name()
	 * @model unique="false"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.graphviz.model.dotty.GVNode#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Rank</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rank</em>' attribute.
	 * @see #setRank(int)
	 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage#getGVNode_Rank()
	 * @model unique="false"
	 * @generated
	 */
	int getRank();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.graphviz.model.dotty.GVNode#getRank <em>Rank</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rank</em>' attribute.
	 * @see #getRank()
	 * @generated
	 */
	void setRank(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 * @generated
	 */
	String toString();

} // GVNode
