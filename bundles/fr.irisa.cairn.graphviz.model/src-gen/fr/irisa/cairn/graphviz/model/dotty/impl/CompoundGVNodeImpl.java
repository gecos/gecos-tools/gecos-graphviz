/**
 */
package fr.irisa.cairn.graphviz.model.dotty.impl;

import fr.irisa.cairn.graphviz.model.dotty.CompoundGVNode;
import fr.irisa.cairn.graphviz.model.dotty.DottyPackage;
import fr.irisa.cairn.graphviz.model.dotty.GVEdge;
import fr.irisa.cairn.graphviz.model.dotty.GVNode;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Compound GV Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.impl.CompoundGVNodeImpl#getNodes <em>Nodes</em>}</li>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.impl.CompoundGVNodeImpl#getEdges <em>Edges</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CompoundGVNodeImpl extends GVNodeImpl implements CompoundGVNode {
	/**
	 * The cached value of the '{@link #getNodes() <em>Nodes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<GVNode> nodes;

	/**
	 * The cached value of the '{@link #getEdges() <em>Edges</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<GVEdge> edges;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompoundGVNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DottyPackage.Literals.COMPOUND_GV_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GVNode> getNodes() {
		if (nodes == null) {
			nodes = new EObjectContainmentEList<GVNode>(GVNode.class, this, DottyPackage.COMPOUND_GV_NODE__NODES);
		}
		return nodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GVEdge> getEdges() {
		if (edges == null) {
			edges = new EObjectContainmentEList<GVEdge>(GVEdge.class, this, DottyPackage.COMPOUND_GV_NODE__EDGES);
		}
		return edges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DottyPackage.COMPOUND_GV_NODE__NODES:
				return ((InternalEList<?>)getNodes()).basicRemove(otherEnd, msgs);
			case DottyPackage.COMPOUND_GV_NODE__EDGES:
				return ((InternalEList<?>)getEdges()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DottyPackage.COMPOUND_GV_NODE__NODES:
				return getNodes();
			case DottyPackage.COMPOUND_GV_NODE__EDGES:
				return getEdges();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DottyPackage.COMPOUND_GV_NODE__NODES:
				getNodes().clear();
				getNodes().addAll((Collection<? extends GVNode>)newValue);
				return;
			case DottyPackage.COMPOUND_GV_NODE__EDGES:
				getEdges().clear();
				getEdges().addAll((Collection<? extends GVEdge>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DottyPackage.COMPOUND_GV_NODE__NODES:
				getNodes().clear();
				return;
			case DottyPackage.COMPOUND_GV_NODE__EDGES:
				getEdges().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DottyPackage.COMPOUND_GV_NODE__NODES:
				return nodes != null && !nodes.isEmpty();
			case DottyPackage.COMPOUND_GV_NODE__EDGES:
				return edges != null && !edges.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CompoundGVNodeImpl
