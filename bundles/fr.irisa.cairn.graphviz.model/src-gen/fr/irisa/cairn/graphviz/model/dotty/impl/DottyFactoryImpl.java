/**
 */
package fr.irisa.cairn.graphviz.model.dotty.impl;

import fr.irisa.cairn.graphviz.model.dotty.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DottyFactoryImpl extends EFactoryImpl implements DottyFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DottyFactory init() {
		try {
			DottyFactory theDottyFactory = (DottyFactory)EPackage.Registry.INSTANCE.getEFactory(DottyPackage.eNS_URI);
			if (theDottyFactory != null) {
				return theDottyFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DottyFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DottyFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DottyPackage.GV_OBJECT: return createGVObject();
			case DottyPackage.GV_NODE: return createGVNode();
			case DottyPackage.GV_GRAPH: return createGVGraph();
			case DottyPackage.GV_EDGE: return createGVEdge();
			case DottyPackage.COMPOUND_GV_NODE: return createCompoundGVNode();
			case DottyPackage.GV_ATTRIBUTE: return createGVAttribute();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GVObject createGVObject() {
		GVObjectImpl gvObject = new GVObjectImpl();
		return gvObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GVNode createGVNode() {
		GVNodeImpl gvNode = new GVNodeImpl();
		return gvNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GVGraph createGVGraph() {
		GVGraphImpl gvGraph = new GVGraphImpl();
		return gvGraph;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GVEdge createGVEdge() {
		GVEdgeImpl gvEdge = new GVEdgeImpl();
		return gvEdge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompoundGVNode createCompoundGVNode() {
		CompoundGVNodeImpl compoundGVNode = new CompoundGVNodeImpl();
		return compoundGVNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GVAttribute createGVAttribute() {
		GVAttributeImpl gvAttribute = new GVAttributeImpl();
		return gvAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DottyPackage getDottyPackage() {
		return (DottyPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DottyPackage getPackage() {
		return DottyPackage.eINSTANCE;
	}

} //DottyFactoryImpl
