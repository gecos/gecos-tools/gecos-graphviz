/**
 */
package fr.irisa.cairn.graphviz.model.dotty.impl;

import fr.irisa.cairn.graphviz.model.dotty.DottyPackage;
import fr.irisa.cairn.graphviz.model.dotty.GVAttribute;
import fr.irisa.cairn.graphviz.model.dotty.GVEdge;
import fr.irisa.cairn.graphviz.model.dotty.GVGraph;
import fr.irisa.cairn.graphviz.model.dotty.GVNode;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>GV Graph</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.impl.GVGraphImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.impl.GVGraphImpl#getDefaultNodeAttributes <em>Default Node Attributes</em>}</li>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.impl.GVGraphImpl#getDefaultEdgeAttributes <em>Default Edge Attributes</em>}</li>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.impl.GVGraphImpl#isAcyclic <em>Acyclic</em>}</li>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.impl.GVGraphImpl#getNodes <em>Nodes</em>}</li>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.impl.GVGraphImpl#getEdges <em>Edges</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GVGraphImpl extends GVObjectImpl implements GVGraph {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDefaultNodeAttributes() <em>Default Node Attributes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultNodeAttributes()
	 * @generated
	 * @ordered
	 */
	protected EList<GVAttribute> defaultNodeAttributes;

	/**
	 * The cached value of the '{@link #getDefaultEdgeAttributes() <em>Default Edge Attributes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultEdgeAttributes()
	 * @generated
	 * @ordered
	 */
	protected EList<GVAttribute> defaultEdgeAttributes;

	/**
	 * The default value of the '{@link #isAcyclic() <em>Acyclic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAcyclic()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ACYCLIC_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAcyclic() <em>Acyclic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAcyclic()
	 * @generated
	 * @ordered
	 */
	protected boolean acyclic = ACYCLIC_EDEFAULT;

	/**
	 * The cached value of the '{@link #getNodes() <em>Nodes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<GVNode> nodes;

	/**
	 * The cached value of the '{@link #getEdges() <em>Edges</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<GVEdge> edges;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GVGraphImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DottyPackage.Literals.GV_GRAPH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DottyPackage.GV_GRAPH__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GVAttribute> getDefaultNodeAttributes() {
		if (defaultNodeAttributes == null) {
			defaultNodeAttributes = new EObjectContainmentEList<GVAttribute>(GVAttribute.class, this, DottyPackage.GV_GRAPH__DEFAULT_NODE_ATTRIBUTES);
		}
		return defaultNodeAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GVAttribute> getDefaultEdgeAttributes() {
		if (defaultEdgeAttributes == null) {
			defaultEdgeAttributes = new EObjectContainmentEList<GVAttribute>(GVAttribute.class, this, DottyPackage.GV_GRAPH__DEFAULT_EDGE_ATTRIBUTES);
		}
		return defaultEdgeAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAcyclic() {
		return acyclic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcyclic(boolean newAcyclic) {
		boolean oldAcyclic = acyclic;
		acyclic = newAcyclic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DottyPackage.GV_GRAPH__ACYCLIC, oldAcyclic, acyclic));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GVNode> getNodes() {
		if (nodes == null) {
			nodes = new EObjectContainmentEList<GVNode>(GVNode.class, this, DottyPackage.GV_GRAPH__NODES);
		}
		return nodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GVEdge> getEdges() {
		if (edges == null) {
			edges = new EObjectContainmentEList<GVEdge>(GVEdge.class, this, DottyPackage.GV_GRAPH__EDGES);
		}
		return edges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DottyPackage.GV_GRAPH__DEFAULT_NODE_ATTRIBUTES:
				return ((InternalEList<?>)getDefaultNodeAttributes()).basicRemove(otherEnd, msgs);
			case DottyPackage.GV_GRAPH__DEFAULT_EDGE_ATTRIBUTES:
				return ((InternalEList<?>)getDefaultEdgeAttributes()).basicRemove(otherEnd, msgs);
			case DottyPackage.GV_GRAPH__NODES:
				return ((InternalEList<?>)getNodes()).basicRemove(otherEnd, msgs);
			case DottyPackage.GV_GRAPH__EDGES:
				return ((InternalEList<?>)getEdges()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DottyPackage.GV_GRAPH__NAME:
				return getName();
			case DottyPackage.GV_GRAPH__DEFAULT_NODE_ATTRIBUTES:
				return getDefaultNodeAttributes();
			case DottyPackage.GV_GRAPH__DEFAULT_EDGE_ATTRIBUTES:
				return getDefaultEdgeAttributes();
			case DottyPackage.GV_GRAPH__ACYCLIC:
				return isAcyclic();
			case DottyPackage.GV_GRAPH__NODES:
				return getNodes();
			case DottyPackage.GV_GRAPH__EDGES:
				return getEdges();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DottyPackage.GV_GRAPH__NAME:
				setName((String)newValue);
				return;
			case DottyPackage.GV_GRAPH__DEFAULT_NODE_ATTRIBUTES:
				getDefaultNodeAttributes().clear();
				getDefaultNodeAttributes().addAll((Collection<? extends GVAttribute>)newValue);
				return;
			case DottyPackage.GV_GRAPH__DEFAULT_EDGE_ATTRIBUTES:
				getDefaultEdgeAttributes().clear();
				getDefaultEdgeAttributes().addAll((Collection<? extends GVAttribute>)newValue);
				return;
			case DottyPackage.GV_GRAPH__ACYCLIC:
				setAcyclic((Boolean)newValue);
				return;
			case DottyPackage.GV_GRAPH__NODES:
				getNodes().clear();
				getNodes().addAll((Collection<? extends GVNode>)newValue);
				return;
			case DottyPackage.GV_GRAPH__EDGES:
				getEdges().clear();
				getEdges().addAll((Collection<? extends GVEdge>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DottyPackage.GV_GRAPH__NAME:
				setName(NAME_EDEFAULT);
				return;
			case DottyPackage.GV_GRAPH__DEFAULT_NODE_ATTRIBUTES:
				getDefaultNodeAttributes().clear();
				return;
			case DottyPackage.GV_GRAPH__DEFAULT_EDGE_ATTRIBUTES:
				getDefaultEdgeAttributes().clear();
				return;
			case DottyPackage.GV_GRAPH__ACYCLIC:
				setAcyclic(ACYCLIC_EDEFAULT);
				return;
			case DottyPackage.GV_GRAPH__NODES:
				getNodes().clear();
				return;
			case DottyPackage.GV_GRAPH__EDGES:
				getEdges().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DottyPackage.GV_GRAPH__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case DottyPackage.GV_GRAPH__DEFAULT_NODE_ATTRIBUTES:
				return defaultNodeAttributes != null && !defaultNodeAttributes.isEmpty();
			case DottyPackage.GV_GRAPH__DEFAULT_EDGE_ATTRIBUTES:
				return defaultEdgeAttributes != null && !defaultEdgeAttributes.isEmpty();
			case DottyPackage.GV_GRAPH__ACYCLIC:
				return acyclic != ACYCLIC_EDEFAULT;
			case DottyPackage.GV_GRAPH__NODES:
				return nodes != null && !nodes.isEmpty();
			case DottyPackage.GV_GRAPH__EDGES:
				return edges != null && !edges.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", acyclic: ");
		result.append(acyclic);
		result.append(')');
		return result.toString();
	}

} //GVGraphImpl
