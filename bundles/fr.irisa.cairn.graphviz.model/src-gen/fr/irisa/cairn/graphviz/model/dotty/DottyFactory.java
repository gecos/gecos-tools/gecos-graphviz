/**
 */
package fr.irisa.cairn.graphviz.model.dotty;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage
 * @generated
 */
public interface DottyFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DottyFactory eINSTANCE = fr.irisa.cairn.graphviz.model.dotty.impl.DottyFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>GV Object</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>GV Object</em>'.
	 * @generated
	 */
	GVObject createGVObject();

	/**
	 * Returns a new object of class '<em>GV Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>GV Node</em>'.
	 * @generated
	 */
	GVNode createGVNode();

	/**
	 * Returns a new object of class '<em>GV Graph</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>GV Graph</em>'.
	 * @generated
	 */
	GVGraph createGVGraph();

	/**
	 * Returns a new object of class '<em>GV Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>GV Edge</em>'.
	 * @generated
	 */
	GVEdge createGVEdge();

	/**
	 * Returns a new object of class '<em>Compound GV Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Compound GV Node</em>'.
	 * @generated
	 */
	CompoundGVNode createCompoundGVNode();

	/**
	 * Returns a new object of class '<em>GV Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>GV Attribute</em>'.
	 * @generated
	 */
	GVAttribute createGVAttribute();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DottyPackage getDottyPackage();

} //DottyFactory
