/**
 */
package fr.irisa.cairn.graphviz.model.dotty;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>GV Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.GVEdge#getSource <em>Source</em>}</li>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.GVEdge#getDest <em>Dest</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage#getGVEdge()
 * @model
 * @generated
 */
public interface GVEdge extends GVObject {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(GVNode)
	 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage#getGVEdge_Source()
	 * @model
	 * @generated
	 */
	GVNode getSource();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.graphviz.model.dotty.GVEdge#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(GVNode value);

	/**
	 * Returns the value of the '<em><b>Dest</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dest</em>' reference.
	 * @see #setDest(GVNode)
	 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage#getGVEdge_Dest()
	 * @model
	 * @generated
	 */
	GVNode getDest();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.graphviz.model.dotty.GVEdge#getDest <em>Dest</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dest</em>' reference.
	 * @see #getDest()
	 * @generated
	 */
	void setDest(GVNode value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 * @generated
	 */
	String toString();

} // GVEdge
