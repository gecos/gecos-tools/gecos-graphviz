/**
 */
package fr.irisa.cairn.graphviz.model.dotty;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Compound GV Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.CompoundGVNode#getNodes <em>Nodes</em>}</li>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.CompoundGVNode#getEdges <em>Edges</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage#getCompoundGVNode()
 * @model
 * @generated
 */
public interface CompoundGVNode extends GVNode {
	/**
	 * Returns the value of the '<em><b>Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.graphviz.model.dotty.GVNode}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nodes</em>' containment reference list.
	 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage#getCompoundGVNode_Nodes()
	 * @model containment="true"
	 * @generated
	 */
	EList<GVNode> getNodes();

	/**
	 * Returns the value of the '<em><b>Edges</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.graphviz.model.dotty.GVEdge}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edges</em>' containment reference list.
	 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage#getCompoundGVNode_Edges()
	 * @model containment="true"
	 * @generated
	 */
	EList<GVEdge> getEdges();

} // CompoundGVNode
