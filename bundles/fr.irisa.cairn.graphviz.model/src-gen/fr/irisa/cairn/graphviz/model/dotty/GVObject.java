/**
 */
package fr.irisa.cairn.graphviz.model.dotty;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>GV Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.GVObject#getAttributes <em>Attributes</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage#getGVObject()
 * @model
 * @generated
 */
public interface GVObject extends EObject {
	/**
	 * Returns the value of the '<em><b>Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.graphviz.model.dotty.GVAttribute}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attributes</em>' containment reference list.
	 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage#getGVObject_Attributes()
	 * @model containment="true"
	 * @generated
	 */
	EList<GVAttribute> getAttributes();

} // GVObject
