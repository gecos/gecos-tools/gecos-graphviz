/**
 */
package fr.irisa.cairn.graphviz.model.dotty;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.graphviz.model.dotty.DottyFactory
 * @model kind="package"
 * @generated
 */
public interface DottyPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "dotty";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "fr.irisa.cairn.graphviz.model.dotty";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "dotty";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DottyPackage eINSTANCE = fr.irisa.cairn.graphviz.model.dotty.impl.DottyPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.graphviz.model.dotty.impl.GVObjectImpl <em>GV Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.graphviz.model.dotty.impl.GVObjectImpl
	 * @see fr.irisa.cairn.graphviz.model.dotty.impl.DottyPackageImpl#getGVObject()
	 * @generated
	 */
	int GV_OBJECT = 0;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_OBJECT__ATTRIBUTES = 0;

	/**
	 * The number of structural features of the '<em>GV Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_OBJECT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>GV Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_OBJECT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.graphviz.model.dotty.impl.GVNodeImpl <em>GV Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.graphviz.model.dotty.impl.GVNodeImpl
	 * @see fr.irisa.cairn.graphviz.model.dotty.impl.DottyPackageImpl#getGVNode()
	 * @generated
	 */
	int GV_NODE = 1;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_NODE__ATTRIBUTES = GV_OBJECT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_NODE__NAME = GV_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Rank</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_NODE__RANK = GV_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>GV Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_NODE_FEATURE_COUNT = GV_OBJECT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_NODE___TO_STRING = GV_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>GV Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_NODE_OPERATION_COUNT = GV_OBJECT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.graphviz.model.dotty.impl.GVGraphImpl <em>GV Graph</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.graphviz.model.dotty.impl.GVGraphImpl
	 * @see fr.irisa.cairn.graphviz.model.dotty.impl.DottyPackageImpl#getGVGraph()
	 * @generated
	 */
	int GV_GRAPH = 2;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_GRAPH__ATTRIBUTES = GV_OBJECT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_GRAPH__NAME = GV_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Default Node Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_GRAPH__DEFAULT_NODE_ATTRIBUTES = GV_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Default Edge Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_GRAPH__DEFAULT_EDGE_ATTRIBUTES = GV_OBJECT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Acyclic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_GRAPH__ACYCLIC = GV_OBJECT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_GRAPH__NODES = GV_OBJECT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_GRAPH__EDGES = GV_OBJECT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>GV Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_GRAPH_FEATURE_COUNT = GV_OBJECT_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>GV Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_GRAPH_OPERATION_COUNT = GV_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.graphviz.model.dotty.impl.GVEdgeImpl <em>GV Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.graphviz.model.dotty.impl.GVEdgeImpl
	 * @see fr.irisa.cairn.graphviz.model.dotty.impl.DottyPackageImpl#getGVEdge()
	 * @generated
	 */
	int GV_EDGE = 3;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_EDGE__ATTRIBUTES = GV_OBJECT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_EDGE__SOURCE = GV_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dest</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_EDGE__DEST = GV_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>GV Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_EDGE_FEATURE_COUNT = GV_OBJECT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_EDGE___TO_STRING = GV_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>GV Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_EDGE_OPERATION_COUNT = GV_OBJECT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.graphviz.model.dotty.impl.CompoundGVNodeImpl <em>Compound GV Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.graphviz.model.dotty.impl.CompoundGVNodeImpl
	 * @see fr.irisa.cairn.graphviz.model.dotty.impl.DottyPackageImpl#getCompoundGVNode()
	 * @generated
	 */
	int COMPOUND_GV_NODE = 4;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_GV_NODE__ATTRIBUTES = GV_NODE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_GV_NODE__NAME = GV_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Rank</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_GV_NODE__RANK = GV_NODE__RANK;

	/**
	 * The feature id for the '<em><b>Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_GV_NODE__NODES = GV_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_GV_NODE__EDGES = GV_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Compound GV Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_GV_NODE_FEATURE_COUNT = GV_NODE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_GV_NODE___TO_STRING = GV_NODE___TO_STRING;

	/**
	 * The number of operations of the '<em>Compound GV Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_GV_NODE_OPERATION_COUNT = GV_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.graphviz.model.dotty.impl.GVAttributeImpl <em>GV Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.graphviz.model.dotty.impl.GVAttributeImpl
	 * @see fr.irisa.cairn.graphviz.model.dotty.impl.DottyPackageImpl#getGVAttribute()
	 * @generated
	 */
	int GV_ATTRIBUTE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_ATTRIBUTE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_ATTRIBUTE__VALUE = 1;

	/**
	 * The number of structural features of the '<em>GV Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_ATTRIBUTE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>GV Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GV_ATTRIBUTE_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.graphviz.model.dotty.GVObject <em>GV Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>GV Object</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.GVObject
	 * @generated
	 */
	EClass getGVObject();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.graphviz.model.dotty.GVObject#getAttributes <em>Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attributes</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.GVObject#getAttributes()
	 * @see #getGVObject()
	 * @generated
	 */
	EReference getGVObject_Attributes();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.graphviz.model.dotty.GVNode <em>GV Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>GV Node</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.GVNode
	 * @generated
	 */
	EClass getGVNode();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.graphviz.model.dotty.GVNode#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.GVNode#getName()
	 * @see #getGVNode()
	 * @generated
	 */
	EAttribute getGVNode_Name();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.graphviz.model.dotty.GVNode#getRank <em>Rank</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rank</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.GVNode#getRank()
	 * @see #getGVNode()
	 * @generated
	 */
	EAttribute getGVNode_Rank();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graphviz.model.dotty.GVNode#toString() <em>To String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>To String</em>' operation.
	 * @see fr.irisa.cairn.graphviz.model.dotty.GVNode#toString()
	 * @generated
	 */
	EOperation getGVNode__ToString();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.graphviz.model.dotty.GVGraph <em>GV Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>GV Graph</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.GVGraph
	 * @generated
	 */
	EClass getGVGraph();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.graphviz.model.dotty.GVGraph#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.GVGraph#getName()
	 * @see #getGVGraph()
	 * @generated
	 */
	EAttribute getGVGraph_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.graphviz.model.dotty.GVGraph#getDefaultNodeAttributes <em>Default Node Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Default Node Attributes</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.GVGraph#getDefaultNodeAttributes()
	 * @see #getGVGraph()
	 * @generated
	 */
	EReference getGVGraph_DefaultNodeAttributes();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.graphviz.model.dotty.GVGraph#getDefaultEdgeAttributes <em>Default Edge Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Default Edge Attributes</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.GVGraph#getDefaultEdgeAttributes()
	 * @see #getGVGraph()
	 * @generated
	 */
	EReference getGVGraph_DefaultEdgeAttributes();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.graphviz.model.dotty.GVGraph#isAcyclic <em>Acyclic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Acyclic</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.GVGraph#isAcyclic()
	 * @see #getGVGraph()
	 * @generated
	 */
	EAttribute getGVGraph_Acyclic();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.graphviz.model.dotty.GVGraph#getNodes <em>Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Nodes</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.GVGraph#getNodes()
	 * @see #getGVGraph()
	 * @generated
	 */
	EReference getGVGraph_Nodes();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.graphviz.model.dotty.GVGraph#getEdges <em>Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Edges</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.GVGraph#getEdges()
	 * @see #getGVGraph()
	 * @generated
	 */
	EReference getGVGraph_Edges();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.graphviz.model.dotty.GVEdge <em>GV Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>GV Edge</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.GVEdge
	 * @generated
	 */
	EClass getGVEdge();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.graphviz.model.dotty.GVEdge#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.GVEdge#getSource()
	 * @see #getGVEdge()
	 * @generated
	 */
	EReference getGVEdge_Source();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.graphviz.model.dotty.GVEdge#getDest <em>Dest</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dest</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.GVEdge#getDest()
	 * @see #getGVEdge()
	 * @generated
	 */
	EReference getGVEdge_Dest();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graphviz.model.dotty.GVEdge#toString() <em>To String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>To String</em>' operation.
	 * @see fr.irisa.cairn.graphviz.model.dotty.GVEdge#toString()
	 * @generated
	 */
	EOperation getGVEdge__ToString();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.graphviz.model.dotty.CompoundGVNode <em>Compound GV Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Compound GV Node</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.CompoundGVNode
	 * @generated
	 */
	EClass getCompoundGVNode();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.graphviz.model.dotty.CompoundGVNode#getNodes <em>Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Nodes</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.CompoundGVNode#getNodes()
	 * @see #getCompoundGVNode()
	 * @generated
	 */
	EReference getCompoundGVNode_Nodes();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.graphviz.model.dotty.CompoundGVNode#getEdges <em>Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Edges</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.CompoundGVNode#getEdges()
	 * @see #getCompoundGVNode()
	 * @generated
	 */
	EReference getCompoundGVNode_Edges();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.graphviz.model.dotty.GVAttribute <em>GV Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>GV Attribute</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.GVAttribute
	 * @generated
	 */
	EClass getGVAttribute();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.graphviz.model.dotty.GVAttribute#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.GVAttribute#getName()
	 * @see #getGVAttribute()
	 * @generated
	 */
	EAttribute getGVAttribute_Name();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.graphviz.model.dotty.GVAttribute#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.irisa.cairn.graphviz.model.dotty.GVAttribute#getValue()
	 * @see #getGVAttribute()
	 * @generated
	 */
	EAttribute getGVAttribute_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DottyFactory getDottyFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.graphviz.model.dotty.impl.GVObjectImpl <em>GV Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.graphviz.model.dotty.impl.GVObjectImpl
		 * @see fr.irisa.cairn.graphviz.model.dotty.impl.DottyPackageImpl#getGVObject()
		 * @generated
		 */
		EClass GV_OBJECT = eINSTANCE.getGVObject();

		/**
		 * The meta object literal for the '<em><b>Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GV_OBJECT__ATTRIBUTES = eINSTANCE.getGVObject_Attributes();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.graphviz.model.dotty.impl.GVNodeImpl <em>GV Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.graphviz.model.dotty.impl.GVNodeImpl
		 * @see fr.irisa.cairn.graphviz.model.dotty.impl.DottyPackageImpl#getGVNode()
		 * @generated
		 */
		EClass GV_NODE = eINSTANCE.getGVNode();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GV_NODE__NAME = eINSTANCE.getGVNode_Name();

		/**
		 * The meta object literal for the '<em><b>Rank</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GV_NODE__RANK = eINSTANCE.getGVNode_Rank();

		/**
		 * The meta object literal for the '<em><b>To String</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation GV_NODE___TO_STRING = eINSTANCE.getGVNode__ToString();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.graphviz.model.dotty.impl.GVGraphImpl <em>GV Graph</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.graphviz.model.dotty.impl.GVGraphImpl
		 * @see fr.irisa.cairn.graphviz.model.dotty.impl.DottyPackageImpl#getGVGraph()
		 * @generated
		 */
		EClass GV_GRAPH = eINSTANCE.getGVGraph();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GV_GRAPH__NAME = eINSTANCE.getGVGraph_Name();

		/**
		 * The meta object literal for the '<em><b>Default Node Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GV_GRAPH__DEFAULT_NODE_ATTRIBUTES = eINSTANCE.getGVGraph_DefaultNodeAttributes();

		/**
		 * The meta object literal for the '<em><b>Default Edge Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GV_GRAPH__DEFAULT_EDGE_ATTRIBUTES = eINSTANCE.getGVGraph_DefaultEdgeAttributes();

		/**
		 * The meta object literal for the '<em><b>Acyclic</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GV_GRAPH__ACYCLIC = eINSTANCE.getGVGraph_Acyclic();

		/**
		 * The meta object literal for the '<em><b>Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GV_GRAPH__NODES = eINSTANCE.getGVGraph_Nodes();

		/**
		 * The meta object literal for the '<em><b>Edges</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GV_GRAPH__EDGES = eINSTANCE.getGVGraph_Edges();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.graphviz.model.dotty.impl.GVEdgeImpl <em>GV Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.graphviz.model.dotty.impl.GVEdgeImpl
		 * @see fr.irisa.cairn.graphviz.model.dotty.impl.DottyPackageImpl#getGVEdge()
		 * @generated
		 */
		EClass GV_EDGE = eINSTANCE.getGVEdge();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GV_EDGE__SOURCE = eINSTANCE.getGVEdge_Source();

		/**
		 * The meta object literal for the '<em><b>Dest</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GV_EDGE__DEST = eINSTANCE.getGVEdge_Dest();

		/**
		 * The meta object literal for the '<em><b>To String</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation GV_EDGE___TO_STRING = eINSTANCE.getGVEdge__ToString();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.graphviz.model.dotty.impl.CompoundGVNodeImpl <em>Compound GV Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.graphviz.model.dotty.impl.CompoundGVNodeImpl
		 * @see fr.irisa.cairn.graphviz.model.dotty.impl.DottyPackageImpl#getCompoundGVNode()
		 * @generated
		 */
		EClass COMPOUND_GV_NODE = eINSTANCE.getCompoundGVNode();

		/**
		 * The meta object literal for the '<em><b>Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOUND_GV_NODE__NODES = eINSTANCE.getCompoundGVNode_Nodes();

		/**
		 * The meta object literal for the '<em><b>Edges</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOUND_GV_NODE__EDGES = eINSTANCE.getCompoundGVNode_Edges();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.graphviz.model.dotty.impl.GVAttributeImpl <em>GV Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.graphviz.model.dotty.impl.GVAttributeImpl
		 * @see fr.irisa.cairn.graphviz.model.dotty.impl.DottyPackageImpl#getGVAttribute()
		 * @generated
		 */
		EClass GV_ATTRIBUTE = eINSTANCE.getGVAttribute();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GV_ATTRIBUTE__NAME = eINSTANCE.getGVAttribute_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GV_ATTRIBUTE__VALUE = eINSTANCE.getGVAttribute_Value();

	}

} //DottyPackage
