/**
 */
package fr.irisa.cairn.graphviz.model.dotty;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>GV Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.GVGraph#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.GVGraph#getDefaultNodeAttributes <em>Default Node Attributes</em>}</li>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.GVGraph#getDefaultEdgeAttributes <em>Default Edge Attributes</em>}</li>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.GVGraph#isAcyclic <em>Acyclic</em>}</li>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.GVGraph#getNodes <em>Nodes</em>}</li>
 *   <li>{@link fr.irisa.cairn.graphviz.model.dotty.GVGraph#getEdges <em>Edges</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage#getGVGraph()
 * @model
 * @generated
 */
public interface GVGraph extends GVObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage#getGVGraph_Name()
	 * @model unique="false"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.graphviz.model.dotty.GVGraph#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Default Node Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.graphviz.model.dotty.GVAttribute}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Node Attributes</em>' containment reference list.
	 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage#getGVGraph_DefaultNodeAttributes()
	 * @model containment="true"
	 * @generated
	 */
	EList<GVAttribute> getDefaultNodeAttributes();

	/**
	 * Returns the value of the '<em><b>Default Edge Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.graphviz.model.dotty.GVAttribute}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Edge Attributes</em>' containment reference list.
	 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage#getGVGraph_DefaultEdgeAttributes()
	 * @model containment="true"
	 * @generated
	 */
	EList<GVAttribute> getDefaultEdgeAttributes();

	/**
	 * Returns the value of the '<em><b>Acyclic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acyclic</em>' attribute.
	 * @see #setAcyclic(boolean)
	 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage#getGVGraph_Acyclic()
	 * @model unique="false"
	 * @generated
	 */
	boolean isAcyclic();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.graphviz.model.dotty.GVGraph#isAcyclic <em>Acyclic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acyclic</em>' attribute.
	 * @see #isAcyclic()
	 * @generated
	 */
	void setAcyclic(boolean value);

	/**
	 * Returns the value of the '<em><b>Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.graphviz.model.dotty.GVNode}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nodes</em>' containment reference list.
	 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage#getGVGraph_Nodes()
	 * @model containment="true"
	 * @generated
	 */
	EList<GVNode> getNodes();

	/**
	 * Returns the value of the '<em><b>Edges</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.graphviz.model.dotty.GVEdge}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edges</em>' containment reference list.
	 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage#getGVGraph_Edges()
	 * @model containment="true"
	 * @generated
	 */
	EList<GVEdge> getEdges();

} // GVGraph
