/**
 */
package fr.irisa.cairn.graphviz.model.dotty.util;

import fr.irisa.cairn.graphviz.model.dotty.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.graphviz.model.dotty.DottyPackage
 * @generated
 */
public class DottySwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DottyPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DottySwitch() {
		if (modelPackage == null) {
			modelPackage = DottyPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case DottyPackage.GV_OBJECT: {
				GVObject gvObject = (GVObject)theEObject;
				T result = caseGVObject(gvObject);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DottyPackage.GV_NODE: {
				GVNode gvNode = (GVNode)theEObject;
				T result = caseGVNode(gvNode);
				if (result == null) result = caseGVObject(gvNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DottyPackage.GV_GRAPH: {
				GVGraph gvGraph = (GVGraph)theEObject;
				T result = caseGVGraph(gvGraph);
				if (result == null) result = caseGVObject(gvGraph);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DottyPackage.GV_EDGE: {
				GVEdge gvEdge = (GVEdge)theEObject;
				T result = caseGVEdge(gvEdge);
				if (result == null) result = caseGVObject(gvEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DottyPackage.COMPOUND_GV_NODE: {
				CompoundGVNode compoundGVNode = (CompoundGVNode)theEObject;
				T result = caseCompoundGVNode(compoundGVNode);
				if (result == null) result = caseGVNode(compoundGVNode);
				if (result == null) result = caseGVObject(compoundGVNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DottyPackage.GV_ATTRIBUTE: {
				GVAttribute gvAttribute = (GVAttribute)theEObject;
				T result = caseGVAttribute(gvAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GV Object</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GV Object</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGVObject(GVObject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GV Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GV Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGVNode(GVNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GV Graph</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GV Graph</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGVGraph(GVGraph object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GV Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GV Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGVEdge(GVEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Compound GV Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Compound GV Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompoundGVNode(CompoundGVNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GV Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GV Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGVAttribute(GVAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //DottySwitch
