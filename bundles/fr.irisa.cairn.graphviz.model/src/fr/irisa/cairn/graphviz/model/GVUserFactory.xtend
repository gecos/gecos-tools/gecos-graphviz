package fr.irisa.cairn.graphviz.model

import fr.irisa.cairn.graphviz.model.dotty.DottyFactory
import fr.irisa.cairn.graphviz.model.dotty.GVAttribute
import fr.irisa.cairn.graphviz.model.dotty.GVNode
import org.eclipse.xtext.xbase.lib.Functions.Function1

class GVUserFactory {
	
	
	new() {
		
	}
	
	def static graph(boolean acyclic) {
		val e = DottyFactory.eINSTANCE.createGVGraph
		e.acyclic=acyclic
		e
	} 
	
//	def static <T> graph(Iterable<T> l, Function1<GVNode,T> func) {
//		val e = DottyFactory.eINSTANCE.createGVGraph
//		e.acyclic=acyclic
//		e
//	} 
	
	def static node(String name, int rank, Iterable<GVAttribute> attributes) {
		val e = DottyFactory.eINSTANCE.createGVNode
		e.name=name
		e.rank=rank
		e.attributes+=attributes
		e
	}


	def static superNode(String name,  Iterable<GVAttribute> attributes) {
		val e = DottyFactory.eINSTANCE.createCompoundGVNode
		e.name="cluster_"+name
		e.attributes+=attributes
		e
	}

	def static label(String txt) {
		attribute("label", '''"«txt.replace("\"","\\\'")»"''')
	}

	def static color(String txt) {
		attribute("color", '''"«txt.replace("\"","\\\'")»"''')
	}

	def static shape(String txt) {
		attribute("shape", '''"«txt.replace("\"","\\\'")»"''')
		
	}
	
	def static style(String txt) {
		attribute("style", '''"«txt.replace("\"","\\\'")»"''')
		
	}
	
	def static dashed() { style("dashed") }
	def static dotted() { style("dotted") }

	def static node(String name, String label, Iterable<GVAttribute> attributes) {
		 node(name,-1,#[label(label)]+attributes)
	}
	def static node(String name, String label,int rank, Iterable<GVAttribute> attributes) {
		 node(name,Math.max(rank,-1),#[label(label)]+attributes)
	}
	
	def static node(String name, String label,int rank) {
		node(name,Math.max(rank,-1),#[label(label)])
	}
	def static node(String name, String label) {
		node(name,-1,#[label(label)])
	}
	def static node(String name) {
		node(name,-1,#[])
	}

	def static edge(GVNode src, GVNode dst) {
		if (src===null) {
			throw new UnsupportedOperationException('''Invalid edge with null source''')
		}
		if (dst===null) {
			throw new UnsupportedOperationException('''Invalid edge with null destination''')
		}
		val e = DottyFactory.eINSTANCE.createGVEdge
		e.dest = dst
		e.source=src
		e
	}
	def static edge(GVNode src, GVNode dst, String labelTxt) {
		val e = edge(src,dst)
		e.attributes+= label(labelTxt)
		e
	}

	def static edge(GVNode src, GVNode dst,  Iterable<GVAttribute> attributes) {
		val e = edge(src,dst)
		e.attributes+= attributes
		e
	}
	def static edge(GVNode src, GVNode dst,   String labelTxt, Iterable<GVAttribute> attributes) {
		val e = edge(src,dst,labelTxt)
		e.attributes+= attributes
		e
	}

	def static attribute(String name, String value) {
		val e = DottyFactory.eINSTANCE.createGVAttribute
		e.name=name
		e.value=value
		e
	}
	
	
	def static compound(String name, Iterable<GVAttribute> attributes, Iterable<GVNode> children) {
		val e = DottyFactory.eINSTANCE.createCompoundGVNode
		e.name=name
		e.nodes+=children
		e.attributes+=attributes
		e
	}
}