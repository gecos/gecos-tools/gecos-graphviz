package fr.irisa.cairn.graphviz.model

import fr.irisa.cairn.graphviz.model.dotty.GVGraph
import fr.irisa.cairn.graphviz.model.dotty.CompoundGVNode
import fr.irisa.cairn.graphviz.model.dotty.GVNode
import fr.irisa.cairn.graphviz.model.dotty.GVEdge
import java.util.HashMap
import fr.irisa.cairn.graphviz.model.dotty.GVAttribute
import java.util.regex.Pattern

class GVFixer {
	
	def static fix(GVGraph g) {
		var id=0
		for (n : g.nodes) n.name="n"+(id++)
	}

}