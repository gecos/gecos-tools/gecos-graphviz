package fr.irisa.cairn.graphviz.model

import fr.irisa.cairn.graphviz.model.dotty.GVGraph
import fr.irisa.cairn.graphviz.model.dotty.GVNode
import fr.irisa.cairn.graphviz.model.dotty.GVEdge
import fr.irisa.cairn.graphviz.model.dotty.CompoundGVNode
import java.io.PrintStream
import fr.irisa.cairn.graphviz.model.dotty.GVAttribute
import org.eclipse.xtext.xbase.lib.Functions.Function1
import java.util.Map
import java.util.List
import java.util.HashMap
import java.util.ArrayList
import java.util.HashSet

class GVPrettyPrinter {

	static Function1<GVNode,Boolean> filter;
	
	def static save(String path, GVGraph g) {
		val ps = new PrintStream(path)
		val pp = new GVPrettyPrinter()
		if (g.nodes.filter(CompoundGVNode).size!=0) {
			pp.buildRankMap(g)
			pp.noRank=true
		}
		ps.append(pp.print(g))
		filter = [true]
		ps.close
	}

	def static save(String path, GVGraph g, Function1<GVNode,Boolean> nodeFilter) {
		val ps = new PrintStream(path)
		val pp = new GVPrettyPrinter()
		filter=nodeFilter
		ps.append(pp.print(g))
		ps.close
	}


	new() {
		
	}
	
	
	Map<Integer,List<GVNode>> rankMap= new HashMap;
	
	boolean noRank = false
	
	def buildRankMap(GVGraph g) {
		rankMap.clear
		for (n:g.nodes) {
			if (!rankMap.containsKey(n.rank)) {
				rankMap.put(n.rank, new ArrayList)
			}
			rankMap.get(n.rank)+=n
		}
	}

	def extractNonRankedNode(GVGraph g) {
		var set = new HashSet(g.nodes)
		set.removeAll(rankMap.filter[k, v| k!=1].values.flatten)
		set
	}

	def dispatch print(GVGraph g) {
		buildRankMap(g)
		val minRank = 1;
		val nonRankedNode = g.nodes.filter[rank<minRank]
		val maxRank = 
		if (g.nodes.size >0)
			g.nodes.map[rank].max
		else
			-1
		if (maxRank==-1 || noRank) {
			'''
			digraph Graph_«g.name» {
				compound=true;
				// non ranked nodes
				
				«IF g.defaultNodeAttributes.size!=0»
					graph[«FOR a:g.defaultNodeAttributes SEPARATOR ","» «print(a)» «ENDFOR» ]
					node [«FOR a:g.defaultNodeAttributes SEPARATOR ","» «print(a)» «ENDFOR» ]
				«ENDIF»
				
				«IF g.defaultEdgeAttributes.size!=0»
					edge [«FOR a:g.defaultEdgeAttributes SEPARATOR ","» «print(a)» «ENDFOR» ]
				«ENDIF»
				
				«FOR n:g.nodes»
					// node «n.name»
					«print(n)» 
				«ENDFOR»
				
				// edges 
				«FOR e:g.edges»
					«print(e)»
				«ENDFOR»
			}
			'''		
			
		} else {
			'''
			digraph Graph_«g.name» {
				compound=true;
				rankdir=TB;
				  
			«IF g.defaultNodeAttributes.size!=0»
				node [«FOR a:g.defaultNodeAttributes» «print(a)» «ENDFOR» ]
			«ENDIF»
			
			«IF g.defaultEdgeAttributes.size!=0»
				edge [«FOR a:g.defaultEdgeAttributes» «print(a)» «ENDFOR» ]
			«ENDIF»

				// ranked nodes
				«FOR r:minRank..maxRank» 
				{ 
					// Rank «r»
					rank = same;
					«IF rankMap.containsKey(r)»
					«FOR n:rankMap.get(r)»
					«print(n)» 
					«ENDFOR»
					«ENDIF»
				}
				«ENDFOR»
				

				// non ranked nodes
				«FOR n:nonRankedNode»
					// node «n.name»
					«print(n)» 
				«ENDFOR»
				
				// edges 
				«FOR e:g.edges»
					«print(e)»
				«ENDFOR»
			}
			'''		
			
		}
	}

	def dispatch print(CompoundGVNode n) {
		val compound =  n.nodes.filter(CompoundGVNode).size!=0
		'''subgraph «n.name» {
			compound = true;
			«FOR e:n.attributes»
				«print(e)»
			«ENDFOR»
			«FOR c:n.nodes»
				«print(c)» 
			«ENDFOR»
			«FOR e:n.edges»
				«print(e)» 
			«ENDFOR»
		}'''
	}

	def dispatch print(GVNode n) {
		if (n.attributes.size==0) {
			'''«n.name»;
			'''
		} else {
			'''«n.name» [«FOR e:n.attributes SEPARATOR ","»«print(e)»«ENDFOR»];
			'''
		}
	}

	def dispatch print(GVAttribute n) {
		''' «n.name»=«n.value»'''
	}

	def dispatch print(GVEdge e) {
		if (e.attributes.size==0) {
			'''«e.source.name»->«e.dest.name»; 
			'''
		} else {
			'''«e.source.name»->«e.dest.name» [«FOR a:e.attributes SEPARATOR ","»«print(a)»«ENDFOR»] ;
			'''
		}
	}
	
}