package fr.irisa.cairn.graphviz.model

import fr.irisa.cairn.graphviz.model.dotty.GVNode
import java.util.Map
import fr.irisa.cairn.graphviz.model.dotty.GVEdge
import java.util.HashMap
import fr.irisa.cairn.graphviz.model.dotty.GVGraph

abstract class GVAdapter<V,E> extends GVUserFactory {
	
	
	protected Map<V,GVNode> nodeMap = new HashMap<V,GVNode>;

	protected GVGraph graph
	
	protected int id=0
	
	new(String name, boolean acyclic) {
		graph = GVUserFactory.graph(true);
		graph.name = name
	}

	def buildDottyModel() {
		for (n: allNodes) {
			addNode(n)
		}
		for (e: allEdges) {
			addEdge(e)
		}
	}	
	abstract def Iterable<E> getAllEdges();
	abstract def Iterable<V> getAllNodes();
	abstract def boolean isSubGraph(V a);
	abstract def V getSource(E e);
	abstract def V getDest(E e);

	def getDottyModel() {
		graph
	}	
	def addNode(V obj) {
		val node =GVUserFactory.node("n"+(id++))
		nodeMap.put(obj,node )
		graph.nodes+=node	
	} 

	def addEdge(E e) {
		val src = getSource(e)
		val dst = getDest(e)
		if (!nodeMap.containsKey(src)) {	
			throw new UnsupportedOperationException("Source node "+src+" is not a known node")
		}
		if (!nodeMap.containsKey(dst)) {	
			throw new UnsupportedOperationException("Dest node "+dst+" is not a known node")
		}
		val edge=GVUserFactory.edge(nodeMap.get(src),nodeMap.get(dst))
		graph.edges+=edge
	} 
	
}