package fr.irisa.cairn.graphviz.model

import fr.irisa.cairn.graphviz.model.dotty.GVGraph
import fr.irisa.cairn.graphviz.model.dotty.CompoundGVNode
import fr.irisa.cairn.graphviz.model.dotty.GVNode
import fr.irisa.cairn.graphviz.model.dotty.GVEdge
import java.util.HashMap
import fr.irisa.cairn.graphviz.model.dotty.GVAttribute
import java.util.regex.Pattern
import fr.irisa.cairn.tools.ecore.query.EMFUtils

class GVChecker {
	
	var HashMap<String,GVNode> nameMap = new HashMap<String,GVNode>()
	
	GVGraph g
	
	new (GVGraph g) {
		this.g=g 
	}
	
	
	
	def check() {
		check(g)
	}
	
	def dispatch check(GVGraph n) {
		nameMap.clear
		val n_nodes = EMFUtils.eAllContentsInstancesOf(n, GVNode)
		if (n.nodes.size==0) {
			//throw new RuntimeException("Inconsistent graphviz model 0 nodes")
		}
		for (_n:n_nodes) check(_n)
		for (e:n.edges) {
			check(e)
			if (!n_nodes.contains(e.source)) throw new RuntimeException("Inconsistent graphviz model : source node "+e.source+" does not belong to the current graph")
			if (!n_nodes.contains(e.dest)) {
				throw new RuntimeException("Inconsistent graphviz model : destination node "+e.dest+" does not belong to the current graph")
			}
		}
	}

	def dispatch check(CompoundGVNode n) {
		checkName(n.name,n)
		if (n.nodes.size==0) throw new RuntimeException("Inconsistent graphviz node")
		for (_n:n.nodes) check(_n)
		for (e:n.edges) {
			check(e)
			if (!n.nodes.contains(e.source)) throw new RuntimeException("Edge in compound node has external source")
			if (!n.nodes.contains(e.dest)) {
				throw new RuntimeException("Inconsistent graphviz model : inconsitent edge")
			}
		}
	
	}

	def checkName(String s, Object o) {
		val p=Pattern.compile("([a-zA-Z_$][0-9a-zA-Z_$]*)");
		if (!p.matcher(s).matches) {
			 throw new RuntimeException("Invalid name "+s+" for "+o.class.simpleName)
		}
	}

	def dispatch check(GVNode n) {
		checkName(n.name,n)
		if (nameMap.containsKey(n.name) && nameMap.get(n.name)!==n) {
			 throw new RuntimeException("Inconsistent graphviz model : nodes "+n+" and "+nameMap.get(n.name)+" have the same id "+n.name)
		} else {
			nameMap.put(n.name,n)
		}
	}

	def dispatch check(GVAttribute a) {
		checkName(a.name,a)
	}

	def dispatch check(GVEdge e) {
		if (e.source===null) throw new RuntimeException("Inconsistent graphviz model")
		if (e.dest===null) throw new RuntimeException("Inconsistent graphviz model")
	}
	
	
}